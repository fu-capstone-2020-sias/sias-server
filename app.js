var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var mysql = require("mysql");
var cors = require("cors");
var session = require("express-session");
var config = require("./config");
var MySQLStore = require("express-mysql-session")(session);
var passport = require("passport");

// Bắt buộc phải sử dụng createPool để duy trì connection với Azure DB
var connection = mysql.createPool({
    host: config.AZURE_DB.DATABASE_HOST,
    port: 3306,
    user: config.AZURE_DB.DATABASE_USERNAME,
    password: config.AZURE_DB.DATABASE_PASSWORD,
    database: config.AZURE_DB.DATABASE_SCHEMA,
});

var sessionStore = new MySQLStore(
    {
        checkExpirationInterval: 300000, // 900000 = 15 minutes
        expiration: 300000, // 86400000 = 24 hours
    },
    connection
);

var indexRouter = require("./routes/index");
var accountRouter = require("./routes/accountRouter");
var authenticationRouter = require("./routes/authenticationRouter");
var companyInfoRouter = require("./routes/companyInfoRouter");
var orderRouter = require("./routes/orderRouter");
var feedbackRouter = require("./routes/feedbackRouter");
const warehouseRouter = require('./routes/warehouseRouter');
const {ALLOWED_ORIGINS} = require("./constants/constants");

const stockProductRouter = require('./routes/stockProductRouter');
const cityProvinceRouter = require('./routes/cityProvinceRouter');
const districtRouter = require('./routes/districRouter');
const wardCommuneRouter = require('./routes/wardCommuneRouter');
const roleRouter = require("./routes/roleRouter");
const departmentRouter = require('./routes/departmentRouter');
const warehouseManagerRouter = require('./routes/warehouseManagerRouter');
const tempRouter = require('./routes/temp')
const steelTypeRouter = require('./routes/steelTypesRouter');
const stockGroupRouter = require('./routes/stockGroup')
const stockProductStandardRouter = require('./routes/stockProductStandardRouter')
const stockProductStatusRouter = require('./routes/stockProductStatusRouter')
const stockProductImportRouter = require('./routes/stockProductImportBatchRouter')
const demoRouter = require("./routes/demoRouter");
const approverRouter = require("./routes/approverRouter");
const assigneeRouter = require("./routes/assigneeRouter");

var app = express();

var expireDate = new Date();
expireDate.setDate(expireDate.getDate() + 1);

// Lưu session trên MySQL Database. 
// Khi session timeout, tự động xóa cái session đó trên DB
// nếu server vẫn chạy 
app.use(
    session({
        resave: true,
        saveUninitialized: true,
        secret: config.SECRET_KEY,
        store: sessionStore,
        cookie: {expires: expireDate}, // expires in 1 day
    })
);


// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
// Sử dụng passport để authenticate
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors({
    credentials: true,
    origin: function (origin, callback) {
        // allow requests with no origin
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        if (ALLOWED_ORIGINS.indexOf(origin) === -1) {
            var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }

}));

app.use("/", indexRouter);
app.use("/account", accountRouter);
app.use("/auth", authenticationRouter);
app.use("/company", companyInfoRouter);
app.use('/order', orderRouter);
app.use("/feedback", feedbackRouter);
app.use('/warehouse', warehouseRouter);
app.use('/stockProduct', stockProductRouter);
app.use('/cityProvince', cityProvinceRouter);
app.use('/district', districtRouter);
app.use('/wardCommune', wardCommuneRouter);
app.use('/role', roleRouter);
app.use('/department', departmentRouter);
app.use('/warehouseManager', warehouseManagerRouter);
app.use('/temp', tempRouter)
app.use('/steelType', steelTypeRouter);
app.use('/stockGroup', stockGroupRouter);
app.use('/stockProductStandard', stockProductStandardRouter)
app.use('/stockProductStatus', stockProductStatusRouter)
app.use('/stockProductImportBatch', stockProductImportRouter)
app.use('/demo', demoRouter)
app.use('/approver', approverRouter);
app.use('/assignee', assigneeRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

console.log("Server initialized.");

var fun = require("./arn_exe/exe_running").fun;

fun();

module.exports = app;
