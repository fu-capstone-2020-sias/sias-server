var exec = require('child_process').execFile;

var fun = function () {
    console.log("ARN module for Cutting Instruction started.");
    exec('./arn_exe/ARN.exe', function (err, data) {
        console.log(err)
        console.log(data.toString());
    });
}

module.exports.fun = fun;
