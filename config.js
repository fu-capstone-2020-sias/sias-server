module.exports = {
  // AZURE_DB: {
  //   DATABASE_HOST: "",
  //   DATABASE_USERNAME: "",
  //   DATABASE_PASSWORD: "",
  //   DATABASE_SCHEMA: "sias_db"
  // },
  AZURE_DB: {
    DATABASE_HOST: "",
    DATABASE_USERNAME: "",
    DATABASE_PASSWORD: "",
    DATABASE_SCHEMA: "sias_db"
  },
  SECRET_KEY: "",
  SEND_GRID_API_KEY: ""
};
