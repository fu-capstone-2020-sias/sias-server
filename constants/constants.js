module.exports = {
    DEFAULT_ITEM_PER_PAGE: 10,
    NOT_EXIST_IN_DATABASE: 'not existed in database',
    SCREEN_ID: {
        WEB_UC08: "WEB_UC08", // Modify existing account
        WEB_UC13: "WEB_UC13", // Modify account information
        WEB_UC30: "WEB_UC30", // Add new Order
        WEB_UC33: "WEB_UC33", // Modify Order Information
        WEB_UC38: "WEB_UC38", // Modify Order Status
        WEB_UC43: "WEB_UC43" // Add new Order Item
    },
    ROLE_ID: {
        R001: "R001", // Admin
        R002: "R002", // Office Manager 
        R003: "R003", // Factory Manager
        R004: "R004" // Office Employee
    },
    ROLE_NAME: {
        R001: "Administrator", // Admin
        R002: "Office Manager", // Office Manager
        R003: "Factory Manager", // Factory Manager
        R004: "Office Employee" // Office Employee
    },
    ORDER_STATUS_ID: {
        OS001: "OS001", // Pending
        OS002: "OS002", // Approved
        OS003: "OS003", // Processing
        OS004: "OS004", // Completed
        OS005: "OS005" // Rejected
    },
    ORDER_STATUS_NAME: {
        OS001: "Pending", // Pending
        OS002: "Approved", // Approved
        OS003: "Processing", // Processing
        OS004: "Completed", // Completed
        OS005: "Rejected" // Rejected
    },
    DATE_FORMAT_STOCKPRODUCT: 'yyyy-mm-dd',
    ALLOWED_ORIGINS: [
        'http://localhost:3001',
        'http://localhost:3000',
        'http://localhost:9000',
        'https://fu-sias-db.herokuapp.com',
    ],
    SOCKET_SERVER: {
        DOMAIN_LOCAL: "127.0.0.1",
        PORT: 9000
    },
    STOCK_PRODUCT_PRICE_RANGE: {
        lower: 0,
        upper: 1e10 - 1
    },
    STOCK_PRODUCT_LENGTH_RANGE: {
        lower: 0,
        upper: 1e5 - 1
    },
    UPLOAD_IMAGE:{
        BCP: 'bcp',
        NIKKEN: 'nikken',
        NIKKEN_DOC: 'nikken-doc',
        NSMP: 'nsmp',
        EN: 'en' 
    }
}