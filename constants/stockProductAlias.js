module.exports = {
    stockProductId: {
        eng: 'Stock product code'
    },
    sku: {
        eng: 'Stock keeping unit (sku)'
    },
    barcode: {
        eng: 'Stock product barcode'
    },
    type: {
        eng: 'Stock product steel type'
    },
    name: {
        eng: 'Stock product name'
    },
    price: {
        eng: 'Stock product price'
    },
    length: {
        eng: 'Stock product length'
    },
    stockGroupId:{
        eng: 'Stock group code'
    },
    standard: {
        eng: 'Stock standard'
    },
    statusId: {
        eng: 'Stock status code'
    },
    manufacturedDate: {
        eng: 'Stock product manufacture date'
    },
    warehouseId: {
        eng: 'Stock product warehouse code'
    },
    creator: {
        eng: 'Stock product creator'
    },
    createdDate: {
        eng: 'Stock product created date'
    },
    updatedDate:{
        eng: 'Stock product updated date'
    },
    updater: {
        eng: 'Stock product updater'
    },
    isBatchAdded: {
        eng: 'Stock product added from batch status'
    },
    stockProductImportBatchId: {
        eng: 'Stock product import batch ID'
    },
    fromPrice: {
        eng: 'From price of stock product'
    },
    toPrice: {
        eng: 'To price of stock product'
    },
    fromLength: {
        eng: 'From length of stock product'
    },
    toLength: {
        eng: 'To length of stock product'
    }
}