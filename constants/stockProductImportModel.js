module.exports = [
    {
        colName: 'sku',
        eng: 'Stock keeping unit (sku)'
    },
    {
        colName: 'barcode',
        eng: 'Stock product barcode'
    },
    {
        colName: 'type',
        eng: 'Stock product steel type'
    },
    {
        colName: 'name',
        eng: 'Stock product name'
    },
    {
        colName: 'price',
        eng: 'Stock product price'
    },
    {
        colName: 'length',
        eng: 'Stock product length'
    },
    {
        colName: 'stockGroupId',
        eng: 'Stock group code'
    },
    {
        colName: 'standard',
        eng: 'Stock standard'
    },
    {
        colName: 'statusId',
        eng: 'Stock status code'
    },
    {
        colName: 'manufacturedDate',
        eng: 'Stock product manufacture date'
    },
    {
        colName: 'warehouseId',
        eng: 'Stock product warehouse code'
    },
    // {
    //     colName: 'creator',
    //     eng: 'Stock product creator'
    // },
    // {
    //     colName: 'createdDate',
    //     eng: 'Stock product created date'
    // },
    // {
    //     colName: 'updatedDate',
    //     eng: 'Stock product updated date'
    // },
    // {
    //     colName: 'updater',
    //     eng: 'Stock product updater'
    // },
    // {
    //     colName: 'isBatchAdded',
    //     eng: 'Stock product added from batch status'
    // },
    // {
    //     colName: 'stockProductImportBatchId',
    //     eng: 'Stock product import batch ID'
    // }
]