module.exports = {
    warehouseId: {
        eng: 'Warehouse identifier'
    },
    name: {
        eng: 'Warehouse name'
    },
    cityProvinceId: {
        eng: 'Warehouse City/Provincy code'
    },
    districtId: {
        eng: 'Warehouse district code'
    },
    wardCommuneId: {
        eng: 'Warehouse ward commune code'
    }, 
    address: {
        eng: ''
    },
    parentDepartmentId: {
        eng: 'Warehouse parent Department code'
    }, 
    headOfWarehouse: {
        eng: 'Warehouse supervisor'
    },
}