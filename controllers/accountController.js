const db = require("../db");
const Sequelize = require("sequelize");
const Account = require("../models/account")(db, Sequelize.DataTypes);
const passwordUtils = require("../utils/passwordUtils");
const {ROLE_ID} = require("../constants/constants");
const {SCREEN_ID, DEFAULT_ITEM_PER_PAGE} = require("../constants/constants");
const Role = require("../models/role")(db, Sequelize.DataTypes);
const Department = require("../models/department")(db, Sequelize.DataTypes);
const CityProvince = require("../models/cityprovince")(db, Sequelize.DataTypes);
const District = require("../models/district")(db, Sequelize.DataTypes);
const WardCommune = require("../models/wardcommune")(db, Sequelize.DataTypes);

// Support for VIEW ACCOUNT INFORMATION
module.exports.getAccountByUsername = async (req, res) => {
    console.log("Check if req.user is bounded")
    console.log(req.user);

    let sql = `SELECT \`account\`.\`userName\`,
    \`account\`.\`hashedPassword\`,
    \`account\`.\`roleId\`,
    \`account\`.\`departmentId\`,
    \`account\`.\`firstName\`,
    \`account\`.\`lastName\`,
    \`account\`.\`gender\`,
    \`account\`.\`dob\`,
    \`account\`.\`email\`,
    \`account\`.\`phoneNumber\`,
    \`account\`.\`cityProvinceId\`,
    (SELECT name FROM cityprovince where cityProvinceId = \`account\`.\`cityProvinceId\`) as cityProvince,
    \`account\`.\`districtId\`,
    (SELECT name FROM district where districtId = \`account\`.\`districtId\`) as district,
    \`account\`.\`wardCommuneId\`,
    (SELECT name FROM wardcommune where wardCommuneId = \`account\`.\`wardCommuneId\`) as wardCommune,
    \`account\`.\`address\`,
    \`account\`.\`companyInfoId\`,
    \`account\`.\`createdDate\`,
    \`account\`.\`updatedDate\`,
    \`account\`.\`disableFlag\`
    FROM \`sias_db\`.\`account\`
    WHERE userName = :userName
`;
    let account = await db.query(sql, {
        replacements: {userName: req.params.userName},
        type: db.QueryTypes.SELECT,
    });

    if (req.params.login) {
        return account[0];
    } else {
        if (account[0] === null || account[0] === undefined) {
            res.json({
                account: null
            });
            return;
        }

        delete account[0].hashedPassword; // Hide salt + hash from Client (less risk)

        for (let i = 0; i < account.length; i++) {
            let tempObj = {}
            tempObj.dataValues = account[i];
            account[i] = tempObj;
        }

        account = await convertForeignIdToValue(account);

        for (let i = 0; i < account.length; i++) {
            account[i] = account[i].dataValues;
        }

        res.json({
            account: account[0]
        });
    }
};

// Support for get number of rows in database, then calculate how many pages needed for pagination
async function getNumberOfRows(sql, replacements) {
    let numberOfRows = await db.query(sql, {
        type: db.QueryTypes.SELECT,
        replacements: replacements
    })

    console.log(numberOfRows);

    return numberOfRows;
}

async function convertForeignIdToValue(accountList) {
    let roleIdMap = new Map();
    let departmentIdMap = new Map();
    let cityProvinceIdMap = new Map();
    let districtIdMap = new Map();
    let wardCommuneIdMap = new Map();

    for (let i = 0; i < accountList.length; i++) {
        let roleId = accountList[i].dataValues.roleId;
        let departmentId = accountList[i].dataValues.departmentId;
        let cityProvinceId = accountList[i].dataValues.cityProvinceId;
        let districtId = accountList[i].dataValues.districtId;
        let wardCommuneId = accountList[i].dataValues.wardCommuneId;

        if (roleIdMap.get(roleId) === undefined) {
            let tempObj = await Role.findByPk(roleId);
            roleIdMap.set(roleId, tempObj.name);
            accountList[i].dataValues.role = tempObj.name;
        } else {
            let tempName = roleIdMap.get(roleId);
            accountList[i].dataValues.role = tempName;
        }

        if (departmentIdMap.get(roleId) === undefined) {
            let tempObj = await Department.findByPk(departmentId);
            departmentIdMap.set(departmentId, tempObj.name);
            accountList[i].dataValues.department = tempObj.name;
        } else {
            let tempName = departmentIdMap.get(departmentId);
            accountList[i].dataValues.department = tempName;
        }

        if (cityProvinceIdMap.get(cityProvinceId) === undefined) {
            let tempObj = await CityProvince.findByPk(cityProvinceId);
            cityProvinceIdMap.set(cityProvinceId, tempObj.name);
            accountList[i].dataValues.cityProvince = tempObj.name;
        } else {
            let tempName = cityProvinceIdMap.get(cityProvinceId);
            accountList[i].dataValues.cityProvince = tempName;
        }

        if (districtIdMap.get(districtId) === undefined) {
            let tempObj = await District.findByPk(districtId);
            districtIdMap.set(districtId, tempObj.name);
            accountList[i].dataValues.district = tempObj.name;
        } else {
            let tempName = districtIdMap.get(districtId);
            accountList[i].dataValues.district = tempName;
        }

        if (wardCommuneIdMap.get(wardCommuneId) === undefined) {
            let tempObj = await WardCommune.findByPk(wardCommuneId);
            wardCommuneIdMap.set(wardCommuneId, tempObj.name);
            accountList[i].dataValues.wardCommune = tempObj.name;
        } else {
            let tempName = wardCommuneIdMap.get(wardCommuneId);
            accountList[i].dataValues.wardCommune = tempName;
        }
    }

    return accountList;
}

// Support for PAGINATION, default for VIEW ACCOUNT LIST
module.exports.getAccountList = async (req, res) => {
    const resPerPage = DEFAULT_ITEM_PER_PAGE;
    const page = req.query.page || 1;
    let offset = resPerPage * page - resPerPage;
    let rowCount = resPerPage;

    let numberOfRows;
    let sqlCountRows = `SELECT COUNT(userName) as count FROM account`;
    let resp = await getNumberOfRows(sqlCountRows, {});
    console.log("numberOfRows");
    console.log(resp[0].count);

    numberOfRows = resp[0].count;

    let numberOfPages = Math.ceil(numberOfRows / resPerPage);

    // Hide salt + hash from Client (less risk)
    let sql = `SELECT 
    ROW_NUMBER() OVER (
      ORDER BY userName
    ) row_num, 
        acc.\`userName\`,
        acc.\`roleId\`,
        acc.\`departmentId\`,
        acc.\`firstName\`,
        acc.\`lastName\`,
        acc.\`gender\`,
        acc.\`dob\`,
        acc.\`email\`,
        acc.\`phoneNumber\`,
        acc.\`cityProvinceId\`,
        acc.\`districtId\`,
        acc.\`wardCommuneId\`,
        acc.\`address\`,
        acc.\`companyInfoId\`,
        acc.\`createdDate\`,
        acc.\`updatedDate\`,
        acc.\`disableFlag\`
    FROM sias_db.account acc 
    ORDER BY userName 
    LIMIT :offset, :rowCount`;

    try {
        let accountList = await db.query(sql, {
            type: db.QueryTypes.SELECT,
            replacements: {
                offset: offset,
                rowCount: rowCount,
            },
        })

        for (let i = 0; i < accountList.length; i++) {
            let tempObj = {}
            tempObj.dataValues = accountList[i];
            accountList[i] = tempObj;
        }

        accountList = await convertForeignIdToValue(accountList);

        for (let i = 0; i < accountList.length; i++) {
            accountList[i] = accountList[i].dataValues;
        }

        res.json({
            accounts: accountList,
            numberOfPages: numberOfPages,
            totalEntries: numberOfRows
        })
    } catch (error) {
        res.statusCode = 500;
        res.json({error: [error]})
    }
};

// Support for SEARCH function, also PAGINATION
module.exports.getAccountListByParams = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    const resPerPage = DEFAULT_ITEM_PER_PAGE;
    const page = req.query.page || 1;
    let offset = resPerPage * page - resPerPage;
    let rowCount = resPerPage;

    let sqlCountRows = `SELECT COUNT(userName) as count FROM account`;


    // Get fields to search
    let fieldsToSearch = req.body;

    let replacements = {}

    // Hide salt + hash from Client (less risk)
    let sql = `SELECT 
    ROW_NUMBER() OVER (
      ORDER BY userName
    ) row_num,
        acc.\`userName\`,
        acc.\`roleId\`,
        acc.\`departmentId\`,
        acc.\`firstName\`,
        acc.\`lastName\`,
        acc.\`gender\`,
        acc.\`dob\`,
        acc.\`email\`,
        acc.\`phoneNumber\`,
        acc.\`cityProvinceId\`,
        acc.\`districtId\`,
        acc.\`wardCommuneId\`,
        acc.\`address\`,
        acc.\`companyInfoId\`,
        acc.\`createdDate\`,
        acc.\`updatedDate\`,
        acc.\`disableFlag\`
    FROM sias_db.account acc
   `;

    let sqlWhere = ` WHERE `;
    let checkComma = 0;

    if (fieldsToSearch.userName !== undefined && fieldsToSearch.userName !== "") {
        sqlWhere += `userName LIKE :userName`;
        checkComma++;
        replacements.userName = `%` + (fieldsToSearch.userName.trim()) + `%`;
    }
    if (fieldsToSearch.fullName !== undefined && fieldsToSearch.fullName !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `CONCAT(firstName, " ", lastName) LIKE :fullName`;
        checkComma++;
        replacements.fullName = `%` + (fieldsToSearch.fullName.trim()) + `%`;
    }
    if (fieldsToSearch.email !== undefined && fieldsToSearch.email !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `email LIKE :email`;
        checkComma++;
        replacements.email = `%` + (fieldsToSearch.email.trim()) + `%`;
    }
    if (fieldsToSearch.phoneNumber !== undefined && fieldsToSearch.phoneNumber !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `phoneNumber LIKE :phoneNumber`;
        checkComma++;
        replacements.phoneNumber = `%` + (fieldsToSearch.phoneNumber.trim()) + `%`;
    }
    if (fieldsToSearch.roleId !== undefined && fieldsToSearch.roleId !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `roleId = :roleId`;
        checkComma++;
        replacements.roleId = fieldsToSearch.roleId.trim();
    }
    if (fieldsToSearch.disableFlag !== undefined && fieldsToSearch.disableFlag !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `disableFlag = :disableFlag`;
        checkComma++;
        replacements.disableFlag = fieldsToSearch.disableFlag.trim();
    }

    let sqlTail = `
   ORDER BY userName 
   LIMIT :offset, :rowCount`;

    if (sqlWhere !== ` WHERE `)
        sql += sqlWhere;

    sql += sqlTail;

    if (sqlWhere !== ` WHERE `)
        sqlCountRows += sqlWhere;

    console.log(sql);

    let numberOfRows;
    let resp = await getNumberOfRows(sqlCountRows, replacements);
    console.log("numberOfRows");
    console.log(resp[0].count);

    numberOfRows = resp[0].count;

    let numberOfPages = Math.ceil(numberOfRows / resPerPage);

    replacements.offset = offset;
    replacements.rowCount = rowCount;

    try {
        let accountList = await db.query(sql, {
            type: db.QueryTypes.SELECT,
            replacements: replacements,
        })

        for (let i = 0; i < accountList.length; i++) {
            let tempObj = {}
            tempObj.dataValues = accountList[i];
            accountList[i] = tempObj;
        }

        accountList = await convertForeignIdToValue(accountList);

        for (let i = 0; i < accountList.length; i++) {
            accountList[i] = accountList[i].dataValues;
        }

        res.json({
            accounts: accountList,
            numberOfPages: numberOfPages,
            totalEntries: numberOfRows
        })
    } catch (error) {
        res.statusCode = 500;
        res.json({error: [error]})
    }
};

async function validateAccountFields(req, account, action, screen_id) {
    let result = await Account.findByPk(account.userName);
    let messageObj = {
        message: []
    }

    let checkRoleAdmin = req.user.roleId === ROLE_ID.R001;

    if (account.userName === undefined || account.userName === "" || !/^[a-z0-9]{0,20}$/.test(account.userName)) {
        messageObj.message.push("Invalid Username. Please provide Username again.\n");
        return messageObj;
    }

    if (result && action === "INSERT_ACCOUNT") {
        messageObj.message.push("Account with userName: " +
            account.userName + " existed.\n")
        return messageObj;
    }

    if (!result && action === "UPDATE_ACCOUNT") {
        messageObj.message.push("Account with userName: " +
            account.userName + " NOT existed.\n")
        return messageObj;
    }

    if (result && action === "UPDATE_ACCOUNT") {
        if (screen_id === SCREEN_ID.WEB_UC08) {
            if (account.isChangePassword !== undefined && account.isChangePassword === "1") {

                // Tránh trường hợp Admin chỉnh sửa password của thằng khác
                if (result.userName !== req.user.userName) {
                    messageObj.message.push("401 - Unauthorized Access.");
                    return messageObj;
                }

                let i = 0;
                if (account.oldPassword === undefined || account.oldPassword === "") {
                    messageObj.message.push("Invalid old password. Please enter again.\n")
                    i = 1;
                }
                let isOldPasswordValid;
                if (i === 0) {
                    isOldPasswordValid = await passwordUtils.isValidPassword(account.oldPassword, result.hashedPassword);

                    if (!isOldPasswordValid) {
                        messageObj.message.push("Invalid old password. Please enter again.\n")
                    }
                }
            }
        }
    }

    if (screen_id === SCREEN_ID.WEB_UC08 || action === "INSERT_ACCOUNT") {
        if (account.isChangePassword !== undefined && account.isChangePassword === "1") {

            // Tránh trường hợp Admin chỉnh sửa password của thằng khác
            if (result !== null) {
                if (result.userName !== req.user.userName) {
                    messageObj.message.push("401 - Unauthorized Access.");
                    return messageObj;
                }
            }

            let i = 0;
            if (account.password === undefined || account.password === "") {
                messageObj.message.push("Invalid current password. Please enter again.\n")
                i = 1;
            }

            if (i === 0) {
                const isPasswordMeetReq = passwordUtils.validatePasswordRequirements(account.password);

                if (!isPasswordMeetReq) {
                    messageObj.message.push("Password must meet minimum " +
                        "eight characters, at least one uppercase letter, " +
                        "one lowercase letter, " +
                        "one number and one special character (@, $, !, %, *, ?, &).\n")
                }

                if (result) {
                    const isNewPasswordDuplicated = await passwordUtils.isValidPassword(account.password, result.hashedPassword);
                    if (isNewPasswordDuplicated) {
                        messageObj.message.push("New password can not be the same as old password. Please enter again.\n")
                    }
                }
            }
        }
    }

    if (checkRoleAdmin) {
        if (account.roleId === undefined || account.roleId === "" || !/^R00[0-4]$/.test(account.roleId)) {
            if (screen_id !== SCREEN_ID.WEB_UC13)
                messageObj.message.push("Invalid Role. Please provide Role again.")
        }

        if (account.departmentId === undefined || account.departmentId === "" || !/^DEPT0[0-9][0-9]$/.test(account.departmentId)) {
            if (screen_id !== SCREEN_ID.WEB_UC13)
                messageObj.message.push("Invalid Department. Please provide Department again.")
        }

        // if (account.companyInfoId === undefined || account.companyInfoId === "" || !/^COM[0-9][0-9][0-9]$/.test(account.companyInfoId)) {
        //     if (screen_id !== SCREEN_ID.WEB_UC13)
        //         messageObj.message.push("Invalid Company. Please provide Company again.")
        // }

        if (account.disableFlag === undefined || account.disableFlag === "" || !/^[0-1]$/.test(account.disableFlag)) {
            if (screen_id !== SCREEN_ID.WEB_UC13)
                messageObj.message.push("Invalid Disable Flag. Please provide Disable Flag again.")
        }

        if (account.firstName === undefined || account.firstName === ""
            || !(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z ]+$/.test(account.firstName))
        ) {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid First Name. Please provide First Name again.\n")
        }

        if (account.lastName === undefined || account.lastName === ""
            || !(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z ]+$/.test(account.lastName))) {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid Last Name. Please provide Last Name again.\n")
        }

        if (account.gender === undefined || account.gender === "" || !/^[0-1]$/.test(account.gender)) {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid Gender. Please provide Gender again.\n")
        }

        if (account.dob === undefined || account.dob === ""
            || !(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/.test(account.dob))) {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid Date of Birth. Please provide Date of Birth again.\n")
        }

        // Email do công ty cấp, vậy nên trường này chỉ nên hiển thị chứ không chỉnh sửa
        if (account.email === undefined || account.email === ""
            || !(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(account.email))) {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid Email. Please provide Email again.\n")
        } else {
            const sql = `SELECT COUNT(userName) as count FROM sias_db.account WHERE email = :email`;
            const isEmailDuplicated = await db.query(sql, {
                type: db.QueryTypes.SELECT,
                replacements: {
                    email: account.email.trim()
                }
            });
            const sql2 = `SELECT userName FROM sias_db.account WHERE email = :email`;
            const userName = await db.query(sql2, {
                type: db.QueryTypes.SELECT,
                replacements: {
                    email: account.email.trim()
                }
            });

            if (userName[0] !== undefined && userName[0].userName !== account.userName && isEmailDuplicated[0].count >= 1) {
                messageObj.message.push("Email is duplicated. Please provide Email again.\n")
            }
        }

        if (account.phoneNumber === undefined || account.phoneNumber === ""
            || !(/^((09|03|07|08|05)+([0-9]{8})\b)$/.test(account.phoneNumber))) {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid Phone Number. Please provide Phone Number again.\n")
        } else {
            const sql = `SELECT COUNT(userName) as count FROM sias_db.account WHERE phoneNumber = :phoneNumber`;
            const isPhoneNumberDuplicated = await db.query(sql, {
                type: db.QueryTypes.SELECT,
                replacements: {
                    phoneNumber: account.phoneNumber.trim()
                }
            });
            const sql2 = `SELECT userName FROM sias_db.account WHERE phoneNumber = :phoneNumber`;
            const userName = await db.query(sql2, {
                type: db.QueryTypes.SELECT,
                replacements: {
                    phoneNumber: account.phoneNumber.trim()
                }
            });

            if (userName[0] !== undefined && userName[0].userName !== account.userName && isPhoneNumberDuplicated[0].count >= 1) {
                messageObj.message.push("Phone Number is duplicated. Please provide Phone Number again.\n")
            }
        }

        if (account.cityProvinceId === undefined || account.cityProvinceId === ""
            // ||
            // !/^CTY0[0-6][0-9]$/.test(account.cityProvinceId)
        ) {
            if (screen_id === SCREEN_ID.WEB_UC08) {

                messageObj.message.push("Invalid City/Province. Please provide City/Province again.\n")
            }
        } else {
            if (screen_id === SCREEN_ID.WEB_UC08) {
                const result = await CityProvince.findByPk(account.cityProvinceId);

                if (!result) {
                    messageObj.message.push("City/Province is not existed! Please provide City/Province again.\n")
                }
            }
        }

        if (account.districtId === undefined || account.districtId === ""
            // || !/^DST0[0-5][0-9]$/.test(account.districtId)
        ) {
            if (screen_id === SCREEN_ID.WEB_UC08) {

                messageObj.message.push("Invalid District. Please provide District again.\n")
            }
        } else {
            if (screen_id === SCREEN_ID.WEB_UC08) {
                const result = await District.findByPk(account.districtId);

                if (!result) {
                    messageObj.message.push("District is not existed! Please provide District  again.\n")
                }
            }
        }

        if (account.wardCommuneId === undefined || account.wardCommuneId === ""
            // || !/^WRD0[0-5][0-9]$/.test(account.wardCommuneId)
        ) {
            if (screen_id === SCREEN_ID.WEB_UC08) {
                messageObj.message.push("Invalid Ward/Commune. Please provide Ward/Commune again.\n")
            }
        } else {
            if (screen_id === SCREEN_ID.WEB_UC08) {
                const result = await WardCommune.findByPk(account.wardCommuneId);

                if (!result) {
                    messageObj.message.push("Ward/Commune is not existed! Please provide Ward/Commune again.\n")
                }
            }
        }

        if (account.address === undefined || account.address === "") {
            if (screen_id === SCREEN_ID.WEB_UC08)
                messageObj.message.push("Invalid Living Address. Please provide Living Address again.\n")
        }
    }

    return messageObj;
}

module.exports.insertNewAccount = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    if (req.body.screen_id === undefined
        || req.body.screen_id === ""
        || req.body.screen_id != SCREEN_ID.WEB_UC08) {
        res.statusCode = 400;
        res.json({
            error: ["Missing screen_id parameter."]
        })
        return;
    }

    let account = req.body;

    let messageObj = await validateAccountFields(req, account, "INSERT_ACCOUNT", "WEB_UC08");

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    const hashedPassword = await passwordUtils.hashBcryptPassword(account.password);

    delete account.password;
    account.hashedPassword = hashedPassword;
    let date = new Date();
    account.createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    account.companyInfoId = req.user.companyInfoId;

    try {
        await Account.create(account);
    } catch (err) {
        res.statusCode = 500;
        res.json({error: [(err)]})
        return;
    }

    res.json({
        message: "Account with userName: " + account.userName
            + " is added into the database."
    });
}

module.exports.updateAccount = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let screen_id = req.body.screen_id; // WEB_UC08, WEB_UC13

    // if (req.user.roleId !== ROLE_ID.R001 &&
    //     (screen_id === SCREEN_ID.WEB_UC08)) {
    //     res.json({
    //         error: ["401 - Unauthorized Access."]
    //     })
    //     return;
    // }

    if (screen_id === undefined || screen_id === "" || screen_id != SCREEN_ID.WEB_UC08) {
        res.statusCode = 400;
        res.json({
            error: ["Missing screen_id parameter."]
        })
        return;
    }


    if (req.user.roleId !== ROLE_ID.R001 &&
        screen_id === SCREEN_ID.WEB_UC08) {
        if (req.body.isChangePassword != undefined && req.body.isChangePassword !== "") {
            if (req.body.isChangePassword === "0") {
                res.statusCode = 401;
                res.json({
                    error: ["401 - Unauthorized Access."]
                })
                return;
            }
        } else { // Thêm vào 2020/11/23 hôm viết test cho Change Password
            res.statusCode = 401;
            res.json({
                error: ["401 - Unauthorized Access."]
            })
            return;
        }
    }

    // Chỉ admin được phép sửa thông tin account
    // if (screen_id === SCREEN_ID.WEB_UC13) {
    //     if (req.body.userName !== req.user.userName) {
    //         res.json({
    //             message: "ATTENTION! You can only edit your account information!"
    //         })
    //         return;
    //     }
    // }

    let destroySessionIfChangePwd = false;
    let account = req.body;

    let messageObj = await validateAccountFields(req, account, "UPDATE_ACCOUNT", screen_id);

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let respAccount = await Account.findByPk(account.userName);
    let checkRoleAdmin = req.user.roleId === ROLE_ID.R001;
    // if () {
    if (screen_id === SCREEN_ID.WEB_UC08 && checkRoleAdmin) {
        // if (account.roleId !== undefined && account.roleId !== "") {
        //     if (account.roleId !== respAccount.roleId)
        //         respAccount.roleId = account.roleId;
        // }

        if (account.departmentId !== undefined && account.departmentId !== "") {
            if (account.departmentId !== respAccount.departmentId)
                respAccount.departmentId = account.departmentId;
        }

        // if (account.companyInfoId !== undefined && account.companyInfoId !== "") {
        //     if (account.companyInfoId !== respAccount.companyInfoId)
        //         respAccount.companyInfoId = account.companyInfoId;
        // }

        if (account.disableFlag !== undefined && account.disableFlag !== "") {
            if (account.disableFlag !== respAccount.disableFlag)
                respAccount.disableFlag = account.disableFlag;
        }
    }
    // else
    if (screen_id === SCREEN_ID.WEB_UC08) {
        // Password được phép đổi bởi tất cả các role
        if (account.isChangePassword !== undefined && account.isChangePassword === '1') {
            if (account.password !== undefined && account.password !== "") {
                const hashedPassword = await passwordUtils.hashBcryptPassword(account.password);
                respAccount.hashedPassword = hashedPassword;
                destroySessionIfChangePwd = true;
            }
        }

        if (checkRoleAdmin) {
            if (account.firstName !== undefined && account.firstName !== "") {
                if (account.firstName !== respAccount.firstName)
                    respAccount.firstName = account.firstName;
            }

            if (account.lastName !== undefined && account.lastName !== "") {
                if (account.lastName !== respAccount.lastName)
                    respAccount.lastName = account.lastName;
            }

            if (account.gender !== undefined && account.gender !== "") {
                if (account.gender !== respAccount.gender.toString())
                    respAccount.gender = account.gender;
            }

            if (account.dob !== undefined && account.dob !== "") {
                if (account.dob !== respAccount.dob)
                    respAccount.dob = account.dob;
            }

            if (account.email !== undefined && account.email !== "") {
                if (account.email !== respAccount.email)
                    respAccount.email = account.email;
            }

            if (account.phoneNumber !== undefined && account.phoneNumber !== "") {
                if (account.phoneNumber !== respAccount.phoneNumber)
                    respAccount.phoneNumber = account.phoneNumber;
            }

            if (account.cityProvinceId !== undefined && account.cityProvinceId !== "") {
                if (account.cityProvinceId !== respAccount.cityProvinceId)
                    respAccount.cityProvinceId = account.cityProvinceId;
            }

            if (account.districtId !== undefined && account.districtId !== "") {
                if (account.districtId !== respAccount.districtId)
                    respAccount.districtId = account.districtId;
            }

            if (account.wardCommuneId !== undefined && account.wardCommuneId !== "") {
                if (account.wardCommuneId !== respAccount.wardCommuneId)
                    respAccount.wardCommuneId = account.wardCommuneId;
            }

            if (account.address !== undefined && account.address !== "") {
                if (account.address !== respAccount.address)
                    respAccount.address = account.address;
            }
        }
    }
    // }

    let date = new Date();
    let tempUserName = respAccount.userName;
    respAccount.updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    let result;
    try {
        result = await respAccount.save();
        if (destroySessionIfChangePwd && tempUserName === req.user.userName) {
            req.logout();
            req.session.destroy();
        }
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
    }
    if (result !== undefined && result !== null) {
        res.json({
            message: "Account with userName: " + account.userName + " updated."
        })
    }
}

module.exports.deleteDisabledAccount = (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    console.log(req.body.arrOfUsernames.toString());
    const usernamesString = req.body.arrOfUsernames.replace(",", ", ");
    const usernames = req.body.arrOfUsernames.split(",");
//    Check if req.body is NOT EMPTY
    if (Object.keys(req.body).length !== 0 && req.body.constructor === Object) {
        Account.destroy({
            where: {userName: usernames, disableFlag: 1}
        }).then((result) => res.json({
            message: result === 0 ? "No account is deleted." :
                "Account with usernames: [" + usernamesString + "], are deleted from SIAS system."
        }))
            .catch((error) => {
                res.statusCode = 500;
                res.json({error: [error]})
            });
    }
}