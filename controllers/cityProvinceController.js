const db = require("../db");
const Sequelize = require("sequelize");
const CityProvince = require('../models/cityprovince')(db, Sequelize.DataTypes)

module.exports = { 
    async getAllCityProvince(){
        const cityProvinces = await CityProvince.findAndCountAll()
        return {
           data: cityProvinces 
        }
    },
    async getCityProvinceBy(where){
        const cityProvinces = await CityProvince.findAndCountAll({
            where
        })
        return {
            data: cityProvinces
        }
    },
    async find(id){
        const res = await this.getCityProvinceBy({cityProvinceId: id})
        return res 
    }
}
