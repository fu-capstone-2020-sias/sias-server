const db = require("../db");
const Sequelize = require("sequelize");
const CompanyInfo = require("../models/companyinfo")(db, Sequelize.DataTypes);
const passwordUtils = require("../utils/passwordUtils");
const CityProvince = require("../models/cityprovince")(db, Sequelize.DataTypes);
const District = require("../models/district")(db, Sequelize.DataTypes);
const WardCommune = require("../models/wardcommune")(db, Sequelize.DataTypes);

// Support for VIEW COMPANY INFORMATION
module.exports.getCompanyInfoById = async (req, res) => {
    if (req.user.companyInfoId !== undefined && req.user.companyInfoId !== "") {
        let result = await CompanyInfo.findByPk(req.user.companyInfoId);
        result = await convertForeignIdToValue(result.dataValues);

        res.json({
                companyInfo: result
            }
        )
    }
}

async function convertForeignIdToValue(companyInfoObj) {
    let cityProvinceIdMap = new Map();
    let districtIdMap = new Map();
    let wardCommuneIdMap = new Map();

    let cityProvinceId = companyInfoObj.cityProvinceId;
    let districtId = companyInfoObj.districtId;
    let wardCommuneId = companyInfoObj.wardCommuneId;

    if (cityProvinceIdMap.get(cityProvinceId) === undefined) {
        let tempObj = await CityProvince.findByPk(cityProvinceId);
        cityProvinceIdMap.set(cityProvinceId, tempObj.name);
        companyInfoObj.cityProvince = tempObj.name;
    } else {
        let tempName = cityProvinceIdMap.get(cityProvinceId);
        companyInfoObj.cityProvince = tempName;
    }

    if (districtIdMap.get(districtId) === undefined) {
        let tempObj = await District.findByPk(districtId);
        districtIdMap.set(districtId, tempObj.name);
        companyInfoObj.district = tempObj.name;
    } else {
        let tempName = districtIdMap.get(districtId);
        companyInfoObj.district = tempName;
    }

    if (wardCommuneIdMap.get(wardCommuneId) === undefined) {
        let tempObj = await WardCommune.findByPk(wardCommuneId);
        wardCommuneIdMap.set(wardCommuneId, tempObj.name);
        companyInfoObj.wardCommune = tempObj.name;
    } else {
        let tempName = wardCommuneIdMap.get(wardCommuneId);
        companyInfoObj.wardCommune = tempName;
    }

    return companyInfoObj;
}

async function validateCompanyInfoFields(companyInfo) {
    let result = await CompanyInfo.findByPk(companyInfo.companyInfoId);
    let messageObj = {
        message: []
    }

    if (!result) {
        messageObj.message.push("Company with ID: " +
            companyInfo.companyInfoId + " NOT existed.\n")
        return messageObj;
    }

    if (companyInfo.name === undefined || companyInfo.name === ""
        || !(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]+$/.test(companyInfo.name))
    ) {

        messageObj.message.push("Invalid Company Name. Please provide Company Name again.\n")
    }

    if (companyInfo.cityProvinceId === undefined || companyInfo.cityProvinceId === "") {
        messageObj.message.push("Invalid City/Province. Please provide City Province again.\n")
    } else {
        const result = await CityProvince.findByPk(companyInfo.cityProvinceId);

        if (!result) {
            messageObj.message.push("City/Province is not existed! Please provide City/Province again.\n")
        }
    }

    if (companyInfo.districtId === undefined || companyInfo.districtId === "") {
        messageObj.message.push("Invalid District. Please provide District again.\n")
    } else {
        const result = await District.findByPk(companyInfo.districtId);

        if (!result) {
            messageObj.message.push("District is not existed! Please provide District again.\n")
        }
    }

    if (companyInfo.wardCommuneId === undefined || companyInfo.wardCommuneId === "") {
        messageObj.message.push("Invalid Ward/Commune. Please provide Ward/Commune again.\n")
    } else {
        const result = await WardCommune.findByPk(companyInfo.wardCommuneId);

        if (!result) {
            messageObj.message.push("Ward/Commune is not existed! Please provide Ward/Commune again.\n")
        }
    }

    if (companyInfo.address === undefined || companyInfo.address === "") {

        messageObj.message.push("Invalid Working Address. Please provide Working Address again.\n")
    }

    if (companyInfo.phoneNumber === undefined || companyInfo.phoneNumber === ""
        || !(/^((09|03|07|08|05)+([0-9]{8})\b)$/.test(companyInfo.phoneNumber))) {

        messageObj.message.push("Invalid Phone Number. Please provide Phone Number again.\n")
    }

    if (companyInfo.email === undefined || companyInfo.email === ""
        || !(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(companyInfo.email))) {

        messageObj.message.push("Invalid Email. Please provide Email again.\n")
    }

    if (companyInfo.notes === undefined || companyInfo.notes === "") {

        messageObj.message.push("Invalid Description. Please provide Description again.\n")
    }

    if (companyInfo.companyWebsite === undefined || companyInfo.companyWebsite === "") {

        messageObj.message.push("Invalid Company Website. Please provide Company Website again.\n")
    }

    return messageObj;
}

module.exports.updateCompanyInfo = async (req, res) => {
    let companyInfo = req.body;
    req.body.companyInfoId = req.user.companyInfoId;
    let messageObj = await validateCompanyInfoFields(companyInfo);

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let companyInfoResp = await CompanyInfo.findByPk(req.user.companyInfoId);
    if (companyInfo.name !== undefined && companyInfo.name !== "") {
        if (companyInfo.name !== companyInfoResp.name)
            companyInfoResp.name = companyInfo.name;
    }
    if (companyInfo.cityProvinceId !== undefined && companyInfo.cityProvinceId !== "") {
        if (companyInfo.cityProvinceId !== companyInfoResp.cityProvinceId)
            companyInfoResp.cityProvinceId = companyInfo.cityProvinceId;
    }
    if (companyInfo.districtId !== undefined && companyInfo.districtId !== "") {
        if (companyInfo.districtId !== companyInfoResp.districtId)
            companyInfoResp.districtId = companyInfo.districtId;
    }
    if (companyInfo.wardCommuneId !== undefined && companyInfo.wardCommuneId !== "") {
        if (companyInfo.wardCommuneId !== companyInfoResp.wardCommuneId)
            companyInfoResp.wardCommuneId = companyInfo.wardCommuneId;
    }
    if (companyInfo.address !== undefined && companyInfo.address !== "") {
        if (companyInfo.address !== companyInfoResp.address)
            companyInfoResp.address = companyInfo.address;
    }
    if (companyInfo.phoneNumber !== undefined && companyInfo.phoneNumber !== "") {
        if (companyInfo.phoneNumber !== companyInfoResp.phoneNumber)
            companyInfoResp.phoneNumber = companyInfo.phoneNumber;
    }
    if (companyInfo.email !== undefined && companyInfo.email !== "") {
        if (companyInfo.email !== companyInfoResp.email)
            companyInfoResp.email = companyInfo.email;
    }
    if (companyInfo.notes !== undefined && companyInfo.notes !== "") {
        if (companyInfo.notes !== companyInfoResp.notes)
            companyInfoResp.notes = companyInfo.notes;
    }
    if (companyInfo.companyWebsite !== undefined && companyInfo.companyWebsite !== "") {
        if (companyInfo.companyWebsite !== companyInfoResp.companyWebsite)
            companyInfoResp.companyWebsite = companyInfo.companyWebsite;
    }

    let date = new Date();
    companyInfoResp.updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    companyInfoResp.updater = req.user.userName;

    let result;
    try {
        result = await companyInfoResp.save();
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
    if (result !== undefined) {
        res.json({
            message: "Company with ID: " + companyInfo.companyInfoId + " updated."
        })
    }
}

