const db = require("../db");
const Sequelize = require("sequelize");
const CuttingInstruction = require("../models/cuttinginstruction")(db, Sequelize.DataTypes);
const {ROLE_ID} = require("../constants/constants");
const {SCREEN_ID, DEFAULT_ITEM_PER_PAGE} = require("../constants/constants");

calculateSumOfStockproduct = async (replacements) => {
    let sql = `SELECT SUM(res.length) as countEnough FROM (
SELECT 
                stockProductId, length 
        FROM 
                sias_db.stockproduct st
   INNER JOIN
   sias_db.warehouse wh
    ON st.warehouseId = wh.warehouseId
        WHERE 
            (st.length >= :minLength AND st.length <= :maxLength)
            AND wh.cityProvinceId = :cityProvinceId
            AND wh.districtId = :districtId
            AND wh.wardCommuneId = :wardCommuneId
            ) as res`;

    if (replacements.cityProvinceId === undefined) {
        sql = sql.replace("AND wh.cityProvinceId = :cityProvinceId", "");
    }

    if (replacements.districtId === undefined) {
        sql = sql.replace("AND wh.districtId = :districtId", "");
    }

    if (replacements.wardCommuneId === undefined) {
        sql = sql.replace("AND wh.wardCommuneId = :wardCommuneId", "");
    }

    let result = await db.query(sql, {
        type: db.QueryTypes.SELECT,
        replacements
    });

    return result;
}

// Get Stock Product in nearest warehouse for each item
module.exports.getStockProductAvailableForCutting = async (req, res) => {
    try {
        let sql = `
        SELECT 
                stockProductId, length 
        FROM 
                sias_db.stockproduct st
         
         INNER JOIN
                 sias_db.warehouse wh
            ON st.warehouseId = wh.warehouseId
         WHERE 
            (st.length >= :minLength AND st.length <= :maxLength)
            AND wh.cityProvinceId = :cityProvinceId
            AND wh.districtId = :districtId
            AND wh.wardCommuneId = :wardCommuneId
            `;
        let sum = req.body.sumOfOrderItem;
        let replacements = {
            minLength: req.body.minLength,
            maxLength: req.body.maxLength,
            cityProvinceId: req.body.cityProvinceId,
            districtId: req.body.districtId,
            wardCommuneId: req.body.wardCommuneId
        }

        // Lấy stock product tư warehoue gần nhất
        // Thứ tự từ CTY, DST, WRD ---> CTY, DST --> CTY ---> All
        let i = 0;
        while (true) {
            let tempSum = await calculateSumOfStockproduct(replacements);

            if (tempSum[0].countEnough >= sum) {
                break;
            }

            if (i === 0) {
                delete replacements.wardCommuneId;
                sql = sql.replace("AND wh.wardCommuneId = :wardCommuneId", "");
            } else if (i === 1) {
                delete replacements.districtId;
                sql = sql.replace("AND wh.districtId = :districtId", "");
                // } else if (i === 2) {
                //     delete replacements.cityProvinceId;
                //     sql = sql.replace("AND wh.cityProvinceId = :cityProvinceId", "");
                // } else if (i === 3) {
                //     return {
                //         message: "There are not enough stock products in all warehouses!"
                //     };
                // }
            } else if (i === 2) {
                return {
                    message: "There are not enough stock products in customer's province!"
                };
            }

            i++;
        }

        if (req.body.stockProductIdSelectedArr.length > 0) {
            replacements.stockProductIdSelectedArr = req.body.stockProductIdSelectedArr
                .toString()
                .replace("[", "")
                .replace("]", "");
            sql +=
                `
                AND
                        stockProductId NOT IN (:stockProductIdSelectedArr) `;
        }

        sql += ` ORDER BY
                        st.length
                        LIMIT 15`;

        let result = await db.query(sql, {
            type: db.QueryTypes.SELECT,
            replacements
        });


        return result;
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
        return;
    }
}

module.exports.getCuttingInstructionByOrderId = async (req, res) => {
    try {
        return await CuttingInstruction.findAll({
            where: {
                orderId: req.query.orderId,
                assignee: req.user.userName
            }
        })
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
    }
}

module.exports.getTotalLengthOfStockProduct = async (req, res) => {
    let sql = `SELECT SUM(length) as totalLength FROM sias_db.stockproduct`;

    try {
        return await db.query(sql, {
            type: db.QueryTypes.SELECT
        })
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
    }
}