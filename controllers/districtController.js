const db = require("../db");
const Sequelize = require("sequelize");
const District = require('../models/district')(db, Sequelize.DataTypes)

module.exports = { 
    async getAllDistrictProvince(){
        const districts = await District.findAndCountAll()
        return {
           data: districts 
        }
    },
    async getDistrictBy(where){
        const districts = await District.findAndCountAll({
            where
        })
        return {
            data: districts
        }
    },
    async find(id){
        const res = await this.getDistrictBy({districtId: id})
        return res 
    }
}