const db = require("../db");
const Sequelize = require("sequelize");
const Feedback = require("../models/feedback")(db, Sequelize.DataTypes);
const Account = require("../models/account")(db, Sequelize.DataTypes);
const {DEFAULT_ITEM_PER_PAGE} = require("../constants/constants");

async function getNumberOfRows(sql, replacements) {
    let numberOfRows = await db.query(sql, {
        type: db.QueryTypes.SELECT,
        replacements: replacements
    })

    console.log(numberOfRows);

    return numberOfRows;
}

module.exports.getFeedbackList = async (req, res) => {
    const resPerPage = DEFAULT_ITEM_PER_PAGE;
    const page = req.query.page || 1;
    let offset = resPerPage * page - resPerPage;
    let rowCount = resPerPage;

    let numberOfRows;
    let sqlCountRows;
    let feedbackList;
    let numberOfPages;
    try {
        sqlCountRows = `SELECT COUNT(feedbackId) as count FROM \`feedback\``;
        let resp = await getNumberOfRows(sqlCountRows, {});
        console.log("numberOfRows");
        console.log(resp[0].count);

        numberOfRows = resp[0].count;

        numberOfPages = Math.ceil(numberOfRows / resPerPage);

        feedbackList = await Feedback.findAndCountAll({
            limit: rowCount,
            offset: offset,
            order: [['createdDate', 'DESC'], ['feedbackId', 'DESC']]
        });
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
        return;
    }

    res.json({
        feedbacks: feedbackList.rows,
        numberOfPages: numberOfPages
    })

}

async function validateFeedbackFields(feedback, action, screen_id) {
    let messageObj = {
        message: []
    }

    if (feedback.title === undefined || feedback.title === ""
        || !(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9]+$/.test(feedback.title))
    ) {
        messageObj.message.push("Invalid Title. Please provide Title again.\n")
    }

    if (feedback.description === undefined || feedback.description === "") {
        messageObj.message.push("Invalid Description. Please provide Description again.")
    }

    // if (feedback.senderName === undefined || feedback.senderName === ""
    //     || !(/^[a-z0-9]+$/.test(feedback.senderName))
    // ) {
    //     messageObj.message.push("Invalid Sender Name. Please provide Sender Name again.")
    // } else {
    //     let result = await Account.findByPk(feedback.senderName.trim());
    //     if (!result) {
    //         messageObj.message.push("Sender Name is not EXISTED!");
    //         return messageObj;
    //     }
    // }
    //
    // if (feedback.senderEmail === undefined || feedback.senderEmail === ""
    //     || !(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(feedback.senderEmail))) {
    //     messageObj.message.push("Invalid Sender Email. Please provide Sender Email again.\n")
    // } else {
    //     let result = await Account.findOne({
    //         where: {
    //             userName: feedback.senderName.trim(),
    //             email: feedback.senderEmail.trim()
    //         }
    //     });
    //     if (!result) {
    //         messageObj.message.push("Sender Email is not EXISTED and not MATCHED with sender!");
    //         return messageObj;
    //     }
    // }

    return messageObj;
}

module.exports.insertNewFeedback = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let feedback = req.body;

    let messageObj = await validateFeedbackFields(feedback, "INSERT_FEEDBACK", "");

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let date = new Date();
    feedback.createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    feedback.senderName = req.user.userName;
    feedback.senderEmail = req.user.email;

    try {
        await Feedback.create(feedback);
        res.json({
            message: "An feedback was created successfully."
        })
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
}

