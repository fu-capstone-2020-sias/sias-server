const db = require("../db");
const Sequelize = require("sequelize");
const Order = require("../models/order")(db, Sequelize.DataTypes);
const OrderItem = require("../models/orderitem")(db, Sequelize.DataTypes);
const OrderStatus = require("../models/orderstatus")(db, Sequelize.DataTypes);
const CityProvince = require("../models/cityprovince")(db, Sequelize.DataTypes);
const District = require("../models/district")(db, Sequelize.DataTypes);
const WardCommune = require("../models/wardcommune")(db, Sequelize.DataTypes);
const passwordUtils = require("../utils/passwordUtils");
const {ROLE_ID} = require("../constants/constants");
const {ORDER_STATUS_NAME} = require("../constants/constants");
const {SCREEN_ID} = require("../constants/constants");
const {ORDER_STATUS_ID, DEFAULT_ITEM_PER_PAGE} = require("../constants/constants");


// Support for get number of rows in database, then calculate how many pages needed for pagination
async function getNumberOfRows(sql, replacements) {
    let numberOfRows = await db.query(sql, {
        type: db.QueryTypes.SELECT,
        replacements: replacements
    })

    console.log(numberOfRows);

    return numberOfRows;
}

async function convertForeignIdToValue(orderList) {
    let orderStatusIdMap = new Map();
    let customerCityProvinceIdMap = new Map();
    let customerDistrictIdMap = new Map();
    let customerWardCommuneIdMap = new Map();
    for (let i = 0; i < orderList.length; i++) {
        let orderStatusId = orderList[i].dataValues.orderStatusId;
        let customerCityProvinceId = orderList[i].dataValues.customerCityProvinceId;
        let customerDistrictId = orderList[i].dataValues.customerDistrictId;
        let customerWardCommuneId = orderList[i].dataValues.customerWardCommuneId;

        if (orderStatusIdMap.get(orderStatusId) === undefined) {
            let tempObj = await OrderStatus.findByPk(orderStatusId);
            orderStatusIdMap.set(orderStatusId, tempObj.name);
            orderList[i].dataValues.orderStatus = tempObj.name;
        } else {
            let tempName = orderStatusIdMap.get(orderStatusId);
            orderList[i].dataValues.orderStatus = tempName;
        }

        if (customerCityProvinceIdMap.get(customerCityProvinceId) === undefined) {
            let tempObj = await CityProvince.findByPk(customerCityProvinceId);
            customerCityProvinceIdMap.set(customerCityProvinceId, tempObj.name);
            orderList[i].dataValues.customerCityProvince = tempObj.name;
        } else {
            let tempName = customerCityProvinceIdMap.get(customerCityProvinceId);
            orderList[i].dataValues.customerCityProvince = tempName;
        }

        if (customerDistrictIdMap.get(customerDistrictId) === undefined) {
            let tempObj = await District.findByPk(customerDistrictId);
            customerDistrictIdMap.set(customerDistrictId, tempObj.name);
            orderList[i].dataValues.customerDistrict = tempObj.name;
        } else {
            let tempName = customerDistrictIdMap.get(customerDistrictId);
            orderList[i].dataValues.customerDistrict = tempName;
        }

        if (customerWardCommuneIdMap.get(customerWardCommuneId) === undefined) {
            let tempObj = await WardCommune.findByPk(customerWardCommuneId);
            customerWardCommuneIdMap.set(customerWardCommuneId, tempObj.name);
            orderList[i].dataValues.customerWardCommune = tempObj.name;
        } else {
            let tempName = customerWardCommuneIdMap.get(customerWardCommuneId);
            orderList[i].dataValues.customerWardCommune = tempName;
        }
    }

    return orderList;
}

module.exports.getOrderList = async (req, res) => {
    const resPerPage = DEFAULT_ITEM_PER_PAGE;
    const page = req.query.page || 1;
    let offset = resPerPage * page - resPerPage;
    let rowCount = resPerPage;

    let numberOfRows;
    let sqlCountRows = `SELECT COUNT(orderId) as count FROM \`order\`  WHERE  (\`order\`.\`creator\` IN ('${req.user.userName}') OR \`order\`.\`updater\` IN ('${req.user.userName}') 
    OR \`order\`.\`approver\` IN ('${req.user.userName}') 
    OR \`order\`.\`assignee\` IN ('${req.user.userName}'))
`
    let resp = await getNumberOfRows(sqlCountRows, {});
    console.log("numberOfRows");
    console.log(resp[0].count);

    numberOfRows = resp[0].count;

    let numberOfPages = Math.ceil(numberOfRows / resPerPage);

    let orderList;
    try {
        orderList = await Order.findAll({
            limit: rowCount,
            offset: offset,
            where:
                Sequelize.or(
                    {creator: [req.user.userName]},
                    {updater: [req.user.userName]},
                    {approver: [req.user.userName]},
                    {assignee: [req.user.userName]},
                )

        })

        orderList = await convertForeignIdToValue(orderList);
    } catch (err) {
        res.statusCode = 500;
        res.json({
            err: [err]
        })
        return;
    }

    res.json({
        orders: orderList,
        numberOfPages: numberOfPages,
        totalEntries: numberOfRows
    });
}

module.exports.getOrderById = async (req, res) => {
    let order;
    try {
        if (req.params.orderId !== undefined && req.params.orderId !== "") {
            order = await Order.findAll({
                where:
                    Sequelize.and(
                        {orderId: req.params.orderId},
                        Sequelize.or(
                            {creator: [req.user.userName]},
                            {updater: [req.user.userName]},
                            {approver: [req.user.userName]},
                            {assignee: [req.user.userName]},
                        )
                    )
            })
            order = await convertForeignIdToValue(order);
        }
    } catch (err) {
        res.statusCode = 500;
        res.json({
            message: [err]
        })
        return;
    }

    if (order[0] !== undefined) {
        res.json({
            order: order[0],
        });
    } else {
        res.statusCode = 400;
        res.json({
            error: ["No order with ID: " + req.params.orderId + " to be found."]
        })
    }
}

module.exports.getOrderItemList = async (req, res) => {
    const resPerPage = DEFAULT_ITEM_PER_PAGE;
    const page = req.query.page || 1;
    let offset = resPerPage * page - resPerPage;
    let rowCount = resPerPage;

    let numberOfRows;
    let sqlCountRows = `SELECT COUNT(orderItemId) as count FROM \`orderitem\` INNER JOIN sias_db.order ord
    ON orderItem.orderId = ord.orderId 
    ` + ` WHERE orderItem.orderId = '${req.query.orderId}'`
    if (req.user.roleId === ROLE_ID.R003) { // For Order Instruction screen, need Factory Manager
        sqlCountRows += ` AND ord.assignee = '${req.user.userName}'`;
    }

    let resp = await getNumberOfRows(sqlCountRows, {});
    console.log("numberOfRows");
    console.log(resp[0].count);

    numberOfRows = resp[0].count;

    let numberOfPages = Math.ceil(numberOfRows / resPerPage);

    let sql = `SELECT orderItem.*,
    ord.assignee 
    FROM orderItem INNER JOIN sias_db.order ord
    ON orderItem.orderId = ord.orderId
    WHERE 
    orderItem.orderId = :orderId `

    if (req.user.roleId === ROLE_ID.R003) { // For Order Instruction, need Factory Manager
        sql += `AND 
        ord.assignee = :assignee`;
    }
    sql +=
        `
    ORDER BY orderItem.orderId
    `;

    let replacements = {
        orderId: req.query.orderId,
        assignee: req.user.userName
    };
    if (!req.body.createInstruction) {
        replacements.offset = offset;
        replacements.rowCount = rowCount;

        sql += `LIMIT :offset, :rowCount`;
    }

    try {
        const result = await db.query(sql, {
            type: db.QueryTypes.SELECT,
            replacements: replacements,
        })

        if (req.body.createInstruction) {
            return result;
        } else {
            res.json({
                orderItems: result,
                numberOfPages: numberOfPages
            });
            return;
        }

    } catch (error) {
        res.statusCode = 500;
        res.json({message: [error]})
    }
}

module.exports.insertNewOrderItem = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let orderItem = req.body;

    orderItem.orderItemId = 0; // Order Item ID is auto-generated
    orderItem.orderId = req.params.orderId;

    let order = await Order.findByPk(req.params.orderId);

    order.totalPrice += orderItem.price;

    let messageObj = await validateOrderItemFields(orderItem, "INSERT_ORDER_ITEM", SCREEN_ID.WEB_UC43);

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let date = new Date();
    orderItem.createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    orderItem.creator = req.user.userName;
    try {
        await OrderItem.create(orderItem);

        await order.save();

        res.json({
            message: "An order item was created successfully."
        })
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
}

module.exports.insertNewOrderItemList = async (req, res) => {
    if ((typeof req.body === "object" && Object.keys(req.body).length === 0) || req.body.length === 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let orderItemList = req.body;
    let order = await Order.findByPk(req.params.orderId);

    if (order !== null) {
        if (!/^OS001$/.test(order.orderStatusId)) {
            res.statusCode = 400; // Bad Request
            res.json({
                error: ["Order's status (ID: " + order.orderId + ") must be 'Pending' to be able to do CRUD."]
            })
            return;
        }
    }

    for (let i = 0; i < orderItemList.length; i++) {
        orderItemList[i].orderItemId = 0; // Order Item ID is auto-generated
        orderItemList[i].orderId = req.params.orderId;

        let messageObj = await validateOrderItemFields(orderItemList[i], "INSERT_ORDER_ITEM", SCREEN_ID.WEB_UC43);

        if (messageObj.message.length !== 0) {
            res.statusCode = 400; // Bad Request
            res.json({
                error: messageObj.message
            })
            return;
        }

        order.totalPrice = parseInt(order.totalPrice, 10) +
            (parseInt(orderItemList[i].price, 10) * parseInt(orderItemList[i].quantity, 10));

        let date = new Date();
        orderItemList[i].createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        orderItemList[i].creator = req.user.userName;
    }

    try {
        await OrderItem.bulkCreate(orderItemList);

        await order.save();

        res.json({
            message: "List of order items were created successfully."
        })
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
}

calculateTotalPriceByOrderId = async (orderId) => {
    let sql = `SELECT 
                SUM(price * quantity) as totalPrice 
               FROM sias_db.orderitem 
               WHERE orderId = :orderId`;

    return await db.query(sql, {
        type: db.QueryTypes.SELECT,
        replacements: {
            orderId: orderId
        }
    });
}

module.exports.updateOrderItem = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let orderItem = req.body;
    orderItem.orderItemId = req.params.orderItemId;
    orderItem.orderId = req.params.orderId;

    // let order = await Order.findByPk(req.params.orderId);

    let messageObj = await validateOrderItemFields(orderItem, "UPDATE_ORDER_ITEM", "");

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let orderItemResp = await OrderItem.findByPk(orderItem.orderItemId);
    let orderResp = await Order.findByPk(orderItem.orderId);

    if (orderItem.orderId !== undefined && orderItem.orderId !== "") {
        if (orderItem.orderId !== orderItemResp.orderId)
            orderItemResp.orderId = orderItem.orderId;
    }

    if (orderItem.length !== undefined && orderItem.length !== "") {
        if (orderItem.length !== orderItemResp.length)
            orderItemResp.length = orderItem.length;
    }

    if (orderItem.quantity !== undefined && orderItem.quantity !== "") {
        if (orderItem.quantity !== orderItemResp.quantity) { // Số lượng của một tiem được cập nhật lại thì cập nhật lại total của order
            orderItemResp.quantity = orderItem.quantity;
        }
    }

    if (orderItem.price !== undefined && orderItem.price !== "") {
        if (orderItem.price !== orderItemResp.price) { // Giá của một item được cập nhật lại thì cập nhật lại total của order
            orderItemResp.price = orderItem.price;
            // order.totalPrice -= orderItem.price;
            // order.totalPrice += orderItem.price;
        }
    }

    if (orderItem.bladeWidth !== undefined && orderItem.bladeWidth !== "") {
        if (orderItem.bladeWidth !== orderItemResp.bladeWidth) {
            orderItemResp.bladeWidth = orderItem.bladeWidth;
        }
    }

    let date = new Date();
    orderItemResp.updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    orderItemResp.updater = req.user.userName;

    let result;
    try {
        result = await orderItemResp.save();
        const totalPrice = await calculateTotalPriceByOrderId(orderItemResp.orderId);
        orderResp.totalPrice = totalPrice[0].totalPrice;
        await orderResp.save();
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
    }
    if (result !== undefined) {
        res.json({
            message: "Order Item with ID: " + orderItem.orderItemId + " updated."
        })
    }
}

module.exports.updateOrderItemList = async (req, res) => {
    if ((typeof req.body === "object" && Object.keys(req.body).length === 0) || req.body.length === 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let orderItemIdArr = [];
    let orderItemList = req.body;

    let order = await Order.findByPk(req.params.orderId);

    if (!/^OS001$/.test(order.orderStatusId)) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["Order's status (ID: " + order.orderId + ") must be 'Pending' to be able to do CRUD."]
        })
        return;
    }

    for (let i = 0; i < orderItemList.length; i++) {
        orderItemIdArr.push(orderItemList[i].orderItemId)

        orderItemList[i].orderId = req.params.orderId;

        let messageObj = await validateOrderItemFields(orderItemList[i], "UPDATE_ORDER_ITEM", "");

        if (messageObj.message.length !== 0) {
            res.statusCode = 400; // Bad Request
            res.json({
                error: messageObj.message
            })
            return;
        }

        let orderItemResp = await OrderItem.findByPk(orderItemList[i].orderItemId);

        // if (orderItemList[i].orderId !== undefined && orderItemList[i].orderId !== "") {
        //     if (orderItemList[i].orderId !== orderItemResp.orderId)
        //         orderItemResp.orderId = orderItemList[i].orderId;
        // }

        if (orderItemList[i].length !== undefined && orderItemList[i].length !== "") {
            if (orderItemList[i].length !== orderItemResp.length)
                orderItemResp.length = orderItemList[i].length;
        }

        if (orderItemList[i].quantity !== undefined && orderItemList[i].quantity !== "") {
            if (orderItemList[i].quantity !== orderItemResp.quantity) { // Số lượng của một tiem được cập nhật lại thì cập nhật lại total của order
                orderItemResp.quantity = orderItemList[i].quantity;
            }
        }

        if (orderItemList[i].price !== undefined && orderItemList[i].price !== "") {
            if (orderItemList[i].price !== orderItemResp.price) { // Giá của một item được cập nhật lại thì cập nhật lại total của order
                orderItemResp.price = orderItemList[i].price;
            }
        }

        if (orderItemList[i].bladeWidth !== undefined && orderItemList[i].bladeWidth !== "") {
            if (orderItemList[i].bladeWidth !== orderItemResp.bladeWidth) {
                orderItemResp.bladeWidth = orderItemList[i].bladeWidth;
            }
        }

        let date = new Date();
        orderItemResp.updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        orderItemResp.updater = req.user.userName;

        let result;
        try {
            result = await orderItemResp.save();
        } catch (err) {
            res.statusCode = 500;
            res.json({
                error: [err]
            })
        }
    }
    let orderResp = await Order.findByPk(req.params.orderId);
    const totalPrice = await calculateTotalPriceByOrderId(req.params.orderId);
    orderResp.totalPrice = totalPrice[0].totalPrice;
    await orderResp.save();

    res.json({
        message: "Order Items with IDs: " + orderItemIdArr + " updated."
    })

}

module.exports.deleteOrderItems = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    console.log(req.body.arrOfOrderItemIds.toString());
    const orderItemIdsString = req.body.arrOfOrderItemIds.replace(",", ", ");
    const orderItemIds = req.body.arrOfOrderItemIds.split(",");
//    Check if req.body is NOT EMPTY
    if (Object.keys(req.body).length !== 0 && req.body.constructor === Object) {
        let orderItem = await OrderItem.findAll({
            where: {
                orderItemId: orderItemIds[0]
            }
        })

        try {
            let result = await OrderItem.destroy({
                where:
                    {orderItemId: orderItemIds}
            })

            if (result > 0) {
                let orderResp = await Order.findByPk(orderItem[0].orderId);
                const totalPrice = await calculateTotalPriceByOrderId(orderItem[0].orderId);
                if (totalPrice[0].totalPrice === null) {
                    totalPrice[0].totalPrice = 0;
                }
                orderResp.totalPrice = totalPrice[0].totalPrice;

                await orderResp.save();
            }

            res.json({
                message: result === 0 ? "No order item is deleted." : "Order Item with IDs: [" + orderItemIdsString + "], " +
                    "are deleted from SIAS system. (Total: " + result + ")"
            })

        } catch (error) {
            res.statusCode = 500;
            res.json({error: [error]})
        }
    }
}

async function validateOrderItemFields(orderItem, action, screen_id) {
    let result = await OrderItem.findByPk(orderItem.orderItemId); // orderItemId auto-gen
    let messageObj = {
        message: []
    }

    if (!result && action === "UPDATE_ORDER_ITEM") {
        messageObj.message.push("Order Item with ID: " +
            orderItem.orderItemId + " NOT existed.\n")
        return messageObj;
    }

    let resultOrder = await Order.findByPk(orderItem.orderId);

    if (!resultOrder) {
        messageObj.message.push("Order with ID: " +
            orderItem.orderId + " NOT existed.\n")
        return messageObj;
    }

    if (orderItem.length === undefined || orderItem.length === "" || !/^[0-9]{0,5}$/.test(orderItem.length)) {
        messageObj.message.push("Invalid Length. Please provide Length again.");
    }

    if (orderItem.quantity === undefined || orderItem.quantity === "" || !/^[0-9]{0,5}$/.test(orderItem.quantity)) {
        messageObj.message.push("Invalid Quantity. Please provide Quantity again.");
    }

    if (orderItem.bladeWidth === undefined || orderItem.bladeWidth === "" || !/^[0-9]{0,2}$/.test(orderItem.bladeWidth)) {
        messageObj.message.push("Invalid Blade Width. Please provide Blade Width again.");
    }

    if (orderItem.price === undefined || orderItem.price === "" || !/^[0-9]{0,11}$/.test(orderItem.price)) {
        messageObj.message.push("Invalid Price. Please provide Price again.");
    }

    return messageObj;
}

async function validateOrderFields(order, action, screen_id) {
    let result = await Order.findByPk(order.orderId);
    let messageObj = {
        message: []
    }

    if ((screen_id !== SCREEN_ID.WEB_UC33 && screen_id !== SCREEN_ID.WEB_UC30
        && screen_id !== SCREEN_ID.WEB_UC38) || screen_id === "" || screen_id === undefined) {
        messageObj.message.push("You must send with valid screen_id parameter for this request!");
        return messageObj;
    }

    if (!result && action === "UPDATE_ORDER") {
        messageObj.message.push("Order with ID: " +
            order.orderId + " NOT existed.\n")
        return messageObj;
    }

    if (screen_id === SCREEN_ID.WEB_UC38 || screen_id === SCREEN_ID.WEB_UC30 || screen_id === SCREEN_ID.WEB_UC33) {
        if (order.orderStatusId === undefined || order.orderStatusId === "" || !/^OS00[1-5]$/.test(order.orderStatusId.trim())) {
            messageObj.message.push("Invalid Order Status ID. Please provide Order Status ID again.")
        }
    }

    if (screen_id === SCREEN_ID.WEB_UC33 || screen_id === SCREEN_ID.WEB_UC30) {
        if (order.customerName === undefined || order.customerName === ""
            || !(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z ]+$/.test(order.customerName))
        ) {
            messageObj.message.push("Invalid Customer Name. Please provide Customer Name again.\n")
        }

        if (order.customerCompanyName === undefined || order.customerCompanyName === ""
            || !(/^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]+$/.test(order.customerCompanyName))
        ) {
            messageObj.message.push("Invalid Company Name. Please provide Company Name again.\n")
        }

        if (order.customerCityProvinceId === undefined || order.customerCityProvinceId === ""
            // || !/^CTY0[0-6][0-9]$/.test(order.customerCityProvinceId)
        ) {
            messageObj.message.push("Invalid City Province. Please provide City Province again.")
        } else {
            const result = await CityProvince.findByPk(order.customerCityProvinceId);

            if (!result) {
                messageObj.message.push("City/Province is not existed! Please provide City Province again.\n")
            }
        }

        if (order.customerDistrictId === undefined || order.customerDistrictId === ""
            // || !/^DST0[0-5][0-9]$/.test(order.customerDistrictId)
        ) {
            messageObj.message.push("Invalid District. Please provide District again.")
        } else {
            const result = await District.findByPk(order.customerDistrictId);

            if (!result) {
                messageObj.message.push("District is not existed! Please provide District again.\n")
            }
        }

        if (order.customerWardCommuneId === undefined || order.customerWardCommuneId === ""
            // || !/^WRD0[0-5][0-9]$/.test(order.customerWardCommuneId)
        ) {

            messageObj.message.push("Invalid Ward/Commune. Please provide Ward/Commune again.")
        } else {
            const result = await WardCommune.findByPk(order.customerWardCommuneId);

            if (!result) {
                messageObj.message.push("Ward/Commune is not existed! Please provide Ward/Commune again.\n")
            }
        }

        if (order.customerAddress === undefined || order.customerAddress === "") {
            messageObj.message.push("Invalid Address. Please provide Address again.")
        }

        if (order.customerPhoneNumber === undefined || order.customerPhoneNumber === ""
            || !(/^((09|03|07|08|05)+([0-9]{8})\b)$/.test(order.customerPhoneNumber))) {
            messageObj.message.push("Invalid Phone Number. Please provide Phone Number again.\n")
        }

        if (order.customerEmail === undefined || order.customerEmail === ""
            || !(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(order.customerEmail))) {
            messageObj.message.push("Invalid Email. Please provide Email again.\n")
        }

        // if (order.totalPrice === undefined || order.totalPrice === "") {
        //     messageObj.message.push("Invalid Total Price. Please provide Total Price again.\n")
        // }

        // Ko cho thay đổi approver, assignee
        if (screen_id === SCREEN_ID.WEB_UC30 || screen_id === SCREEN_ID.WEB_UC33) {

            if (screen_id === SCREEN_ID.WEB_UC30) {
                if (order.approver === undefined || order.approver === "") {
                    messageObj.message.push("Invalid Approver. Please provide Approver again.\n")
                } else {
                    const sql = `SELECT COUNT(userName) as count FROM sias_db.account WHERE userName = :userName`;
                    const isApproverExist = await db.query(sql, {
                        type: db.QueryTypes.SELECT,
                        replacements: {
                            userName: order.approver.trim()
                        }
                    });

                    if (isApproverExist[0].count !== 1) {
                        messageObj.message.push("Approver is not existed. Please enter again.\n")
                    }
                }
            }

            if (screen_id === SCREEN_ID.WEB_UC33) {
                if (order.assignee === undefined || order.assignee === "") {
                    messageObj.message.push("Invalid Assignee. Please provide Assignee again.\n")
                } else {
                    const sql = `SELECT COUNT(userName) as count FROM sias_db.account WHERE userName = :userName`;
                    const isAssigneeExist = await db.query(sql, {
                        type: db.QueryTypes.SELECT,
                        replacements: {
                            userName: order.assignee.trim()
                        }
                    });

                    if (isAssigneeExist[0].count !== 1) {
                        messageObj.message.push("Assignee is not existed. Please enter again.\n")
                    }
                }
            }
        }
    }

    return messageObj;
}

module.exports.insertNewOrder = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let order = req.body;

    order.orderId = 0; // Order ID is auto-generated
    order.totalPrice = 0; // Default

    let messageObj = await validateOrderFields(order, "INSERT_ORDER", SCREEN_ID.WEB_UC30);

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let date = new Date();
    order.createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    order.creator = req.user.userName;
    try {
        const newOrder = await Order.create(order);

        res.json({
            message: "An order was created successfully.",
            newOrder: newOrder
        })
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
}

module.exports.insertNewOrderForCSV = async (parsedObject, res) => {
    try {
        await Order.create(parsedObject);
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
}

module.exports.downloadCSVByArrOfOrderIds = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let sql = `SELECT 
            ord.orderId,
            (SELECT ordst.name FROM sias_db.orderstatus ordst WHERE ordst.orderStatusId = ord.orderStatusId) as orderStatus,
            ord.customerName,
            ord.customerCompanyName,
            (SELECT cty.name FROM sias_db.cityprovince cty WHERE cty.cityProvinceId = ord.customerCityProvinceId) as customerCityProvince,
            (SELECT dst.name FROM sias_db.district dst WHERE dst.districtId = ord.customerDistrictId) as customerDistrict,
            (SELECT wrd.name FROM sias_db.wardcommune wrd WHERE wrd.wardcommuneId = ord.customerWardCommuneId) as customerWardCommune,
            ord.customerAddress,
            ord.customerPhoneNumber,
            ord.customerEmail,
            ord.totalPrice,
            ord.createdDate,
            ord.updatedDate,
            ord.creator,
            ord.updater,
            ord.approver,
            ord.assignee,
            ord.isBatchAdded,
            ord.orderImportBatchId,
            ordit.orderItemId,
            ordit.length,
            ordit.quantity,
            ordit.price
            FROM sias_db.orderitem ordit INNER JOIN sias_db.order ord ON ordit.orderId = ord.orderId
            WHERE ord.orderId IN
            `;

    const arrOfOrderIds = req.body.arrOfOrderIds.toString();

    const splited = arrOfOrderIds.split(",");
    if (splited.length > 50) {
        res.statusCode = 400;
        res.json({
            error: ["Only 50 records are allowed to export to CSV file by batch"]
        })
        return;
    }

    sql += " (" + arrOfOrderIds + ")";
    let resultArr;
    try {
        resultArr = await db.query(sql, {
            type: db.QueryTypes.SELECT
        })
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
        return;
    }

    return resultArr;
}

module.exports.updateOrder = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    let order = req.body;

    let messageObj = await validateOrderFields(order, "UPDATE_ORDER", req.body.screen_id);

    if (messageObj.message.length !== 0) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: messageObj.message
        })
        return;
    }

    let orderResp;
    let orderResult = await Order.findAll({
        where:
            Sequelize.and(
                {orderId: order.orderId},
                Sequelize.or(
                    {creator: [req.user.userName]},
                    {updater: [req.user.userName]},
                    {approver: [req.user.userName]}
                )
            )
    });
    orderResp = orderResult[0];

    if (order.orderStatusId !== undefined && order.orderStatusId !== "") {
        if (order.orderStatusId !== orderResp.orderStatusId)
            orderResp.orderStatusId = order.orderStatusId;
    }

    if (order.customerName !== undefined && order.customerName !== "") {
        if (order.customerName !== orderResp.customerName)
            orderResp.customerName = order.customerName;
    }

    if (order.customerCompanyName !== undefined && order.customerCompanyName !== "") {
        if (order.customerCompanyName !== orderResp.customerCompanyName)
            orderResp.customerCompanyName = order.customerCompanyName;
    }

    if (order.customerCityProvinceId !== undefined && order.customerCityProvinceId !== "") {
        if (order.customerCityProvinceId !== orderResp.customerCityProvinceId)
            orderResp.customerCityProvinceId = order.customerCityProvinceId;
    }

    if (order.customerDistrictId !== undefined && order.customerDistrictId !== "") {
        if (order.customerDistrictId !== orderResp.customerDistrictId)
            orderResp.customerDistrictId = order.customerDistrictId;
    }

    if (order.customerWardCommuneId !== undefined && order.customerWardCommuneId !== "") {
        if (order.customerWardCommuneId !== orderResp.customerWardCommuneId)
            orderResp.customerWardCommuneId = order.customerWardCommuneId;
    }

    if (order.customerAddress !== undefined && order.customerAddress !== "") {
        if (order.customerAddress !== orderResp.customerAddress)
            orderResp.customerAddress = order.customerAddress;
    }

    if (order.customerPhoneNumber !== undefined && order.customerPhoneNumber !== "") {
        if (order.customerPhoneNumber !== orderResp.customerPhoneNumber)
            orderResp.customerPhoneNumber = order.customerPhoneNumber;
    }

    if (order.customerEmail !== undefined && order.customerEmail !== "") {
        if (order.customerEmail !== orderResp.customerEmail)
            orderResp.customerEmail = order.customerEmail;
    }

    if (order.totalPrice !== undefined && order.totalPrice !== "") {
        if (order.totalPrice !== orderResp.totalPrice)
            orderResp.totalPrice = order.totalPrice;
    }

    // if (order.approver !== undefined && order.approver !== "") {
    //     if (order.approver !== orderResp.approver)
    //         orderResp.approver = order.approver;
    // }

    if (order.assignee !== undefined && order.assignee !== "") {
        if (order.assignee !== orderResp.assignee)
            orderResp.assignee = order.assignee;
    }

    let date = new Date();
    orderResp.updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];
    orderResp.updater = req.user.userName;

    let result;
    try {
        result = await orderResp.save();
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        })
    }
    if (result !== undefined) {
        res.json({
            message: "Order with ID: " + order.orderId + " updated."
        })
    }
}

module.exports.deleteRejectedOrders = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    console.log(req.body.arrOfOrderIds.toString());
    const orderIdsString = req.body.arrOfOrderIds.replace(",", ", ");
    const orderIds = req.body.arrOfOrderIds.split(",");
//    Check if req.body is NOT EMPTY
    if (Object.keys(req.body).length !== 0 && req.body.constructor === Object) {
        OrderItem.destroy({
            where:
                {orderId: orderIds}
        }).catch((error) => {
            res.statusCode = 500;
            res.json({error: [error]})
        });

        Order.destroy({
            where:
                Sequelize.and(
                    {orderId: orderIds},
                    {orderStatusId: ORDER_STATUS_ID.OS005},
                    Sequelize.or(
                        {creator: [req.user.userName]},
                        {updater: [req.user.userName]}
                    )
                )
        }).then((result) => {
            res.json(result === 0 ? {error: "No order is deleted."} : {message: "Order with IDs: [" + orderIdsString + "], are deleted from SIAS system."})
        })
            .catch((error) => {
                res.statusCode = 500;
                res.json({error: [error]})
            });
    }
}

module.exports.getOrderListByParams = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    const resPerPage = DEFAULT_ITEM_PER_PAGE;
    const page = req.query.page || 1;
    let offset = resPerPage * page - resPerPage;
    let rowCount = resPerPage;

    let sqlCountRows = `SELECT COUNT(orderId) as count FROM \`order\` `;

    // Get fields to search
    let fieldsToSearch = req.body;

    let replacements = {}

    let sql = `
            SELECT 
            ROW_NUMBER() OVER (
              ORDER BY orderId
            ) row_num,
        \t\`order\`.\`orderId\`,
            \`order\`.\`orderStatusId\`,
            \`order\`.\`customerName\`,
            \`order\`.\`customerCompanyName\`,
            \`order\`.\`customerCityProvinceId\`,
            \`order\`.\`customerDistrictId\`,
            \`order\`.\`customerWardCommuneId\`,
            \`order\`.\`customerAddress\`,
            \`order\`.\`customerPhoneNumber\`,
            \`order\`.\`customerEmail\`,
            \`order\`.\`totalPrice\`,
            \`order\`.\`createdDate\`,
            \`order\`.\`updatedDate\`,
            \`order\`.\`creator\`,
            \`order\`.\`updater\`,
            \`order\`.\`approver\`,
            \`order\`.\`assignee\`,
            \`order\`.\`isBatchAdded\`,
            \`order\`.\`orderImportBatchId\`
        FROM \`sias_db\`.\`order\`
   `;

    let sqlWhere = ` WHERE `;
    let checkComma = 0;

    if (fieldsToSearch.orderId !== undefined && fieldsToSearch.orderId !== "") {
        sqlWhere += `orderId LIKE :orderId`;
        checkComma++;
        replacements.orderId = `%` + (fieldsToSearch.orderId.trim()) + `%`;
    }
    if (fieldsToSearch.customerName !== undefined && fieldsToSearch.customerName !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `customerName LIKE :customerName`;
        checkComma++;
        replacements.customerName = `%` + (fieldsToSearch.customerName.trim()) + `%`;
    }
    if (fieldsToSearch.customerCompanyName !== undefined && fieldsToSearch.customerCompanyName !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `customerCompanyName LIKE :customerCompanyName`;
        checkComma++;
        replacements.customerCompanyName = `%` + (fieldsToSearch.customerCompanyName.trim()) + `%`;
    }
    if (fieldsToSearch.customerPhoneNumber !== undefined && fieldsToSearch.customerPhoneNumber !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `customerPhoneNumber LIKE :customerPhoneNumber`;
        checkComma++;
        replacements.customerPhoneNumber = `%` + (fieldsToSearch.customerPhoneNumber.trim()) + `%`;
    }
    if (fieldsToSearch.customerEmail !== undefined && fieldsToSearch.customerEmail !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `customerEmail LIKE :customerEmail`;
        checkComma++;
        replacements.customerEmail = `%` + (fieldsToSearch.customerEmail.trim()) + `%`;
    }

    // Tính năng filter gộp chung với search ngày 08/11/2020
    if (fieldsToSearch.orderStatusId !== undefined &&
        !(/^OS00[1-5]$/.test(fieldsToSearch.orderStatusId))) {
        res.statusCode = 400;
        res.json({
            error: ["Field 'Order Status' is in wrong format."]
        })
        return;
    } else if (fieldsToSearch.orderStatusId !== undefined && fieldsToSearch.orderStatusId !== "") {
        if (checkComma > 0) {
            sqlWhere += ` AND `;
        }
        sqlWhere += `orderStatusId = :orderStatusId`;
        checkComma++;
        replacements.orderStatusId = (fieldsToSearch.orderStatusId.trim());
    }

    if (fieldsToSearch.searchByDate !== undefined && fieldsToSearch.searchByDate === "1") {
        // From date bắt buộc phải có, validate ở phía client
        if (fieldsToSearch.fromDate !== undefined &&
            !(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/.test(fieldsToSearch.fromDate))) {
            res.statusCode = 400;
            res.json({
                error: ["Field 'From Date' is in wrong format."]
            })
            return;
        } else if (fieldsToSearch.fromDate !== undefined && fieldsToSearch.fromDate !== "") {
            if (checkComma > 0) {
                sqlWhere += ` AND `;
            }
            sqlWhere += `(createdDate BETWEEN :fromDate`;
            checkComma++;
            replacements.fromDate = fieldsToSearch.fromDate.trim();
        } else { // Trên front-end bắt buộc phải có nút check tìm theo date nữa
            res.statusCode = 400;
            res.json({
                error: ["Field 'From Date' is required before field 'To Date'."]
            });
            return;
        }

        if (fieldsToSearch.toDate !== undefined &&
            !(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/.test(fieldsToSearch.toDate))) {
            res.statusCode = 400;
            res.json({
                error: ["Field 'To Date' is in wrong format."]
            })
            return;
        } else if (fieldsToSearch.toDate !== undefined && fieldsToSearch.toDate !== "") {
            if (checkComma > 0) {
                sqlWhere += ` AND `;
            }
            sqlWhere += ` :toDate)`;
            checkComma++;
            replacements.toDate = fieldsToSearch.toDate.trim();
        } else {
            if (checkComma > 0) {
                sqlWhere += ` AND `;
            }
            sqlWhere += ` '${new Date().toISOString().split('T')[0]}')`;
            checkComma++;
        }
    }

    let sqlTail = `
   ORDER BY orderId 
   LIMIT :offset, :rowCount`;

    if (sqlWhere !== ` WHERE `) {
        sql += sqlWhere + " AND " +
            "(`order`.`creator` IN ('" + req.user.userName + "') " +
            "OR `order`.`updater` IN ('" + req.user.userName + "') " +
            "OR `order`.`approver` IN ('" + req.user.userName + "') " +
            "OR `order`.`assignee` IN ('" + req.user.userName + "'))";
    } else {
        sql += sqlWhere + " " +
            "(`order`.`creator` IN ('" + req.user.userName + "') " +
            "OR `order`.`updater` IN ('" + req.user.userName + "') " +
            "OR `order`.`approver` IN ('" + req.user.userName + "') " +
            "OR `order`.`assignee` IN ('" + req.user.userName + "'))";
    }

    sql += sqlTail;

    if (sqlWhere !== ` WHERE `) {
        sqlCountRows += sqlWhere + " AND " +
            "(`order`.`creator` IN ('" + req.user.userName + "') " +
            "OR `order`.`updater` IN ('" + req.user.userName + "') " +
            "OR `order`.`approver` IN ('" + req.user.userName + "') " +
            "OR `order`.`assignee` IN ('" + req.user.userName + "'))";
    } else {
        sqlCountRows += sqlWhere + " " +
            "(`order`.`creator` IN ('" + req.user.userName + "') " +
            "OR `order`.`updater` IN ('" + req.user.userName + "') " +
            "OR `order`.`approver` IN ('" + req.user.userName + "') " +
            "OR `order`.`assignee` IN ('" + req.user.userName + "'))";
    }

    console.log(sql);


    let numberOfRows;
    let resp = await getNumberOfRows(sqlCountRows, replacements);
    console.log("numberOfRows");
    console.log(resp[0].count);

    numberOfRows = resp[0].count;

    let numberOfPages = Math.ceil(numberOfRows / resPerPage);

    replacements.offset = offset;
    replacements.rowCount = rowCount;

    try {
        let orders = await db.query(sql, {
            type: db.QueryTypes.SELECT,
            replacements: replacements,
        })

        for (let i = 0; i < orders.length; i++) {
            let tempObj = {}
            tempObj.dataValues = orders[i];
            orders[i] = tempObj;
        }

        orders = await convertForeignIdToValue(orders);

        for (let i = 0; i < orders.length; i++) {
            orders[i] = orders[i].dataValues;
        }

        res.json({
            orders: orders,
            numberOfPages: numberOfPages,
            totalEntries: numberOfRows
        });
    } catch (error) {
        res.statusCode = 500;
        res.json({error: [error]})
    }

}

module.exports.markOrderAsDone = async (req, res) => {
    if (Object.keys(req.body).length === 0 || req.body.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.body is EMPTY."]
        })
        return;
    }

    if (req.body.arrOfOrderIds === undefined && req.body.arrOfOrderIds === "") {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["There is no order id coming with this request!"]
        })
        return;
    }

    let sql = "SELECT * FROM \`sias_db\`.\`order\` " +
        "WHERE orderId IN (" + req.body.arrOfOrderIds + ")" +
        " AND assignee = '" + req.user.userName + "'";

    let orderList = await db.query(sql, {
        type: db.QueryTypes.SELECT,
        model: Order,
        mapToModel: true
    })

    let arrOfOK = [false];
    try {
        for (let i = 0; i < orderList.length; i++) {
            if (orderList[i].orderStatusId === ORDER_STATUS_ID.OS003) { // OS003 === Processing
                if (i === 0) {
                    arrOfOK = [];
                }
                orderList[i].orderStatusId = "OS004";
                const bool = await orderList[i].save();
                arrOfOK.push(bool);
            }
        }
        const stringResult = req.body.arrOfOrderIds.replace(/,/g, ", ");
        console.log('End')

        for (let i = 0; i < arrOfOK.length; i++) {
            if (arrOfOK[i] === false) {
                res.statusCode = 400;
                res.json({
                    error: ["Something went wrong. Please check your request!"]
                })
                return;
            }
        }

        res.json({
            message: "Order with IDs: [" + stringResult + "] are marked as COMPLETED."
        })

    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
    }
}

module.exports.changeOrderStatusById = async (req, res) => {
    if (Object.keys(req.query).length === 0 || req.query.constructor !== Object) {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["req.query is EMPTY."]
        })
        return;
    }

    if (req.query.orderId === undefined && req.body.orderId === "") {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["There is no order id coming with this request!"]
        })
        return;
    }

    if (req.query.orderStatusId === undefined && req.body.orderStatusId === "") {
        res.statusCode = 400; // Bad Request
        res.json({
            error: ["There is no order status ID coming with this request!"]
        })
        return;
    }

    let options = {
        where:
            Sequelize.and(
                {orderId: req.query.orderId},
                Sequelize.or(
                    (req.user.roleId === ROLE_ID.R002 ?
                        {approver: [req.user.userName]} : {assignee: [req.user.userName]})
                )
            )

    };

    try {
        let order = await Order.findAll(options);

        if (!order[0]) {
            res.statusCode = 400; // Bad Request
            res.json({
                error: ["Order with ID: " +
                req.query.orderId + " NOT existed or you are not authorized to do this action.\n"]
            })
            return;
        }

        if (!/^OS00[1-5]$/.test(req.query.orderStatusId)) {
            res.statusCode = 400; // Bad Request
            res.json({
                error: ["Wrong Order Status ID."]
            })
            return;
        } else {
            if (
                (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R003) &&
                order[0].orderStatusId === ORDER_STATUS_ID.OS001) {
                if (req.query.orderStatusId !== ORDER_STATUS_ID.OS002
                    && req.query.orderStatusId !== ORDER_STATUS_ID.OS005) {
                    res.statusCode = 400; // Bad Request
                    res.json({
                        error: ["Order Status 'Pending' (OS001) must be changed to 'Approved' (OS002) or 'Rejected' (OS005)."]
                    })
                    return;
                } else {
                    order[0].orderStatusId = req.query.orderStatusId;
                    await order[0].save();
                    res.json({
                        message: ["Status of Order #" + req.query.orderId + " is changed to " + ORDER_STATUS_NAME[req.query.orderStatusId] + "."]
                    })
                    return;
                }
            }

            if (order[0].orderStatusId === ORDER_STATUS_ID.OS005 || order[0].orderStatusId === ORDER_STATUS_ID.OS004) {
                res.statusCode = 400; // Bad Request
                res.json({
                    error: ["Order Status 'Completed' (OS004) or Rejected' (OS005) cannot be changed to another status because" +
                    " it is the end of workflow."]
                })
                return;
            }

            if (
                (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R003)
                &&
                order[0].orderStatusId === ORDER_STATUS_ID.OS003) {
                if (req.query.orderStatusId !== ORDER_STATUS_ID.OS004) {
                    res.statusCode = 400; // Bad Request
                    res.json({
                        error: ["Order Status 'Processing' (OS003) must be changed to 'Completed' (OS004)."]
                    })
                    return;
                } else {
                    order[0].orderStatusId = req.query.orderStatusId;
                    await order[0].save();
                    res.json({
                        message: "Status of Order #" + req.query.orderId + " is changed to " + ORDER_STATUS_NAME[req.query.orderStatusId] + "."
                    })
                    return;
                }
            }

            if (
                (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R003)
                &&
                order[0].orderStatusId === ORDER_STATUS_ID.OS002) {
                if (req.query.orderStatusId !== ORDER_STATUS_ID.OS003) {
                    res.statusCode = 400; // Bad Request
                    res.json({
                        error: ["Order Status 'Approved' (OS002) must be changed to 'Processing' (OS003)."]
                    })
                    return;
                } else {
                    order[0].orderStatusId = req.query.orderStatusId;
                    await order[0].save();
                    res.json({
                        message: "Status of Order #" + req.query.orderId + " is changed to " + ORDER_STATUS_NAME[req.query.orderStatusId] + "."
                    })
                    return;
                }
            }
        }
    } catch (err) {
        res.statusCode = 500; // Bad Request
        res.json({
            error: [err]
        })
        return;
    }
    res.statusCode = 401;
    res.json({
        error: ["You are not authorized to do this action."]
    })
}