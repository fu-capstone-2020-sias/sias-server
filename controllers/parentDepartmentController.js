const db = require("../db");
const Sequelize = require("sequelize");
const Department = require('../models/department')(db, Sequelize.DataTypes)

module.exports = {
    async getAllDepartment(){
        const departments = await Department.findAndCountAll()
        return {
           data: departments 
        }
    },
    async getDepartmentBy(where){
        const departments = await Department.findAndCountAll({
            where
        })
        return {
            data: departments
        }
    },
    async find(id){
        const res = await this.getDepartmentBy({departmentId: id})
        return res 
    }
}
