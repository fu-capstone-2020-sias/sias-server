const db = require("../db");
const Sequelize = require("sequelize");
const Role = require("../models/role")(db, Sequelize.DataTypes);

module.exports.getAllRoles = async (req, res) => {
    const result = await Role.findAll();
    res.json({
        roles: result
    })
}
