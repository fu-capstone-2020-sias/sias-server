const db = require("../db");
const Sequelize = require("sequelize");
const Steeltype = require('../models/steeltype')(db, Sequelize.DataTypes);

module.exports = {
    async getAllSteelType() {
        const steelTypes = await Steeltype.findAndCountAll({
            order: ['name']
        })
        return {
            data: steelTypes
        }
    },
    async getSteelTypeBy(where){
        const steelTypes = await Steeltype.findAndCountAll({
            where,
        })
        return {
            data: steelTypes
        }
    },
    async find(id){
        const res = await this.getSteelTypeBy({steeltypeId: id})
        return res 
    }
}