const db = require("../db");
const Sequelize = require("sequelize");
const StockGroup = require('../models/stockproductgroup')(db, Sequelize.DataTypes);

module.exports = {
    async getAllStockGroup() {
        const stockGroups = await StockGroup.findAndCountAll({
            where: {
                disableFlag: 0
            },
            order: ['name']
        })
        return {
            data: stockGroups
        }
    },
    async getStockGroupTypeBy(where){
        const stockGroups = await StockGroup.findAndCountAll({
            where
        })
        return {
            data: stockGroups
        }
    },
    async find(id){
        const res = await this.getStockGroupTypeBy({stockProductGroupId:id})
        return res;
    }
}