const db = require("../db");
const Sequelize = require("sequelize");
const Stockproduct = require('../models/stockproduct')(db, Sequelize.DataTypes)

const SteelType = require('../models/steeltype')(db, Sequelize.DataTypes)
const StockProductGroup = require('../models/stockproductgroup')(db, Sequelize.DataTypes)
const StockProductStandard = require('../models/stockproductstandard')(db, Sequelize.DataTypes)
const StockProductStatus = require('../models/stockproductstatus')(db, Sequelize.DataTypes)
const Warehouse = require('../models/warehouse')(db, Sequelize.DataTypes)
const Account = require('../models/account')(db, Sequelize.DataTypes)
const StockProductImportBatch = require('../models/stockproductimportbatch')(db, Sequelize.DataTypes);
const Stockproductexportbatch = require('../models/stockproductexportbatch')(db, Sequelize.DataTypes);
const { checkNullUndefinedEmptyStockProduct,
    checkExistDatabase,
    checkFieldInRange,
    isNumber,
    checkExistDatabaseForUpdate,
    checkExistDatabaseForDelete,
    validateDateStockProduct,
    checkSKU,
    checkStockProductName,
    checkBarcode,
    validateDateStockProductSearch
} = require('../utils/stockProductUtils');

const stockProductAlias = require("../constants/stockProductAlias");
const stockProductCSVUtils = require('../utils/stockProductCSVUtils');
SteelType.hasMany(Stockproduct, { foreignKey: 'type' });
Stockproduct.belongsTo(SteelType, { foreignKey: 'type' });

StockProductGroup.hasMany(Stockproduct, { foreignKey: 'stockGroupId' });
Stockproduct.belongsTo(StockProductGroup, { foreignKey: 'stockGroupId' });
StockProductStandard.hasMany(Stockproduct, { foreignKey: 'standard' });
Stockproduct.belongsTo(StockProductStandard, { foreignKey: 'standard' });
StockProductStatus.hasMany(Stockproduct, { foreignKey: 'statusId' });
Stockproduct.belongsTo(StockProductStatus, { foreignKey: 'statusId' });
Warehouse.hasMany(Stockproduct, { foreignKey: 'warehouseId' });
Stockproduct.belongsTo(Warehouse, { foreignKey: 'warehouseId' });

Account.hasMany(Stockproduct, { foreignKey: 'creator' });
Stockproduct.belongsTo(Account, { as: 'stockProductCreator', foreignKey: 'creator' });

Account.hasMany(Stockproduct, { foreignKey: 'updater' });
Stockproduct.belongsTo(Account, { as: 'stockProductUpdater', foreignKey: 'updater' });

StockProductImportBatch.hasMany(Stockproduct, { foreignKey: 'stockProductImportBatchId' });
Stockproduct.belongsTo(StockProductImportBatch, { foreignKey: 'stockProductImportBatchId' });

const accountTableAttribute = ['userName', 'firstName', 'lastName']
const { readCsvFile } = require('../utils/stockProductCSVUtils');
const stockProductImportBatchController = require("./stockProductImportBatchController");
const { Op } = require("sequelize");
const { STOCK_PRODUCT_LENGTH_RANGE, STOCK_PRODUCT_PRICE_RANGE, UPLOAD_IMAGE } = require('../constants/constants');
const fs = require('fs');
const request = require('request');
const steeltypeController = require('../controllers/steelTypeController');
module.exports = {
    async getAllStockProduct(where) {
        const res = await Stockproduct.findAndCountAll({
            where,
            include: [
                { model: SteelType },
                { model: StockProductGroup },
                { model: StockProductStandard },
                { model: StockProductStatus },
                { model: Warehouse },
                { model: Account, as: 'stockProductCreator', attributes: accountTableAttribute },
                { model: Account, as: 'stockProductUpdater', attributes: accountTableAttribute },
                { model: StockProductImportBatch },
            ],
        })
        return {
            data: res
        }
    },
    async getStockProductBy(input) {
        const { offset, limit, stockProductId, sku, barcode, type, name, stockGroupId, standard, statusId, warehouseId, creator, updater, isBatchAdded, stockProductImportBatchId, searchByDate, fromDate, toDate, fromPrice, toPrice, fromLength, toLength } = input
        const fields = []
        if (stockProductId) {
            fields.push({
                stockProductId: {
                    [Op.like]: `%${stockProductId}%`
                }
            })
        }
        if (sku) {
            fields.push({
                sku: {
                    [Op.like]: `%${sku}%`
                }
            })
        }
        if (barcode) {
            fields.push({
                barcode: {
                    [Op.like]: `%${barcode}%`
                }
            })
        }
        if (type) {
            fields.push({
                type: {
                    [Op.eq]: `${type}`
                }
            })
        }
        if (name) {
            fields.push({
                name: {
                    [Op.like]: `%${name}%`
                }
            })
        }
        if (fromPrice && toPrice) {
            await isNumber({ fromPrice, toPrice })
            await checkFieldInRange({
                fromPrice, toPrice
            }, STOCK_PRODUCT_PRICE_RANGE)
            fields.push({
                price: {
                    [Op.between]: [fromPrice, toPrice],
                }
            })
        }
        if (fromLength && toLength) {
            await isNumber({ fromLength, toLength })
            await checkFieldInRange({
                fromLength, toLength
            }, STOCK_PRODUCT_LENGTH_RANGE)
            fields.push({
                length: {
                    [Op.between]: [fromLength, toLength],
                }
            })
        }
        if (stockGroupId) {
            fields.push({
                stockGroupId: {
                    [Op.eq]: `${stockGroupId}`,
                }
            })
        }
        if (standard) {
            fields.push({
                standard: {
                    [Op.eq]: `${standard}`,
                }
            })
        }
        if (searchByDate) {
            switch (searchByDate) {
                case '0':
                    // fields.push({
                    //     manufacturedDate: {
                    //         [Op.eq]: `${manufacturedDate}`,
                    //     }
                    // })
                    break;
                case '1':
                    validateDateStockProductSearch(fromDate)
                    validateDateStockProductSearch(toDate)
                    fields.push({
                        manufacturedDate: {
                            [Op.between]: [fromDate, toDate],
                        }
                    })
                    break;
            }
        }
        if (statusId) {
            fields.push({
                statusId: {
                    [Op.eq]: `${statusId}`
                }
            })
        }
        if (warehouseId) {
            fields.push({
                warehouseId: {
                    [Op.eq]: `${warehouseId}`
                }
            })
        }
        if (creator) {
            fields.push({
                creator: {
                    [Op.like]: `%${creator}%`
                }
            })
        }
        if (updater) {
            fields.push({
                updater: {
                    [Op.like]: `%${updater}%`
                }
            })
        }
        if (isBatchAdded) {
            fields.push({
                isBatchAdded: {
                    [Op.eq]: isBatchAdded
                }
            })
        }
        if (stockProductImportBatchId) {
            fields.push({
                isBatchAdded: {
                    [Op.eq]: `${isBatchAdded}`
                }
            })
        }
        let where = {}
        if (fields.length > 0) {
            where = {
                [Op.and]: fields
            }
        }

        const response = await Stockproduct.findAndCountAll({
            offset, limit,
            include: [
                { model: SteelType },
                { model: StockProductGroup },
                { model: StockProductStandard },
                { model: StockProductStatus },
                { model: Warehouse },
                { model: Account, as: 'stockProductCreator', attributes: accountTableAttribute },
                { model: Account, as: 'stockProductUpdater', attributes: accountTableAttribute },
                { model: StockProductImportBatch },
            ],
            where,
            order: [
                ['stockProductId', 'ASC'],
            ]
        })
        const expectedTotalPages = Math.ceil(response['count'] / limit);
        const totalPages = expectedTotalPages != 0 ? expectedTotalPages : 1;
        const stockProductRaw = response.rows;
        const stockproductList = stockProductRaw.map((item) => {
            return item.dataValues
        });
        return {
            totalResult: response['count'],
            totalPages,
            stockproductList
        }
    },
    async insertWarehouse(input) {
        const {
            sku, barcode, type, name, price,
            length, stockGroupId, standard,
            statusId, manufacturedDate, warehouseId, user } = input;
        await checkNullUndefinedEmptyStockProduct({
            sku, barcode, type, name, price,
            length, stockGroupId, standard,
            statusId, manufacturedDate, warehouseId, user
        });
        await checkExistDatabase({
            type, stockGroupId, standard, statusId, warehouseId
        });
        await isNumber({ price, length })
        await checkFieldInRange({
            price
        }, STOCK_PRODUCT_PRICE_RANGE)
        await checkFieldInRange({
            length
        }, STOCK_PRODUCT_LENGTH_RANGE)
        validateDateStockProduct(manufacturedDate);
        checkSKU(sku);
        checkStockProductName(name);
        checkBarcode(barcode);
        let date = new Date();
        let createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        const newStockProduct = Stockproduct.create({
            sku, barcode, type, name,
            price, length, stockGroupId,
            standard, statusId, manufacturedDate,
            warehouseId, creator: user, createdDate,
            updatedDate: null,
            updater: null,
            isBatchAdded: 0,
            stockProductImportBatchId: null
        });
        return newStockProduct;
    },
    async updateStockProduct(input) {
        const {
            stockProductId, sku, barcode, type, name, price,
            length, stockGroupId, standard,
            statusId, manufacturedDate, warehouseId, user } = input;
        await checkNullUndefinedEmptyStockProduct({
            stockProductId, sku, barcode, type, name, price,
            length, stockGroupId, standard,
            statusId, manufacturedDate, warehouseId, user
        });
        await checkExistDatabaseForUpdate({
            stockProductId, type, stockGroupId, standard, statusId, warehouseId
        });
        await isNumber({ price, length })
        await checkFieldInRange({
            price
        }, STOCK_PRODUCT_PRICE_RANGE)
        await checkFieldInRange({
            length
        }, STOCK_PRODUCT_LENGTH_RANGE)
        validateDateStockProduct(manufacturedDate)
        checkSKU(sku);
        checkStockProductName(name);
        checkBarcode(barcode);
        let date = new Date();
        let updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        const res = await Stockproduct.update({
            sku, barcode, type, name,
            price, length, stockGroupId, standard, statusId, manufacturedDate,
            warehouseId, updatedDate, updater: user,
        }, {
            where: {
                stockProductId
            }
        })
        let newData = {}
        if (res) {
            const updatedStockProduct = await this.find(stockProductId);
            newData = updatedStockProduct.data.rows[0];
        }
        return newData
    },
    async deleteStockProduct(input) {
        const { stockProductId } = input;
        await checkNullUndefinedEmptyStockProduct({ stockProductId });
        await checkExistDatabaseForDelete({ stockProductId });
        const result = await Stockproduct.destroy({
            where: {
                stockProductId
            }
        });
        const a = 1
        return result
    },
    async find(id) {
        const res = await this.getAllStockProduct({ stockProductId: id })
        return res
    },
    async exportStockByListId({ idList, pathCSV, exporter }) {
        await checkNullUndefinedEmptyStockProduct({ stockProductId: idList }, stockProductAlias)
        const parsedIdList = idList.split(',')
        const res = await Stockproduct.findAndCountAll({
            raw: true,
            where: {
                stockProductId: parsedIdList
            },
            include: [
                { model: SteelType },
                { model: StockProductGroup },
                { model: StockProductStandard },
                { model: StockProductStatus },
                { model: Warehouse },
                { model: Account, as: 'stockProductCreator', attributes: accountTableAttribute },
                { model: Account, as: 'stockProductUpdater', attributes: accountTableAttribute },
                { model: StockProductImportBatch },
            ]
        });
        if (res.count !== parsedIdList.length) {
            const errors = ['Id not existed in database'];
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
        res.rows.forEach((row) => {
            const type = row['steeltype.name']
            row['type'] = type

            const group = row['stockproductgroup.name']
            row['stockGroupId'] = group

            const standard = row['stockproductstandard.name']
            row['standard'] = standard

            const status = row['stockproductstatus.name']
            row['statusId'] = status

            const warehouseName = row['warehouse.name']
            row['warehouseId'] = warehouseName;

            const creator = row['stockProductCreator.userName']
            row['creator'] = creator

            const updater = row['stockProductUpdater.userName']
            row['updater'] = updater
        });
        let date = new Date();
        let createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        await Stockproductexportbatch.create({
            listOfStockProductIds: idList,
            createdDate,
            creator: exporter
        })
        await stockProductCSVUtils.writeCsvForStock(res.rows, pathCSV);
    },
    async getTemplate({ pathCSV }) {
        await stockProductCSVUtils.getTemplateCSVforStock(pathCSV);
    },
    async importCSV({ path, user }) {
        const csvString = await readCsvFile(path);
        const lines = csvString.trim().split('\r\n');
        const header = lines[0];
        const data = lines.slice(1, lines.length);
        await stockProductCSVUtils.checkHeader({ header, path });
        const parsedData = await stockProductCSVUtils.checkData({ data, path });
        const batch = await stockProductImportBatchController.insertNewBatch({ user });
        const validDataToInsert = parsedData.map(row => {
            const { sku, barcode, type,
                name, price, length, stockGroupId,
                standard, statusId, manufacturedDate, warehouseId } = row;
            let date = new Date();
            let createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
                .toISOString()
                .split("T")[0];
            return {
                sku, barcode, type, name,
                price, length, stockGroupId,
                standard, statusId, manufacturedDate,
                warehouseId, creator: user, createdDate,
                updatedDate: null,
                updater: null,
                isBatchAdded: 1,
                stockProductImportBatchId: batch.stockProductImportBatchId
            }
        });
        const insertedStock = await Stockproduct.bulkCreate(
            validDataToInsert
        );

        if (insertedStock.length !== validDataToInsert.length) {
            const errorWhileInserted = Error('Error while inserting')
            errorWhileInserted.statusCode = 400
            errorWhileInserted.errorList = errors
            throw errorWhileInserted
        }
        return insertedStock;
    },
    async uploadImage({ path, user, option }) {
        // console.log('e')
        if (!option) {
            const errorWhileInserted = Error('Invalid option')
            errorWhileInserted.statusCode = 400
            errorWhileInserted.errorList = ['Option cannot be empty']
            throw errorWhileInserted
        }
        if (!UPLOAD_IMAGE[option]) {
            const errorWhileInserted = Error('Invalid option')
            errorWhileInserted.statusCode = 400
            errorWhileInserted.errorList = ['Option not existed']
            throw errorWhileInserted
        }

        const requestPromise = new Promise((resolve, reject) => {

            // const url = `http://103.200.20.141:8080/ocr_api/${UPLOAD_IMAGE[option]}`
            const url = `http://localhost:5000/ocr_api/${UPLOAD_IMAGE[option]}`
            const options = {
                method: "POST",
                url,
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                formData: {
                    "file": fs.createReadStream(path)
                }
            };
            request(options, function (err, res, body) {
                if (err) reject(err);
                else resolve(body);
            });
        });
        const response = await requestPromise;
        const parsedData = JSON.parse(response);
        const dataToInsert = {}
        if (parsedData.zaiHinname) {
            const steelType = await steeltypeController.getSteelTypeBy({ name: parsedData.zaiHinname });
            if (steelType.data.count === 1) {
                dataToInsert.type = steelType.data.rows[0];
            }
        }
        if (parsedData.zaiKikaku) {
            dataToInsert.name = parsedData.zaiKikaku;
        }
        if (parsedData.zaiLength) {
            dataToInsert.length = parsedData.zaiLength * 1000;
        }
        if (parsedData.zaiCharge) {
            dataToInsert.sku = parsedData.zaiCharge;
        }
        if (parsedData.zaiSozaino) {            
            const barcode = parsedData.zaiSozaino;
            const isnum = /^\d+$/.test(barcode);
            const len = barcode.length;            
            if (!isnum || len === 0) {
                dataToInsert.barcode = '';
            }            
            else if (len < 12) {
                dataToInsert.barcode = '0'.repeat(12 - len) + barcode;
            } else if (len === 12) {
                dataToInsert.barcode = parsedData.zaiSozaino;
            } else {
                dataToInsert.barcode = parsedData.zaiSozaino.substring(0, 12);
            }

        }
        // console.log(JSON.stringify(dataToInsert));
        const a = 1
        return {
            originalData: parsedData,
            dataToInsert
        }
    },
    async getFirstRow() {
        const res = await Stockproduct.findAndCountAll({
            include: [
                { model: SteelType },
                { model: StockProductGroup },
                { model: StockProductStandard },
                { model: StockProductStatus },
                { model: Warehouse },
                { model: Account, as: 'stockProductCreator', attributes: accountTableAttribute },
                { model: Account, as: 'stockProductUpdater', attributes: accountTableAttribute },
                { model: StockProductImportBatch },
            ],
            limit: 1,
            raw: true
        })
        return {
            data: res
        }
    }
}