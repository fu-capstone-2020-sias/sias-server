const db = require("../db");
const Sequelize = require("sequelize");
const StockProductImportBatch = require('../models/stockproductimportbatch')(db, Sequelize.DataTypes)

module.exports = {
    async getALlStockProductImportbatch() {
        const StockProductImportbatch = await StockProductImportBatch.findAndCountAll()
        return {
            data: StockProductImportbatch
        }
    },
    async getStockProductImportbatchBy(where) {
        const StockProductImportbatch = await StockProductImportBatch.findAndCountAll({
            where
        })
        return {
            data: StockProductImportbatch
        }
    },
    async insertNewBatch({user}) {
        let date = new Date();
        let createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        const newBatch = await StockProductImportBatch.create({
            createdDate, 
            creator: user
        });
        return newBatch;
    },
    async find(id) {
        const res = await this.getStockProductImportbatchBy({ stockProductStatusId: id })
        return res
    }
}