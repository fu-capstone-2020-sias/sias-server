const db = require("../db");
const Sequelize = require("sequelize");
const StockProductStandard = require('../models/stockproductstandard')(db, Sequelize.DataTypes);

module.exports = {
    async getAllStockProductStandard() {
        const steelTypes = await StockProductStandard.findAndCountAll({
            order: ['name']
        })
        return {
            data: steelTypes
        }
    },
    async getStockProductStandardBy(where){
        const steelTypes = await StockProductStandard.findAndCountAll({
            where
        })
        return {
            data: steelTypes
        }
    },
    async find(id){
        const res = await this.getStockProductStandardBy({stockProductStandardId: id})
        return res;
    }
}