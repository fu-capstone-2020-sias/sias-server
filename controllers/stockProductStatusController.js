const db = require("../db");
const Sequelize = require("sequelize");
const StockProductStatus = require('../models/stockproductstatus')(db, Sequelize.DataTypes)

module.exports = {
    async getALlStockProductStatus(){
        const stockproductstatus = await StockProductStatus.findAndCountAll()
        return{
            data: stockproductstatus
        }
    },
    async getStockProductStatusBy(where){
        const stockproductstatus = await StockProductStatus.findAndCountAll({
            where
        })
        return {
            data: stockproductstatus
        }
    },
    async find(id){
        const res = await this.getStockProductStatusBy({ stockProductStatusId: id })
        return res
    }
}