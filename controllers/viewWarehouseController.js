const db = require("../db");
const Sequelize = require("sequelize");
const Warehouse = require('../models/warehouse')(db, Sequelize.DataTypes)
const warehouseUtils = require('../utils/warehouseUtils');

const CityProvince = require('../models/cityprovince')(db, Sequelize.DataTypes)
const District = require('../models/district')(db, Sequelize.DataTypes)
const WardCommune = require('../models/wardcommune')(db, Sequelize.DataTypes)
const Department = require('../models/department')(db, Sequelize.DataTypes)
const Account = require('../models/account')(db, Sequelize.DataTypes)

CityProvince.hasMany(Warehouse, { foreignKey: 'cityProvinceId' });
Warehouse.belongsTo(CityProvince, { foreignKey: 'cityProvinceId' });

District.hasMany(Warehouse, { foreignKey: 'districtId' });
Warehouse.belongsTo(District, { foreignKey: 'districtId' });

WardCommune.hasMany(Warehouse, { foreignKey: 'wardCommuneId' });
Warehouse.belongsTo(WardCommune, { foreignKey: 'wardCommuneId' });

Department.hasMany(Warehouse, { foreignKey: 'parentDepartmentId' });
Warehouse.belongsTo(Department, { foreignKey: 'parentDepartmentId' });

Account.hasMany(Warehouse, { foreignKey: 'headOfWarehouse' });
Warehouse.belongsTo(Account, { as: 'head_Of_Warehouse', foreignKey: 'headOfWarehouse' });

Account.hasMany(Warehouse, { foreignKey: 'creator' });
Warehouse.belongsTo(Account, { as: 'warehouseCreator', foreignKey: 'creator' });

Account.hasMany(Warehouse, { foreignKey: 'updater' });
Warehouse.belongsTo(Account, { as: 'warehouseUpdater', foreignKey: 'updater' });

const accountTableAttribute = ['userName', 'firstName', 'lastName']
const stockProductController = require('../controllers/stockProductController');
module.exports = {
    async getWarehouseBy(input) {
        const { offset, limit } = input
        const response = await Warehouse.findAndCountAll({
            offset, limit,
            include: [
                { model: CityProvince },
                { model: District },
                { model: WardCommune },
                { model: Department },
                {
                    model: Account,
                    as: 'head_Of_Warehouse',
                    attributes: accountTableAttribute
                },
                {
                    model: Account,
                    as: 'warehouseCreator',
                    attributes: accountTableAttribute
                },
                {
                    model: Account,
                    as: 'warehouseUpdater',
                    attributes: accountTableAttribute
                },
            ],
        })
        const expectedTotalPages = Math.ceil(response['count'] / limit);
        const totalPages = expectedTotalPages != 0 ? expectedTotalPages : 1;
        const warehouseListRaw = response.rows;
        const warehouseList = warehouseListRaw.map((warehouse) => {
            return warehouse.dataValues
        })
        return {
            count: response['count'],
            totalPages,
            warehouseList
        }
    },
    async insertWarehouse(input) {
        const {
            name, cityProvinceId, districtId, wardCommuneId, address,
            parentDepartmentId, headOfWarehouse, user
        } = input;
        await warehouseUtils.checkNullUndefinedEmptyWarehouse(input);
        await warehouseUtils.checkExistDatabase(input)
        let date = new Date();
        let createdDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        const res = await Warehouse.create({
            name, cityProvinceId, districtId, wardCommuneId, address,
            parentDepartmentId, headOfWarehouse, createdDate,
            updatedDate: null,
            creator: user,
            updater: null,
        });
        return res
    },
    async updateWarehouse(input) {
        const {
            warehouseId, name, cityProvinceId, districtId, wardCommuneId, address,
            parentDepartmentId, headOfWarehouse, updater
        } = input;
        await warehouseUtils.checkNullUndefinedEmptyWarehouseForUpdate(input);
        await warehouseUtils.checkExistDatabaseForUpdate(input)
        let date = new Date();
        let updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
            .toISOString()
            .split("T")[0];
        const res = await Warehouse.update({
            name, cityProvinceId, districtId, wardCommuneId,
            address, parentDepartmentId, headOfWarehouse, updater,
            updatedDate
        }, {
            where: {
                warehouseId,
            },
            returning: true,
            plain: true
        })
        let newData = {}
        if (res) {
            const updatedWarehouse = await this.find(warehouseId);
            newData = updatedWarehouse.data.rows[0]
        } else {
            throw new Error('Data not updated')
        }
        return newData;
    },
    async deleteWarehouse(input) {
        const { warehouseId } = input
        if (warehouseId) {
            const warehouseIsInStockProduct = await stockProductController.getAllStockProduct({ warehouseId })
            if (warehouseIsInStockProduct.data.count === 0) {
                const result = await Warehouse.destroy({
                    where: {
                        warehouseId
                    }
                })
                return result
            }
            else {
                const err = Error('Warehouse is being used in stock product. Cannot delete')
                err.statusCode = 400
                throw err
            }
        } else {
            const err = Error('Invalid warehouse code')
            err.statusCode = 400
            throw err
        }
    },
    async find(id) {
        const res = await Warehouse.findAndCountAll({
            where: {
                warehouseId: id
            },
            include: [
                { model: CityProvince },
                { model: District },
                { model: WardCommune },
                { model: Department },
                {
                    model: Account,
                    as: 'head_Of_Warehouse',
                    attributes: accountTableAttribute
                },
                {
                    model: Account,
                    as: 'warehouseCreator',
                    attributes: accountTableAttribute
                },
                {
                    model: Account,
                    as: 'warehouseUpdater',
                    attributes: accountTableAttribute
                },
            ]
        })
        return {
            data: res
        }
    },
    async getAll() {
        const res = await Warehouse.findAndCountAll({
            attributes: [
                'warehouseId',
                'name'
            ],
            order: ['name']
        })
        return {
            data: res
        }
    }
}