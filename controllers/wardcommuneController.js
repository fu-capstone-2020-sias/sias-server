const db = require("../db");
const Sequelize = require("sequelize");
const Wardcommune = require('../models/wardcommune')(db, Sequelize.DataTypes)

module.exports = {
    async getALlCommune(){
        const communes = await Wardcommune.findAndCountAll({
            raw: true
        })
        return{
            data: communes
        }
    },
    async getCommuneBy(where){
        const communes = await Wardcommune.findAndCountAll({
            where
        })
        const a = 1
        return {
            data: communes
        }
    },
    async find(id){
        const res = await this.getCommuneBy({ wardCommuneId: id })
        const a = 1
        return res
    }
}