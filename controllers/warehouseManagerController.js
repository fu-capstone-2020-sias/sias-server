const db = require("../db");
const Sequelize = require("sequelize");
const { find } = require("./cityProvinceController");
const Account = require('../models/account')(db, Sequelize.DataTypes)
const {ROLE_ID} = require('../constants/constants');

module.exports = {
    async getAllAccount(){
        const accounts = await Account.findAndCountAll({
            attributes: [
                'userName'
            ],
            where: {
                roleId: ROLE_ID.R002
            }
        })
        return {
            data: accounts
        }
    },
    async getAccountBy(where){
        const accounts = await Account.findAndCountAll({
            where,
            attributes: [
                'userName'
            ]
        })
        return {
            data: accounts
        }
    },
    async find(id){
        const res = await this.getAccountBy({ userName: id });
        return res 
    }
}