const Sequelize = require("sequelize");
const config = require("./config");

// mysql://bff86136e47d15:bb5c3579@us-cdbr-east-02.cleardb.com/heroku_5f62c7169681725?reconnect=true

const sequelize = new Sequelize(
  config.AZURE_DB.DATABASE_SCHEMA,
  config.AZURE_DB.DATABASE_USERNAME,
  config.AZURE_DB.DATABASE_PASSWORD,
  {
    host: config.AZURE_DB.DATABASE_HOST,
    dialect: "mysql",
    ssl: true,
    dialectOptions: {
      ssl: {
        require: true,
      },
    },
  }
);

module.exports = sequelize;
