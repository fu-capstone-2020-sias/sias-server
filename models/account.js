/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('account', {
    userName: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    hashedPassword: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    roleId: {
      type: DataTypes.STRING(4),
      allowNull: false,
      references: {
        model: 'role',
        key: 'roleId'
      },
      unique: "roleId"
    },
    departmentId: {
      type: DataTypes.STRING(7),
      allowNull: false,
      references: {
        model: 'department',
        key: 'departmentId'
      },
      unique: "departmentId"
    },
    firstName: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    gender: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    dob: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(62),
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING(11),
      allowNull: false
    },
    cityProvinceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    districtId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    wardCommuneId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(95),
      allowNull: false
    },
    companyInfoId: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    disableFlag: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'account',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userName" },
        ]
      },
      {
        name: "roleId_idx",
        using: "BTREE",
        fields: [
          { name: "roleId" },
        ]
      },
      {
        name: "departmentId_idx",
        using: "BTREE",
        fields: [
          { name: "departmentId" },
        ]
      },
      {
        name: "companyInfoId_idx",
        using: "BTREE",
        fields: [
          { name: "companyInfoId" },
        ]
      },
    ]
  });
};
