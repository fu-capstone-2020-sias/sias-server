/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('companyinfo', {
    companyInfoId: {
      type: DataTypes.STRING(6),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    cityProvinceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    districtId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    wardCommuneId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(95),
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING(11),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(62),
      allowNull: false
    },
    notes: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updater: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "updaterId"
    },
    companyWebsite: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'companyinfo',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "companyInfoId" },
        ]
      },
      {
        name: "updater_idx",
        using: "BTREE",
        fields: [
          { name: "updater" },
        ]
      },
    ]
  });
};
