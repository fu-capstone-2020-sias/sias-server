/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cuttinginstruction', {
    cuttingInstructionId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    orderId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    orderItemId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    totalQuantity: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    steelBarNumber: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    steelLength: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    stockProductId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    stockProductLength: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    assignee: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    bladeWidth: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'cuttinginstruction',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "cuttingInstructionId" },
        ]
      },
    ]
  });
};
