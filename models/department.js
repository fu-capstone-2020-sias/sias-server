/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('department', {
    departmentId: {
      type: DataTypes.STRING(7),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    cityProvinceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    districtId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    wardCommuneId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(95),
      allowNull: false
    },
    parentDepartmentId: {
      type: DataTypes.STRING(7),
      allowNull: false,
      references: {
        model: 'department',
        key: 'departmentId'
      },
      unique: "parentDepartmentId"
    },
    headOfDepartmentId: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    companyInfoId: {
      type: DataTypes.STRING(6),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'department',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "departmentId" },
        ]
      },
      {
        name: "parentDepartmentId_idx",
        using: "BTREE",
        fields: [
          { name: "parentDepartmentId" },
        ]
      },
      {
        name: "headOfDepartmentId_idx",
        using: "BTREE",
        fields: [
          { name: "headOfDepartmentId" },
        ]
      },
      {
        name: "companyInfoId1_idx",
        using: "BTREE",
        fields: [
          { name: "companyInfoId" },
        ]
      },
    ]
  });
};
