/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('imageupload', {
    imageUploadCode: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true
    },
    imageUploadURL: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'imageupload',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "imageUploadCode" },
        ]
      },
    ]
  });
};
