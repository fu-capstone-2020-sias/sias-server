var DataTypes = require("sequelize").DataTypes;
var _account = require("./account");
var _cityprovince = require("./cityprovince");
var _companyinfo = require("./companyinfo");
var _cuttinginstruction = require("./cuttinginstruction");
var _department = require("./department");
var _district = require("./district");
var _feedback = require("./feedback");
var _imageupload = require("./imageupload");
var _order = require("./order");
var _orderexportbatch = require("./orderexportbatch");
var _orderimportbatch = require("./orderimportbatch");
var _orderitem = require("./orderitem");
var _orderstatus = require("./orderstatus");
var _role = require("./role");
var _sessions = require("./sessions");
var _steeltype = require("./steeltype");
var _stockproduct = require("./stockproduct");
var _stockproductexportbatch = require("./stockproductexportbatch");
var _stockproductgroup = require("./stockproductgroup");
var _stockproductimportbatch = require("./stockproductimportbatch");
var _stockproductstandard = require("./stockproductstandard");
var _stockproductstatus = require("./stockproductstatus");
var _wardcommune = require("./wardcommune");
var _warehouse = require("./warehouse");

function initModels(sequelize) {
  var account = _account(sequelize, DataTypes);
  var cityprovince = _cityprovince(sequelize, DataTypes);
  var companyinfo = _companyinfo(sequelize, DataTypes);
  var cuttinginstruction = _cuttinginstruction(sequelize, DataTypes);
  var department = _department(sequelize, DataTypes);
  var district = _district(sequelize, DataTypes);
  var feedback = _feedback(sequelize, DataTypes);
  var imageupload = _imageupload(sequelize, DataTypes);
  var order = _order(sequelize, DataTypes);
  var orderexportbatch = _orderexportbatch(sequelize, DataTypes);
  var orderimportbatch = _orderimportbatch(sequelize, DataTypes);
  var orderitem = _orderitem(sequelize, DataTypes);
  var orderstatus = _orderstatus(sequelize, DataTypes);
  var role = _role(sequelize, DataTypes);
  var sessions = _sessions(sequelize, DataTypes);
  var steeltype = _steeltype(sequelize, DataTypes);
  var stockproduct = _stockproduct(sequelize, DataTypes);
  var stockproductexportbatch = _stockproductexportbatch(sequelize, DataTypes);
  var stockproductgroup = _stockproductgroup(sequelize, DataTypes);
  var stockproductimportbatch = _stockproductimportbatch(sequelize, DataTypes);
  var stockproductstandard = _stockproductstandard(sequelize, DataTypes);
  var stockproductstatus = _stockproductstatus(sequelize, DataTypes);
  var wardcommune = _wardcommune(sequelize, DataTypes);
  var warehouse = _warehouse(sequelize, DataTypes);

  return {
    account,
    cityprovince,
    companyinfo,
    cuttinginstruction,
    department,
    district,
    feedback,
    imageupload,
    order,
    orderexportbatch,
    orderimportbatch,
    orderitem,
    orderstatus,
    role,
    sessions,
    steeltype,
    stockproduct,
    stockproductexportbatch,
    stockproductgroup,
    stockproductimportbatch,
    stockproductstandard,
    stockproductstatus,
    wardcommune,
    warehouse,
  };
}
module.exports = { initModels };
