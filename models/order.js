/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('order', {
    orderId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    orderStatusId: {
      type: DataTypes.STRING(7),
      allowNull: false,
      references: {
        model: 'orderstatus',
        key: 'orderStatusId'
      },
      unique: "orderStatusId"
    },
    customerName: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    customerCompanyName: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    customerCityProvinceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    customerDistrictId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    customerWardCommuneId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    customerAddress: {
      type: DataTypes.STRING(95),
      allowNull: false
    },
    customerPhoneNumber: {
      type: DataTypes.STRING(11),
      allowNull: false
    },
    customerEmail: {
      type: DataTypes.STRING(62),
      allowNull: false
    },
    totalPrice: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator1"
    },
    updater: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "updater2"
    },
    approver: {
      type: DataTypes.STRING(30),
      allowNull: false,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "approver1"
    },
    assignee: {
      type: DataTypes.STRING(30),
      allowNull: false,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "assignee1"
    },
    isBatchAdded: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    orderImportBatchId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'orderimportbatch',
        key: 'orderImportBatchId'
      },
      unique: "orderImportBatchId"
    }
  }, {
    sequelize,
    tableName: 'order',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "orderId" },
        ]
      },
      {
        name: "orderStatusId_idx",
        using: "BTREE",
        fields: [
          { name: "orderStatusId" },
        ]
      },
      {
        name: "creator1_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
      {
        name: "updater2_idx",
        using: "BTREE",
        fields: [
          { name: "updater" },
        ]
      },
      {
        name: "approver1_idx",
        using: "BTREE",
        fields: [
          { name: "approver" },
        ]
      },
      {
        name: "assignee1_idx",
        using: "BTREE",
        fields: [
          { name: "assignee" },
        ]
      },
      {
        name: "orderImportBatchId_idx",
        using: "BTREE",
        fields: [
          { name: "orderImportBatchId" },
        ]
      },
    ]
  });
};
