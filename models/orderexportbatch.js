/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('orderexportbatch', {
    ordeExportBatchId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    listOfOrderIds: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator3"
    }
  }, {
    sequelize,
    tableName: 'orderexportbatch',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ordeExportBatchId" },
        ]
      },
      {
        name: "creator3_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
    ]
  });
};
