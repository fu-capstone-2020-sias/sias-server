/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('orderitem', {
    orderItemId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    orderId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'order',
        key: 'orderId'
      },
      unique: "orderId4"
    },
    length: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    bladeWidth: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    quantity: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator99"
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updater: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "updater99"
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'orderitem',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "orderItemId" },
        ]
      },
      {
        name: "orderId4_idx",
        using: "BTREE",
        fields: [
          { name: "orderId" },
        ]
      },
      {
        name: "updater99_idx",
        using: "BTREE",
        fields: [
          { name: "updater" },
        ]
      },
      {
        name: "creator99_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
    ]
  });
};
