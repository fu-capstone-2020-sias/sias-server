/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('role', {
    roleId: {
      type: DataTypes.STRING(4),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    disableFlag: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'role',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "roleId" },
        ]
      },
    ]
  });
};
