/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stockproduct', {
    stockProductId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    sku: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    barcode: {
      type: DataTypes.STRING(12),
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'steeltype',
        key: 'steeltypeId'
      },
      unique: "type"
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    length: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    stockGroupId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'stockproductgroup',
        key: 'stockProductGroupId'
      },
      unique: "stockgroupid"
    },
    standard: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'stockproductstandard',
        key: 'stockproductstandardId'
      },
      unique: "standard"
    },
    statusId: {
      type: DataTypes.STRING(6),
      allowNull: false,
      references: {
        model: 'stockproductstatus',
        key: 'stockProductStatusId'
      },
      unique: "stockProductStatusId"
    },
    manufacturedDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    warehouseId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'warehouse',
        key: 'warehouseId'
      },
      unique: "warehouseId2"
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator5"
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updater: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "updater5"
    },
    isBatchAdded: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    stockProductImportBatchId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'stockproductimportbatch',
        key: 'stockProductImportBatchId'
      },
      unique: "stockProductImportId"
    }
  }, {
    sequelize,
    tableName: 'stockproduct',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "stockProductId" },
        ]
      },
      {
        name: "stockProductStatusId_idx",
        using: "BTREE",
        fields: [
          { name: "statusId" },
        ]
      },
      {
        name: "creator5_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
      {
        name: "updater5_idx",
        using: "BTREE",
        fields: [
          { name: "updater" },
        ]
      },
      {
        name: "stockgroupid_idx",
        using: "BTREE",
        fields: [
          { name: "stockGroupId" },
        ]
      },
      {
        name: "warehouseId2_idx",
        using: "BTREE",
        fields: [
          { name: "warehouseId" },
        ]
      },
      {
        name: "type_idx",
        using: "BTREE",
        fields: [
          { name: "type" },
        ]
      },
      {
        name: "standard_idx",
        using: "BTREE",
        fields: [
          { name: "standard" },
        ]
      },
      {
        name: "stockProductImportId_idx",
        using: "BTREE",
        fields: [
          { name: "stockProductImportBatchId" },
        ]
      },
    ]
  });
};
