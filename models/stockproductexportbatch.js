/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stockproductexportbatch', {
    stockProductExportBatchId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    listOfStockProductIds: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator6"
    }
  }, {
    sequelize,
    tableName: 'stockproductexportbatch',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "stockProductExportBatchId" },
        ]
      },
      {
        name: "creator6_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
    ]
  });
};
