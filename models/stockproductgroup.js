/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stockproductgroup', {
    stockProductGroupId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    totalQuantity: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator7"
    },
    updater: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "updater6"
    },
    disableFlag: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'stockproductgroup',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "stockProductGroupId" },
        ]
      },
      {
        name: "creator7_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
      {
        name: "updater6_idx",
        using: "BTREE",
        fields: [
          { name: "updater" },
        ]
      },
    ]
  });
};
