/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stockproductimportbatch', {
    stockProductImportBatchId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator8"
    }
  }, {
    sequelize,
    tableName: 'stockproductimportbatch',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "stockProductImportBatchId" },
        ]
      },
      {
        name: "creator8_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
    ]
  });
};
