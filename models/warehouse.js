/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('warehouse', {
    warehouseId: {
      autoIncrement: true,
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    cityProvinceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    districtId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    wardCommuneId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(95),
      allowNull: false
    },
    parentDepartmentId: {
      type: DataTypes.STRING(7),
      allowNull: false,
      references: {
        model: 'department',
        key: 'departmentId'
      },
      unique: "parentDepartmentId6"
    },
    headOfWarehouse: {
      type: DataTypes.STRING(30),
      allowNull: false,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "headOfWarehouseId3"
    },
    createdDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    creator: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "creator9"
    },
    updater: {
      type: DataTypes.STRING(30),
      allowNull: true,
      references: {
        model: 'account',
        key: 'userName'
      },
      unique: "updater9"
    }
  }, {
    sequelize,
    tableName: 'warehouse',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "warehouseId" },
        ]
      },
      {
        name: "parentDepartmentId6_idx",
        using: "BTREE",
        fields: [
          { name: "parentDepartmentId" },
        ]
      },
      {
        name: "headOfWarehouseId3_idx",
        using: "BTREE",
        fields: [
          { name: "headOfWarehouse" },
        ]
      },
      {
        name: "creator9_idx",
        using: "BTREE",
        fields: [
          { name: "creator" },
        ]
      },
      {
        name: "updater9_idx",
        using: "BTREE",
        fields: [
          { name: "updater" },
        ]
      },
    ]
  });
};
