const StockProductAlias = require('../constants/stockProductAlias');
const StockProductCSVModel = require('../constants/stockProductImportModel');

module.exports = {
    parseLineToStockProduct(datas) {
        const res = [];
        const errors = []
        datas.forEach((line, index) => {
            const rows = line.trim().split(',');
            if (rows.length < StockProductCSVModel.length) {
                errors.push(`Line ${index + 1} is missing a column`)
            } else {
                const row = {}
                StockProductCSVModel.forEach((column, index) => {
                    row[column.colName] = rows[index].trim();
                })
                res.push(row);
            }
        })
        const a = 0;
        if (errors.length > 0) {
            const invalidFieldError = Error('Missing data values')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
        return res;
    }
}