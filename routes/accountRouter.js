const express = require("express");
const bodyParser = require("body-parser");
var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");

const accountController = require("../controllers/accountController");
const {ROLE_ID} = require("../constants/constants");

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

// URL: /account?page=number---> For example, /account?page=1
router.get(
    "/",
    // corsUtils.corsWithOptions,
    authenticateUtils.isAuthenticated,
    function (req, res, next) {
        // const jsonObj =
        if (req.user.roleId === ROLE_ID.R001) {
            const result = accountController.getAccountList(req, res);
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    }
);

// URL: /account/:id ---> For example, /account/hoangch1
router.get(
    "/:userName",
    // corsUtils.corsWithOptions,
    authenticateUtils.isAuthenticated,
    function (req, res, next) {
        // const jsonObj =
        const result = accountController.getAccountByUsername(req, res);
    }
);

// URL: /account/search ---> For example, /account/search
router.post(
    "/search",
    authenticateUtils.isAuthenticated,
    function (req, res) {
        if (req.user.roleId === ROLE_ID.R001
            // || req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004
        ) {
            const result = accountController.getAccountListByParams(req, res);
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    })

// URL: /account ---> For example, /account
router.post(
    "/",
    authenticateUtils.isAuthenticated,
    function (req, res) {
        if (req.user.roleId === ROLE_ID.R001) {
            const result = accountController.insertNewAccount(req, res);
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    })

// URL: /account ---> For example, /account
router.put(
    "/",
    authenticateUtils.isAuthenticated,
    function (req, res) {
        const result = accountController.updateAccount(req, res);

        if (result !== null) {
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    })

// URL: /account ---> For example, /account, send with req.body.arrOfUsernames, arrOfUsernames = ["t1, "t2", "t3]
router.delete(
    "/",
    authenticateUtils.isAuthenticated,
    function (req, res) {
        if (req.user.roleId === ROLE_ID.R001) {
            const result = accountController.deleteDisabledAccount(req, res);
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    })

module.exports = router;
