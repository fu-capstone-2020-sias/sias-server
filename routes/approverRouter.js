const express = require("express");
const bodyParser = require("body-parser");
var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");

const db = require("../db");
const Sequelize = require("sequelize");
const Account = require("../models/account")(db, Sequelize.DataTypes);

const accountController = require("../controllers/accountController");
const {ROLE_ID} = require("../constants/constants");

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

router.get(
    "/",
    // corsUtils.corsWithOptions,
    authenticateUtils.isAuthenticated,
    async function (req, res, next) {
        const listOfApprover = await Account.findAll({
            where: {
                roleId: ROLE_ID.R002 // Office Manager
            },
            attributes: {exclude: ['hashedPassword']},
        })

        res.json({
            listOfApprover: listOfApprover
        })
    }
);

module.exports = router;