const express = require("express");
const bodyParser = require("body-parser");
// var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils"); // Bắt buộc phải import ông thần này để passport.authenticate("local") mới hoạt động
var passport = require("passport");
var cors = require('cors')
var passwordUtils = require("../utils/passwordUtils");
var mailUtils = require("../utils/mailUtils");
const db = require("../db")
const Sequelize = require("sequelize");
const {ALLOWED_ORIGINS} = require("../constants/constants");
const Account = require("../models/account")(db, Sequelize.DataTypes);

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    (req, res) => {
        res.sendStatus(200);
    }
);

// var allowedOrigins = ['http://localhost:3001',
//     'http://yourapp.com'];

// URL: /auth/login
router.post(
    "/login",
    // cors({
    //     origin: function (origin, callback) {
    //         // allow requests with no origin
    //         // (like mobile apps or curl requests)
    //         if (!origin) return callback(null, true);
    //         if (ALLOWED_ORIGINS.indexOf(origin) === -1) {
    //             var msg = 'The CORS policy for this site does not ' +
    //                 'allow access from the specified Origin.';
    //             return callback(new Error(msg), false);
    //         }
    //         return callback(null, true);
    //     }
    // }),
    async function (req, res, next) {
        req.params.userName = req.body.userName; // set request parameter
        req.params.login = true; // set request parameter

        if (req.user !== undefined) {
            if (req.body.userName === req.user.userName) {
                res.statusCode = 400;
                res.json({
                    error: ["Hey there! You are logged in, please don't authenticate yourself again."]
                })
                return;
            } else {
                res.statusCode = 400;
                res.json({
                    error: ["Hey there! You must log out your current account" + " (" + req.user.userName +
                    ") " + "in order to login another account."]
                })
                return;
            }
        }

        passport.authenticate("local", {session: true}, (err, user, info) => {
            if (info !== undefined) { // Change default message of Passport
                res.statusCode = 400;
                res.json(info); // If message (error or success) exists, return to the client
                return;
            }

            // If you are using the authenticate callback when you authenticate with
            // passport you need to log the user in manually.
            // It will not be called for you.
            req.logIn(user, function (err) { // <-- Log user in
                delete user.hashedPassword; // Hide hashed password from the client for secure purpose
                res.json({
                    message: "Welcome back, " + user.userName + ". You are now logged in.", account: user
                });
            });

            // If the client has stored SESSION ID in browser (previous login),
            // then Passport can restore authentication of the user by using SESSION ID

        })(req, res, next);
    })

// URL: /auth/logout
router.post(
    "/logout",
    authenticateUtils.isAuthenticated,
    function (req, res) {
        try {
            const userName = req.user.userName;
            req.logout(); // Passport built-in method
            console.log("Logout successfully");
            req.session.destroy(); // Destroy the session both on database and express server
            console.log(req.session);
            res.clearCookie('connect.sid', {path: '/'});
            res.json({
                message: "User with username: " + userName + " is logged out. See you again!"
            })
        } catch (err) {
            res.statusCode = 500;
            res.json({
                error: [err]
            })
        }
    });

// URL: /forgotPassword/tinsmith111
router.post("/forgotPassword/:userName", async function (req, res) {
    let account;
    try {
        account = await Account.findByPk(req.params.userName);
        if (!account) {
            res.statusCode = 400;
            res.json({
                error: ["Account with username: " + req.params.userName + " not existed."]
            });
            return;
        } else {
            if (account.disableFlag === 1) {
                res.statusCode = 400;
                res.json({
                    error: ["Account with userName: " + req.params.userName + " is DISABLED. Please contact Administrator for more information."]
                });
                return;
            }
        }
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: [err]
        });
        return;
    }

    let randomstring = require("randomstring");
    // Generate a random password for the user to reset his/her password
    let newPassword = randomstring.generate({
        length: 8,
        charset: 'alphabetic'
    });
    let tailPassword = randomstring.generate({
        length: 4,
        charset: 'numeric'
    });

    let uppercasePassword = randomstring.generate({
        length: 4,
        charset: 'SIAS'
    });

    let finalPass = uppercasePassword + newPassword + '@' + tailPassword;

    account.hashedPassword = await passwordUtils.hashBcryptPassword(finalPass);
    let date = new Date();
    account.updatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString()
        .split("T")[0];


    await account.save();
    var ip_address = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null);

    try {
        await mailUtils.sendMail(finalPass, account.email, ip_address);
    } catch (err) {
        res.statusCode = 500;
        res.json({
            error: ["Server encountered problems. Try again later."]
        });
        return;
    }

    res.json({
        message: `An email has been sent to your inbox (${account.email}). Please check it.`,
        email: account.email
    })

});


module.exports = router;