const express = require('express');
const router = express.Router();
const cityProvinceController = require('../controllers/cityProvinceController');
const authenticationUtils = require('../utils/authenticateUtils');
const {handleError} = require('../utils/commonUtils')

router.get('/', authenticationUtils.isAuthenticated, async (req , res) => {
    try{
        const result = await cityProvinceController.getAllCityProvince()
        res.status(200).send({
            data: result.data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router