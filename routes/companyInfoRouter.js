const express = require("express");
const bodyParser = require("body-parser");
// var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");

const companyInfoController = require("../controllers/companyInfoController");
const {isAuthenticated} = require("../utils/authenticateUtils");
const {ROLE_ID} = require("../constants/constants");

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

// URL: /company
router.get(
    "/",
    isAuthenticated,
    async function (req, res) {
        await companyInfoController.getCompanyInfoById(req, res);
    })

router.put(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R001) {
            await companyInfoController.updateCompanyInfo(req, res);
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    })

module.exports = router;