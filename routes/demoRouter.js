const express = require("express");
const bodyParser = require("body-parser");
var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");
var csvUtils = require("../utils/csvUtils");
var socketUtils = require("../utils/socketUtils");
const csvWriterForOrder = require("../utils/csvUtils").csvWriterForOrder;
const multerUpload = require("../utils/uploadUtils").multerUpload;
const db = require("../db");
const Sequelize = require("sequelize");
const OrderImportBatch = require("../models/orderimportbatch")(db, Sequelize.DataTypes);
const CuttingInstruction = require("../models/cuttinginstruction")(db, Sequelize.DataTypes);
const StockProduct = require("../models/stockproduct")(db, Sequelize.DataTypes);
const OrderStatus = require("../models/orderstatus")(db, Sequelize.DataTypes);
const Order = require("../models/order")(db, Sequelize.DataTypes);
const fs = require("fs");

var parse = require("csv-parse");
var csvFile = "data.csv"; // TEST PURPOSE

const orderController = require("../controllers/orderController");
const cuttingInsController = require("../controllers/cuttingInstructionController");
const {ROLE_ID} = require("../constants/constants");
var net = require('net');
const {ORDER_STATUS_ID} = require("../constants/constants");
const {ORDER_STATUS_NAME} = require("../constants/constants");
const {SOCKET_SERVER} = require("../constants/constants");

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

router.post(
    "/createInstruction",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        // R005 === ACCOUNT DEMO CHO KHÁCH
        if (req.user.roleId === "R005") {
            let steelAvailable = req.body.steelAvailable; // arr
            let steelToCut = req.body.steelToCut; // arr
            let bladeWidth = req.body.bladeWidth;

            let sumSteelAvailable = 0;
            let sumSteelToCut = 0;

            let availableArr = steelAvailable.split(",");
            let cutArr = steelToCut.split(",");

            try {
                for (let i = 0; i < availableArr.length; i++) {
                    if (isNaN(availableArr[i])) {
                        throw new Error();
                    }

                    sumSteelAvailable += parseInt(availableArr[i]);
                }

                for (let i = 0; i < cutArr.length; i++) {
                    if (isNaN(cutArr[i])) {
                        throw new Error();
                    }

                    sumSteelToCut += parseInt(cutArr[i]);
                }

                if (sumSteelAvailable < sumSteelToCut) {
                    res.statusCode = 400;
                    res.json({
                        message: "ERROR: THÉP CÓ SẴN không đủ độ dài cắt!"
                    })
                    return;
                }
            } catch (err) {
                res.statusCode = 400;
                res.json({
                    message: "ERROR: Kiểm tra lại các input đầu vào!"
                })
            }

            let firstString = steelAvailable + "|" + steelToCut + "|" + bladeWidth;

            let zeroString = "";
            let firstCount = ("|" + firstString).length + "";
            let countString = firstCount.length;

            for (let z = 1; z <= (6 - countString); z++) {
                zeroString += "0"
            }

            zeroString += firstCount;

            let finalString = zeroString + "|" + firstString;
            req.body.finalString = finalString;

            let stringInstruction = await socketUtils.sendRequestToSocketServer(req, res);
            let strInsArr = stringInstruction.split(",");

            res.json({
                message: `Thanh có sẵn: ${steelAvailable} | Thanh cần cắt: ${steelToCut} | Độ rộng lưỡi cắt: ${bladeWidth} 
                | Chỉ dẫn cắt: ` + strInsArr + " (ứng với vị trí của Thanh có sẵn, bắt đầu từ index 0)"
            })
        }
    })

module.exports = router;