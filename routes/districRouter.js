const express = require('express');
const router = express.Router();
const districtController = require('../controllers/districtController');
const authenticationUtils = require('../utils/authenticateUtils');
const {handleError} = require('../utils/commonUtils')

router.get('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        const districts = await districtController.getAllDistrictProvince();
        res.status(200).send({
            data: districts.data
        })
    } catch (e) {
        handleError(res, e)
    }
})

router.get('/:cityProvinceId', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        const districts = await districtController.getDistrictBy({
                cityProvinceId: req.params.cityProvinceId
        });
        res.status(200).send({
            data: districts.data
        })
    } catch (e) {
        handleError(res, e)
    }
})

module.exports = router