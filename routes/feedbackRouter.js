const express = require("express");
const bodyParser = require("body-parser");
// var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");

const feedbackController = require("../controllers/feedbackController");
const {isAuthenticated} = require("../utils/authenticateUtils");
const {ROLE_ID} = require("../constants/constants");

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

router.get(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R001) {
            await feedbackController.getFeedbackList(req, res);
            return;
        }

        res.statusCode = 401;
        res.json({
            error: ["401 - Unauthorized Access."]
        })
    })

router.post(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (/^R00[1-4]$/.test(req.user.roleId)) {
            await feedbackController.insertNewFeedback(req, res);
        }
    })

module.exports = router;