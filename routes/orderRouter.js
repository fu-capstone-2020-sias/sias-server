const express = require("express");
const bodyParser = require("body-parser");
var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");
var csvUtils = require("../utils/csvUtils");
var socketUtils = require("../utils/socketUtils");
const csvWriterForOrder = require("../utils/csvUtils").csvWriterForOrder;
const multerUpload = require("../utils/uploadUtils").multerUpload;
const db = require("../db");
const Sequelize = require("sequelize");
const OrderImportBatch = require("../models/orderimportbatch")(db, Sequelize.DataTypes);
const CuttingInstruction = require("../models/cuttinginstruction")(db, Sequelize.DataTypes);
const StockProduct = require("../models/stockproduct")(db, Sequelize.DataTypes);
const OrderStatus = require("../models/orderstatus")(db, Sequelize.DataTypes);
const Order = require("../models/order")(db, Sequelize.DataTypes);
const fs = require("fs");

var parse = require("csv-parse");
var csvFile = "data.csv"; // TEST PURPOSE

const orderController = require("../controllers/orderController");
const cuttingInsController = require("../controllers/cuttingInstructionController");
const {ROLE_ID} = require("../constants/constants");
var net = require('net');
const {ORDER_STATUS_ID} = require("../constants/constants");
const {ORDER_STATUS_NAME} = require("../constants/constants");
const {SOCKET_SERVER} = require("../constants/constants");

const router = express.Router();
router.use(bodyParser.json());

// This function is used for interacting with preflight requests
router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

router.get(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004 || req.user.roleId === ROLE_ID.R003) {
            await orderController.getOrderList(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

// URL: /cuttingInstruction?orderId=4
router.get(
    "/cuttingInstruction",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R003) {

            if (req.query.orderId === null || req.query.orderId === undefined
                || req.query.orderId === '') {
                res.statusCode = 400;
                res.json({
                    error: ["Missing orderId parameter!"]
                })
                return;
            }

            let result = await cuttingInsController.getCuttingInstructionByOrderId(req, res);
            let order = await Order.findAll({
                where: {
                    orderId: req.query.orderId,
                    assignee: req.user.userName
                }
            })

            if (order.length === 0) {
                res.statusCode = 400;
                res.json({
                    error: ["There is no cutting instruction for this order (Order ID: " + req.query.orderId + ") or " +
                    "you are not Assignee for this cutting instruction."]
                })
                return;
            }

            let orderStatus = await OrderStatus.findAll({
                where: {
                    orderStatusId: order[0].dataValues.orderStatusId
                }
            })

            order[0].dataValues.orderStatus = orderStatus[0].dataValues.name;

            if (result.length === 0) {
                res.statusCode = 400;
                res.json({
                    error: ["There is no cutting instruction for this order (Order ID: " + req.query.orderId + ") or " +
                    "you are not Assignee for this cutting instruction."]
                })
                return;
            }

            res.json({
                cuttingInstruction: result,
                order: order
            })
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)

// Receive an array of Order ID
router.get(
    "/sampleFileForImportOrderByBatch",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            const pathCSV = './downloads/samples/sample_file_import_order.csv';

            res.download(pathCSV); // Set disposition and send it.
            console.log("File CSV name: " + pathCSV + " is exported successfully.");
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)

// /order/item
router.get(
    "/:orderId",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004 || req.user.roleId === ROLE_ID.R003) {
            if (req.params.orderId === "item") {
                await orderController.getOrderItemList(req, res);
                return;
            }

            await orderController.getOrderById(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

async function checkIfStockProductAvailable(orderItemList, req, res) {

    let totalLengthOfOrderItems = 0;
    for (let i = 0; i < orderItemList.length; i++) {
        totalLengthOfOrderItems += (orderItemList[i].length * orderItemList[i].quantity);
    }

    let totalLengthOfStockProduct = await cuttingInsController.getTotalLengthOfStockProduct(req, res);

    if (totalLengthOfStockProduct[0].totalLength < totalLengthOfOrderItems) {
        return "There are not enough stock products in all warehouses!";
    }

    // for (let i = 0; i < orderItemList.length; i++) {
    //
    //     let qty = orderItemList[i].quantity;
    //     let length = orderItemList[i].length;
    //     let sum = qty * length;
    //
    //     // Find stock product có length >= total length of 1 order item
    //     let minLength = length;
    //     let maxLength = length * qty + 5000;
    //
    //     req.body.minLength = minLength;
    //     req.body.maxLength = maxLength;
    //     req.body.stockProductIdSelectedArr = stockProductIdSelectedArr; // Hiện đang để là rỗng vì ở cuối đã cập nhật lại length cho stock product bị cắt
    //     req.body.cityProvinceId = order.customerCityProvinceId;
    //     req.body.districtId = order.customerDistrictId;
    //     req.body.wardCommuneId = order.customerWardCommuneId;
    //     req.body.sumOfOrderItem = sum;
    //
    //     let stockProductFinds = await cuttingInsController.getStockProductAvailableForCutting(req, res);
    //
    //     // Trong trường hợp tất cả các kho đều ko đủ stock product để cắt
    //     if (stockProductFinds !== undefined) {
    //         if (stockProductFinds.message !== undefined && stockProductFinds.message !== "") {
    //             message = stockProductFinds.message;
    //             break;
    //         }
    //     }
    // }

    return undefined;
}

// Nếu order có status là Approved rồi và factory manager nhấn button yêu cầu get instruction
// ở màn order detail, một request create insutrction được gửi đi
// URL: /createInstruction gửi kèm orderId trong request body
router.post(
    "/createInstruction",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        // res.statusCode = 400;
        // res.json({
        //     error: ["There is not enoguh stock product to cut from!!! Please add more!"]
        // });
        // return;

        if (req.user.roleId === ROLE_ID.R003) { // Factory Manager
            req.body.createInstruction = true; // For orderController.js
            req.params.orderId = req.query.orderId;
            let order = await Order.findByPk(req.query.orderId);
            let orderItemList = await orderController.getOrderItemList(req, res);
            let cuttingInstruction = await CuttingInstruction.findAll({
                where: {
                    orderId: req.query.orderId
                }
            });


            if (order !== undefined && order !== null) {
                if (order.orderStatusId !== ORDER_STATUS_ID.OS003) {
                    res.statusCode = 400;
                    res.json({
                        error: ["This order (Order ID: " + req.params.orderId + ") must have status of 'Processing' (OS003)."]
                    })
                    return;
                }
            } else {
                res.statusCode = 400;
                res.json({
                    error: ["Missing orderId parameter!"]
                })
                return;
            }

            if (cuttingInstruction.length > 0) {
                res.statusCode = 400;
                res.json({
                    error: ["This order (Order ID: " + req.query.orderId + ") has existed Cutting Instruction. You can not create a new one!"]
                });
                return;
            }

            if (orderItemList.length === 0) {
                res.statusCode = 400;
                res.json({
                    error: ["Sorry, you have 0 (ZERO) order to process."]
                })
                return;
            }

            let messageNotEnough = await checkIfStockProductAvailable(orderItemList, req, res);

            if (messageNotEnough !== undefined) {
                res.statusCode = 400;
                res.json({
                    error: [messageNotEnough]
                })

                return;
            }

            let stockProductIdSelectedArr = [];
            for (let i = 0; i < orderItemList.length; i++) {
                let qty = orderItemList[i].quantity;
                let length = orderItemList[i].length;
                let bladeWidth = orderItemList[i].bladeWidth;
                let minLength = length;
                let maxLength = length * qty + 1000000; // max length infinity and beyond
                let sum = length * qty;

                // Find stock product có length >= total length of 1 order item
                let stockProductIdArr = [];
                let stockProductLength = [];
                req.body.minLength = parseInt(minLength, 10) + parseInt(bladeWidth, 10); // Bù trừ bladeWidth
                req.body.maxLength = maxLength;
                req.body.stockProductIdSelectedArr = stockProductIdSelectedArr; // Hiện đang để là rỗng vì ở cuối đã cập nhật lại length cho stock product bị cắt
                req.body.cityProvinceId = order.customerCityProvinceId;
                req.body.districtId = order.customerDistrictId;
                req.body.wardCommuneId = order.customerWardCommuneId;
                req.body.sumOfOrderItem = sum;

                let stockProductFinds = await cuttingInsController.getStockProductAvailableForCutting(req, res);

                // Trong trường hợp tất cả các kho đều ko đủ stock product để cắt
                if (stockProductFinds !== undefined) {
                    if (stockProductFinds.message !== undefined && stockProductFinds.message !== "") {
                        res.statusCode = 400;
                        res.json({error: [stockProductFinds.message]});
                        return;
                    }
                }
            }


            stockProductIdSelectedArr = [];
            for (let i = 0; i < orderItemList.length; i++) {
                let steelBarToBeCut = "";
                let qty = orderItemList[i].quantity;
                let bladeWidth = orderItemList[i].bladeWidth;
                let length = orderItemList[i].length;

                for (let i = 1; i <= qty; i++) {
                    steelBarToBeCut = steelBarToBeCut + length + ",";
                }

                // Remove last ","
                steelBarToBeCut = steelBarToBeCut.substr(0, steelBarToBeCut.length - 1);
                steelBarToBeCut = steelBarToBeCut + "|" + bladeWidth;

                // Find stock product có length >= total length of 1 order item
                let stockProductIdArr = [];
                let stockProductLength = [];
                let minLength = length;
                let maxLength = length * qty + 1000000; // max length infinity and beyond
                let sum = length * qty;

                req.body.minLength = parseInt(minLength, 10) + parseInt(bladeWidth, 10); // Bù trừ bladeWidth
                req.body.maxLength = maxLength;
                req.body.stockProductIdSelectedArr = stockProductIdSelectedArr; // Hiện đang để là rỗng vì ở cuối đã cập nhật lại length cho stock product bị cắt
                req.body.cityProvinceId = order.customerCityProvinceId;
                req.body.districtId = order.customerDistrictId;
                req.body.wardCommuneId = order.customerWardCommuneId;
                req.body.sumOfOrderItem = sum;

                let stockProductFinds = await cuttingInsController.getStockProductAvailableForCutting(req, res);

                // Trong trường hợp tất cả các kho đều ko đủ stock product để cắt
                if (stockProductFinds !== undefined) {
                    if (stockProductFinds.message !== undefined && stockProductFinds.message !== "") {
                        res.statusCode = 400;
                        res.json({error: [stockProductFinds.message]});
                        return;
                    }
                }

                for (let j = 0; j < stockProductFinds.length; j++) {
                    stockProductIdArr.push(stockProductFinds[j].stockProductId);
                    stockProductLength.push(stockProductFinds[j].length);
                }

                // Create final message to send to Socket server
                let stockProductAvailable = stockProductLength
                    .toString()
                    .replace("[", "")
                    .replace("]", "");
                stockProductAvailable += "|";

                let zeroString = "";
                let firstCount = ("|" + stockProductAvailable + steelBarToBeCut).length + "";
                let countString = firstCount.length;

                for (let z = 1; z <= (6 - countString); z++) {
                    zeroString += "0"
                }

                zeroString += firstCount;

                let finalString = zeroString + "|" + stockProductAvailable + steelBarToBeCut;
                req.body.finalString = finalString;

                let stringInstruction = await socketUtils.sendRequestToSocketServer(req, res);
                let strInsArr = stringInstruction.split(",");

                let orderIdToDeleteIfError = req.query.orderId;
                try {
                    for (let j = 0; j < strInsArr.length; j++) {
                        // stockProductIdSelectedArr.push(strInsArr[j]);
                        let tempCuttingInsObj = {};

                        tempCuttingInsObj.orderId = orderItemList[i].orderId;
                        tempCuttingInsObj.orderItemId = orderItemList[i].orderItemId;
                        tempCuttingInsObj.totalQuantity = orderItemList[i].quantity;
                        tempCuttingInsObj.steelBarNumber = j + 1;
                        tempCuttingInsObj.steelLength = orderItemList[i].length;
                        tempCuttingInsObj.stockProductId =
                            stockProductIdArr[strInsArr[j]];
                        tempCuttingInsObj.bladeWidth = orderItemList[i].bladeWidth;

                        // Lấy length mới nhát của stock product cho cutting instruction
                        let stockProduct = await StockProduct.findByPk(tempCuttingInsObj.stockProductId);
                        stockProduct.length = stockProduct.length - (tempCuttingInsObj.steelLength) - bladeWidth;
                        stockProduct = await stockProduct.save();

                        tempCuttingInsObj.stockProductLength =
                            stockProduct.length;
                        tempCuttingInsObj.assignee = orderItemList[i].assignee;

                        await CuttingInstruction.create(tempCuttingInsObj);

                    }
                } catch (err) {
                    // await CuttingInstruction.destroy({
                    //     where: {
                    //         orderId: orderIdToDeleteIfError
                    //     }
                    // })
                    res.json({
                        error: ["Error occurred"]
                    });
                    return;
                }
            }

            res.statusCode = 200;
            res.json({
                message: "Created cutting instruction for order (ID: " + req.query.orderId + ") successfully."
            })
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)

router.post(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            await orderController.insertNewOrder(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

// router.post(
//     "/:orderId/item",
//     authenticateUtils.isAuthenticated,
//     async function (req, res) {
//         if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
//             await orderController.insertNewOrderItem(req, res);
//             return;
//         }
//
//         let temp = [];
//         temp.push("401 - Unauthorized Access.")
//         res.statusCode = 401;
//         res.json({
//             error: temp
//         })
//     })

router.post(
    "/:orderId/item",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            // await orderController.insertNewOrderItem(req, res);
            await orderController.insertNewOrderItemList(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

router.put(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            await orderController.updateOrder(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

router.put(
    "/:orderId/item",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            await orderController.updateOrderItemList(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

// URL: /changeOrderStatus?orderId=5&orderStatusId=OS001
router.put(
    "/changeOrderStatus",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R003) { // Bổ sung thêm cho Factory Manager
            await orderController.changeOrderStatusById(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)

router.delete(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002) {
            await orderController.deleteRejectedOrders(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)

// Delete order item from order
router.delete(
    "/items",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) { // Office Manager, Office Employee
            await orderController.deleteOrderItems(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

router.post(
    "/search",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004 || req.user.roleId === ROLE_ID.R003) {
            await orderController.getOrderListByParams(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

router.post(
    "/markDone",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R003) {
            await orderController.markOrderAsDone(req, res);
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    })

router.post(
    "/exportOrderCSV",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            const resultCSV = await orderController.downloadCSVByArrOfOrderIds(req, res);
            const pathCSV = './downloads/' + Date.now() + "_exportedCSV.csv";

            try {
                await csvWriterForOrder(pathCSV, resultCSV);
            } catch (err) {
                res.statusCode = 500;
                res.json({
                    message: err
                })
            }

            res.download(pathCSV); // Set disposition and send it.
            console.log("File CSV name: " + pathCSV + " is exported successfully.");
            return;
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)

router.post(
    "/orderCSV",
    authenticateUtils.isAuthenticated,
    multerUpload.single("csvFile"),
    async function (req, res) {
        if (req.user.roleId === ROLE_ID.R002 || req.user.roleId === ROLE_ID.R004) {
            try {
                let fileName;
                let fileUploader;
                if (req.params.fileNameAndUploader !== undefined) {
                    let result = req.params.fileNameAndUploader.split(",");
                    fileName = result[0];
                    fileUploader = result[1];
                }

                let messageTemp = [];
                if (fileUploader === req.user.userName) {
                    const csvString = csvUtils.readCsvFile("./uploads/" + fileName);

                    if (csvString.indexOf('Error') !== -1) {
                        let tempArr = csvString.split("\n");

                        res.statusCode = 400;
                        res.json({
                            error: tempArr
                        })
                        return;

                    }

                    let result = await csvUtils.validateCsvFileByRules(csvString, csvUtils.orderCsvRules);
                    console.log(result);

                    if (!result.status) { // Something Error...
                        fs.unlinkSync("./uploads/" + fileName);
                        res.json({
                            error: result.errorMessage
                        })
                        return;
                    }

                    let date = new Date();
                    let orderImportBatchCreatedDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
                        .toISOString()
                        .split("T")[0];
                    const orderImportBatch = await OrderImportBatch.create({
                        createdDate: orderImportBatchCreatedDate,
                        creator: req.user.userName
                    })

                    for (let i = 0; i < result.data.data.length; i++) {
                        // QA: createdDate
                        result.data.data[i].createdDate = orderImportBatchCreatedDate;

                        // QA: creator
                        result.data.data[i].creator = req.user.userName;

                        result.data.data[i].orderImportBatchId = orderImportBatch.orderImportBatchId;
                        await orderController.insertNewOrderForCSV(result.data.data[i], res);
                    }

                    res.json({
                        message: "Your CSV file is uploaded and validated successfully."
                    })
                } else {
                    res.statusCode = 400;
                    res.json({
                        // error: ["1. Invalid CSV format; 2. File của user này bị đọc nhầm bởi user khác, fix nó đi."]
                        error: ["1. Invalid CSV format; 2. Error occurred."]
                    })
                }
                return;
            } catch (error) {
                console.error(error);
                res.statusCode = 400;
                res.json({
                    // error: ["1. Invalid CSV format; 2. File của user này bị đọc nhầm bởi user khác, fix nó đi."]
                    error: ["1. Invalid CSV format; 2. Error occurred."]
                })
            }
        }

        let temp = [];
        temp.push("401 - Unauthorized Access.")
        res.statusCode = 401;
        res.json({
            error: temp
        })
    }
)


module.exports = router;