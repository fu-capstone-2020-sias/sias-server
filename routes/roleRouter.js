const express = require("express");
const bodyParser = require("body-parser");
// var passport = require("passport");
var authenticateUtils = require("../utils/authenticateUtils");
var roleController = require("../controllers/roleController");

const router = express.Router();
router.use(bodyParser.json());

router.options(
    "*",
    // corsUtils.corsWithOptions,
    (req, res) => {
        res.sendStatus(200);
    }
);

router.get(
    "/",
    authenticateUtils.isAuthenticated,
    async function (req, res) {
        await roleController.getAllRoles(req, res);
    })

module.exports = router;