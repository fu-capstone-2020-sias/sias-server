const express = require('express');
const router = express.Router();
const authenticationUtils = require('../utils/authenticateUtils');
const { handleError } = require('../utils/commonUtils')
const steelTypeController = require('../controllers/steelTypeController')

router.get('/', authenticationUtils.isAuthenticated, async(req , res) => {
    try{
        const data = await steelTypeController.getAllSteelType();
        res.status(200).send ({
            data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router;