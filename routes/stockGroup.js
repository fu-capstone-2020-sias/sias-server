const express = require('express');
const router = express.Router();
const authenticationUtils = require('../utils/authenticateUtils');
const { handleError } = require('../utils/commonUtils')
const stockGroupController = require('../controllers/stockGroupController')

router.get('/', authenticationUtils.isAuthenticated, async (req , res) => {
    try{
        const data = await stockGroupController.getAllStockGroup();
        res.status(200).send ({
            data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router;