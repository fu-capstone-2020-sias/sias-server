const express = require('express');
const router = express.Router();
const authenticationUtils = require('../utils/authenticateUtils');
const { handleError } = require('../utils/commonUtils')
const stockProductImportBatchController = require('../controllers/stockProductImportBatchController');

router.get('/', authenticationUtils.isAuthenticated, async (req , res) => {
    try{
        const data = await stockProductImportBatchController.getALlStockProductImportbatch();
        res.status(200).send ({
            data
        })
    }catch(e){
        handleError(res, e)
    }
});
router.post('/', authenticationUtils.isAuthenticated, async (req , res) => {
    try{
        const data = await stockProductImportBatchController.insertNewBatch({user: req.user.userName});
        res.status(200).send ({
            data
        })
    }catch(e){
        handleError(res, e)
    }
});

module.exports = router;