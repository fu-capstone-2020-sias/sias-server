const express = require('express');
const router = express.Router();
const stockProductController = require('../controllers/stockProductController')
const { DEFAULT_ITEM_PER_PAGE, ROLE_ID } = require('../constants/constants');
const authenticationUtils = require('../utils/authenticateUtils');
const roles02and03 = [ROLE_ID.R002, ROLE_ID.R003]
const { handleError } = require('../utils/commonUtils')
const { validateNotNullPageNumber, validateNumberPageNumber } = require('../utils/pagingUtils')
const { multerUploadCSVStockProduct } = require("../utils/csvUploadStockProduct");
const { multerUploadImageStockProduct } = require('../utils/imageUploadStockProduct')
router.get('/:id', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const id = req.params.id;
            const data = await stockProductController.getAllStockProduct({ stockProductId: id });
            res.status(200).send(data);
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
})
router.get('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const pageNumber = req.query.pageNumber;
            const { stockProductId, sku, barcode, type, name, stockGroupId, standard, statusId, manufacturedDate, warehouseId, creator, updater, isBatchAdded, stockProductImportBatchId, searchByDate, fromDate, toDate, fromPrice, toPrice, fromLength, toLength   } = req.query
            await validateNotNullPageNumber({ pageNumber });
            await validateNumberPageNumber({ pageNumber });
            let itemsPerPage = parseInt(req.query.itemsPerPage) || DEFAULT_ITEM_PER_PAGE
            const result = await stockProductController.getStockProductBy({
                offset: (pageNumber - 1) * itemsPerPage,
                limit: itemsPerPage,
                stockProductId, sku, barcode, type, name, stockGroupId, standard, statusId, manufacturedDate, warehouseId, creator, updater, isBatchAdded, stockProductImportBatchId, searchByDate, fromDate, toDate, fromPrice, toPrice, fromLength, toLength 
            });
            res.status(200)
            res.send({
                data: result,
                pageNumber,
                itemsPerPage,
            })
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
})

router.post('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const input = req.body
            input.user = req.user.userName;
            const insertedStockProduct = await stockProductController.insertWarehouse(input);
            res.status(201).send({
                insertedStockProduct
            })
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
})

router.put('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const input = req.body
            input.user = req.user.userName;
            const updatedStockProduct = await stockProductController.updateStockProduct(input);
            res.status(200).send({
                updatedStockProduct
            })
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
});
router.delete('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const deletedStockProduct = await stockProductController.deleteStockProduct(req.body);
            if (deletedStockProduct > 0) {
                res.status(200).send({
                    deletedStockProduct
                })
            } else {
                throw new Error('Data not deleted')
            }
        }
        else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
});

router.post('/export', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const pathCSV = './downloads/stock_' + Date.now() + "_exportedCSV.csv";
            await stockProductController.exportStockByListId({
                idList: req.body.idList,
                pathCSV,
                exporter: req.user.userName
            });
            res.status(200).download(pathCSV)
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
})
router.post('/template', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const pathCSV = './downloads/stock_' + Date.now() + "_templateCSV.csv";
            await stockProductController.getTemplate({
                pathCSV
            });
            res.status(200).download(pathCSV)
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e);
    }
});

router.post(
    '/import',
    authenticationUtils.isAuthenticated,
    multerUploadCSVStockProduct.single('csvFile'),
    async (req, res) => {
        try {
            if (roles02and03.includes(req.user.roleId)) {
                const file = req.file
                const inserttedData = await stockProductController.importCSV({ ...file, user: req.user.userName })
                // res.send({
                //     data: inserttedData,
                // })
                res.json({
                    message: "Import CSV file successfully."
                })
            } else {
                res.status(401);
                res.send({
                    error: ["Invalid role."]
                })
            }
        } catch (e) {
            handleError(res, e)
        }
    });

router.post('/uploadImage',
    multerUploadImageStockProduct.single('image'),
    authenticationUtils.isAuthenticated,
    async (req, res) => {
        try {
            if (roles02and03.includes(req.user.roleId)) {
                const file = req.file
                const option = req.body.option
                const data = await stockProductController.uploadImage({ ...file, user: req.user.userName, option })
                res.send(data)
            } else {
                res.status(401);
                res.send({
                    error: ["Invalid role."]
                })
            }
        } catch (e) {
            handleError(res, e)
        }
    })
module.exports = router