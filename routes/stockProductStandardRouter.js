const express = require('express');
const router = express.Router();
const authenticationUtils = require('../utils/authenticateUtils');
const { handleError } = require('../utils/commonUtils')
const stockProductStandardController = require('../controllers/stockProductStandardController')

router.get('/', authenticationUtils.isAuthenticated, async (req , res) => {
    try{
        const data = await stockProductStandardController.getAllStockProductStandard();
        res.status(200).send ({
            data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router;