const express = require('express');
const router = express.Router();
const authenticationUtils = require('../utils/authenticateUtils');
const { handleError } = require('../utils/commonUtils')
const stockProductController = require('../controllers/stockProductController')
router.get('/', authenticationUtils.isAuthenticated, async(req , res) => {
    try{
        const data = await stockProductController.getAllStockProduct();
        res.status(200).send ({
            data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router;