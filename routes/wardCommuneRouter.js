const express = require('express');
const router = express.Router();
const wardCommuneController = require('../controllers/wardcommuneController');
const authenticationUtils = require('../utils/authenticateUtils');
const {handleError} = require('../utils/commonUtils')

router.get('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try{
        const allWardCommune  = await wardCommuneController.getALlCommune();
        res.status(200).send({
            data: allWardCommune.data
        })
    }catch(e){
        handleError(res, e)
    }
})

router.get('/:districtId', authenticationUtils.isAuthenticated, async (req, res) => {
    try{
        const allWardCommune  = await wardCommuneController.getCommuneBy({
            districtId: req.params.districtId
        });
        res.status(200).send({
            data: allWardCommune.data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router;