const express = require('express');
const router = express.Router();
const warehouseManagerController = require('../controllers/warehouseManagerController');
const authenticationUtils = require('../utils/authenticateUtils');
const {handleError} = require('../utils/commonUtils')

router.get('/', authenticationUtils.isAuthenticated, async(req, res)=> {
    try{
        const departments = await warehouseManagerController.getAllAccount();
        res.status(200).send({
            data: departments.data
        })
    }catch(e){
        handleError(res, e)
    }
})

module.exports = router