const express = require('express');
const router = express.Router();
const viewWarehouseController = require('../controllers/viewWarehouseController')
const { DEFAULT_ITEM_PER_PAGE, ROLE_ID } = require('../constants/constants');
const authenticationUtils = require('../utils/authenticateUtils');
const roles02and03 = [ROLE_ID.R002, ROLE_ID.R003]
const { handleError } = require('../utils/commonUtils')
const { validateNotNullPageNumber, validateNumberPageNumber } = require('../utils/pagingUtils')

router.get('/:id', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        const id = req.params.id
        const data = await viewWarehouseController.find(id);
        if(data.data.count < 1){
            const invalidFieldError = Error('Warehouse not existed')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = ['Warehouse not existed']
            throw invalidFieldError
        }
        res.status(200).send(data);
    } catch (e) {
        handleError(res, e, "Error at view warehouse list")
    }
})
router.get('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (roles02and03.includes(req.user.roleId)) {
            const pageNumber = req.query.pageNumber
            await validateNotNullPageNumber({ pageNumber });
            await validateNumberPageNumber({ pageNumber });
            let itemsPerPage = parseInt(req.query.itemsPerPage) || DEFAULT_ITEM_PER_PAGE
            const result = await viewWarehouseController.getWarehouseBy({
                offset: (pageNumber - 1) * itemsPerPage,
                limit: itemsPerPage
            });
            res.status(200)
            res.send({
                data: result,
                pageNumber,
                itemsPerPage,
            })
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e, "Error at view warehouse list")
    }
})

router.post('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (req.user.roleId === ROLE_ID.R002) {
            const user = req.user.userName;
            const input = req.body;
            input.user = user
            const newWarehouse = await viewWarehouseController.insertWarehouse(input);
            if (newWarehouse) {
                res.status(201).send({
                    newWarehouse
                })
            } else {
                throw new Error("Cannot insert into database")
            }
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e, `Error at creating warehosue: ${e.message}`)
    }
})
router.put('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (req.user.roleId === ROLE_ID.R002) {
            const updater = req.user.userName;
            const input = req.body;
            input.updater = updater
            const updatedWarehouse = await viewWarehouseController.updateWarehouse(
                input
            )
            res.status(200).send({
                updatedWarehouse
            })
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e, `Error at modifying warehosue: ${e.message}`)
    }
})
router.delete('/', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        if (req.user.roleId === ROLE_ID.R002) {
            const warehouseId = req.body.warehouseId
            const deletedWarehouse = await viewWarehouseController.deleteWarehouse({
                warehouseId
            })
            if (deletedWarehouse > 0) {
                res.status(200).send({
                    deletedWarehouse
                })
            } else {
                const invalidFieldError = Error('Data not deleted')
                invalidFieldError.statusCode = 400
                throw invalidFieldError
            }
        } else {
            res.status(401);
            res.send({
                message: "Invalid role."
            })
        }
    } catch (e) {
        handleError(res, e, `Error at deleting warehosue: ${e.message}`)
    }
})
router.post('/all', authenticationUtils.isAuthenticated, async (req, res) => {
    try {
        const data = await viewWarehouseController.getAll();
        res.status(200).send(data);
    } catch (e) {
        handleError(res, e, "Error at view warehouse list")
    }
})
module.exports = router;