const app = require("../../app");
const session = require("supertest-session");
var testSession = null;

const successResponse = require("./account_successResponse"); // Response Body trả về từ server
const failedResponse = require("./account_failedResponse");
const input = require("./account_input");
const authLoginObject = require("../shared/authenticate_loginObject");

beforeEach(function () {
    testSession = session(app);
});


// // -----------------------------------------
// //
// //    Sheet: View account list
// //
// // -----------------------------------------
// describe('-------Sheet: View Account List', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.get('/account')
//             .send()
//             .expect(401, failedResponse.viewAccountList_utc002, done)
//     });
// });
//
// describe('-------Sheet: View Account List', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.get('/account')
//             .send()
//             .expect(401, failedResponse.viewAccountList_utc003, done)
//     });
// });
//
// describe('-------Sheet: View Account List', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.get('/account')
//             .send()
//             .expect(401, failedResponse.viewAccountList_utc004, done)
//     });
// });
//
// describe('-------Sheet: View Account List', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.get('/account')
//             .send()
//             .expect(200, successResponse.viewAccountList_utc007, done)
//     });
// });

// // -----------------------------------------
// //
// //    Sheet: Search Account
// //
// // -----------------------------------------
// describe('-------Sheet: Search Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.post('/account/search')
//             .send(input.searchAccount_utc002)
//             .expect(401, failedResponse.searchAccount_utc002, done)
//     });
// });
//
// describe('-------Sheet: Search Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.post('/account/search')
//             .send(input.searchAccount_utc003)
//             .expect(400, failedResponse.searchAccount_utc003, done)
//     });
// });
//
// describe('-------Sheet: Search Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.post('/account/search')
//             .send(input.searchAccount_utc004)
//             .expect(200, successResponse.searchAccount_utc004, done)
//     });
// });
//
// describe('-------Sheet: Search Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.post('/account/search')
//             .send(input.searchAccount_utc005)
//             .expect(200, successResponse.searchAccount_utc005, done)
//     });
// });
//
// describe('-------Sheet: Search Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.post('/account/search')
//             .send(input.searchAccount_utc008)
//             .expect(200, successResponse.searchAccount_utc008, done)
//     });
// });

// // -----------------------------------------
// //
// //    Sheet: Add New Account
// //
// // -----------------------------------------
// describe('-------Sheet: Add new Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.post('/account')
//             .send(input.addNewAccount_utc002)
//             .expect(401, failedResponse.addNewAccount_utc002, done)
//     });
// });
//
// describe('-------Sheet: Add new Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.post('/account')
//             .send(input.addNewAccount_utc004)
//             .expect(400, failedResponse.addNewAccount_utc004, done)
//     });
// });
//
// describe('-------Sheet: Add new Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.post('/account')
//             .send(input.addNewAccount_utc006)
//             .expect(400, failedResponse.addNewAccount_utc006, done)
//     });
// });
//
// describe('-------Sheet: Add new Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.post('/account')
//             .send(input.addNewAccount_utc007)
//             .expect(400, failedResponse.addNewAccount_utc007, done)
//     });
// });
//
// describe('-------Sheet: Add new Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.post('/account')
//             .send(input.addNewAccount_utc008)
//             .expect(400, failedResponse.addNewAccount_utc008, done)
//     });
// });
//
// describe('-------Sheet: Add new Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC009', function (done) {
//         console.log(input.addNewAccount_utc009)
//         authenticatedSession.post('/account')
//             .send(input.addNewAccount_utc009)
//             .expect(200, successResponse.addNewAccount_utc009, done)
//     });
// });

// -----------------------------------------
//
//    Sheet: Modify Existing Account
//
// -----------------------------------------
// describe('-------Sheet: Modify Existing Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.put('/account')
//             .send(input.modifyExistingAccount_utc002)
//             .expect(401, failedResponse.modifyExistingAccount_utc002, done)
//     });
// });
//
// describe('-------Sheet: Modify Existing Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.put('/account')
//             .send(input.modifyExistingAccount_utc003)
//             .expect(400, failedResponse.modifyExistingAccount_utc003, done)
//     });
// });
//
// describe('-------Sheet: Modify Existing Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.put('/account')
//             .send(input.modifyExistingAccount_utc006)
//             .expect(400, failedResponse.modifyExistingAccount_utc006, done)
//     });
// });
//
// describe('-------Sheet: Modify Existing Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.put('/account')
//             .send(input.modifyExistingAccount_utc007)
//             .expect(400, failedResponse.modifyExistingAccount_utc007, done)
//     });
// });
//
// describe('-------Sheet: Modify Existing Account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.put('/account')
//             .send(input.modifyExistingAccount_utc008)
//             .expect(200, successResponse.modifyExistingAccount_utc008, done)
//     });
// });
//
// // -----------------------------------------
// //
// //    Sheet: Delete disabled account
// //
// // -----------------------------------------
// describe('-------Sheet: Delete disabled account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.delete('/account')
//             .send(input.deleteDisabledAccount_utc002)
//             .expect(401, failedResponse.deleteDisabledAccount_utc002, done)
//     });
// });
//
// describe('-------Sheet: Delete disabled account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.delete('/account')
//             .send(input.deleteDisabledAccount_utc004)
//             .expect(400, failedResponse.deleteDisabledAccount_utc004, done)
//     });
// });
//
// describe('-------Sheet: Delete disabled account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.delete('/account')
//             .send(input.deleteDisabledAccount_utc005)
//             .expect(200, failedResponse.deleteDisabledAccount_utc005, done)
//     });
// });
//
// describe('-------Sheet: Delete disabled account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.delete('/account')
//             .send(input.deleteDisabledAccount_utc006)
//             .expect(200, failedResponse.deleteDisabledAccount_utc006, done)
//     });
// });
//
// describe('-------Sheet: Delete disabled account', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.delete('/account')
//             .send(input.deleteDisabledAccount_utc008)
//             .expect(200, successResponse.deleteDisabledAccount_utc008, done)
//     });
// });


// // -----------------------------------------
// //
// //    Sheet: View account information
// //
// // -----------------------------------------
// describe('-------Sheet: View account information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.get('/account/' + input.viewAccountInformation_allUTC)
//             .send()
//             .expect(200, successResponse.viewAccountInfo_utc004, done)
//     });
// });
//
// describe('-------Sheet: View account information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.get('/account/' + input.viewAccountInformation_allUTC)
//             .send()
//             .expect(200, successResponse.viewAccountInfo_utc005, done)
//     });
// });
//
// describe('-------Sheet: View account information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.get('/account/' + input.viewAccountInformation_allUTC)
//             .send()
//             .expect(200, successResponse.viewAccountInfo_utc006, done)
//     });
// });
//
// describe('-------Sheet: View account information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.get('/account/' + input.viewAccountInformation_allUTC)
//             .send()
//             .expect(200, successResponse.viewAccountInfo_utc007, done)
//     });
// });
