const commonException = require("../shared/common_exception");
// -----------------------------------------
//
//    Sheet: View Account List
//
// -----------------------------------------
module.exports.viewAccountList_utc002 = commonException.unauthorizedAccess

module.exports.viewAccountList_utc003 = commonException.unauthorizedAccess

module.exports.viewAccountList_utc004 = commonException.unauthorizedAccess

// -----------------------------------------
//
//    Sheet: Search Account
//
// -----------------------------------------
module.exports.searchAccount_utc002 = commonException.unauthorizedAccess

module.exports.searchAccount_utc003 = commonException.reqBodyEmpty

// -----------------------------------------
//
//    Sheet: Add New Account
//
// -----------------------------------------
module.exports.addNewAccount_utc002 = commonException.unauthorizedAccess;

module.exports.addNewAccount_utc004 = commonException.reqBodyEmpty;

module.exports.addNewAccount_utc006 = {
    "error": [
        "Invalid Username. Please provide Username again.\n"
    ]
}

module.exports.addNewAccount_utc007 = {
    error: [
        'Password must meet minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character (@, $, !, %, *, ?, &).\n',
        'Invalid Role. Please provide Role again.',
        'Invalid Department. Please provide Department again.',
        'Invalid Disable Flag. Please provide Disable Flag again.',
        'Invalid First Name. Please provide First Name again.\n',
        'Invalid Last Name. Please provide Last Name again.\n',
        'Invalid Gender. Please provide Gender again.\n',
        'Invalid Date of Birth. Please provide Date of Birth again.\n',
        'Invalid Email. Please provide Email again.\n',
        'Invalid Phone Number. Please provide Phone Number again.\n',
        'City/Province is not existed! Please provide City/Province again.\n',
        'District is not existed! Please provide District  again.\n',
        'Ward/Commune is not existed! Please provide Ward/Commune again.\n',
        'Invalid Living Address. Please provide Living Address again.\n'
    ]
}


module.exports.addNewAccount_utc008 = {
    "error": [
        "Account with userName: test2 existed.\n"
    ]
}

// -----------------------------------------
//
//    Sheet: Modify Existing Account
//
// -----------------------------------------
module.exports.modifyExistingAccount_utc002 = commonException.unauthorizedAccess

module.exports.modifyExistingAccount_utc003 = commonException.reqBodyEmpty

module.exports.modifyExistingAccount_utc006 = {
    error: [
        'Invalid Role. Please provide Role again.',
        'Invalid Department. Please provide Department again.',
        'Invalid Disable Flag. Please provide Disable Flag again.',
        'Invalid First Name. Please provide First Name again.\n',
        'Invalid Last Name. Please provide Last Name again.\n',
        'Invalid Gender. Please provide Gender again.\n',
        'Invalid Date of Birth. Please provide Date of Birth again.\n',
        'Invalid Email. Please provide Email again.\n',
        'Invalid Phone Number. Please provide Phone Number again.\n',
        'City/Province is not existed! Please provide City/Province again.\n',
        'District is not existed! Please provide District  again.\n',
        'Ward/Commune is not existed! Please provide Ward/Commune again.\n',
        'Invalid Living Address. Please provide Living Address again.\n'
    ]
}

module.exports.modifyExistingAccount_utc007 = {
    "error": [
        "Account with userName: zxc NOT existed.\n"
    ]
}


// -----------------------------------------
//
//    Sheet: Delete disabled account
//
// -----------------------------------------
module.exports.deleteDisabledAccount_utc002 = commonException.unauthorizedAccess;

module.exports.deleteDisabledAccount_utc004 = commonException.reqBodyEmpty;

module.exports.deleteDisabledAccount_utc005 = {
    message: "No account is deleted."
}

module.exports.deleteDisabledAccount_utc006 = {
    message: "No account is deleted."
}

// -----------------------------------------
//
//    Sheet: View Account Information
//
// -----------------------------------------
// No need

