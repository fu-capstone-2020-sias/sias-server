// -----------------------------------------
//
//    Sheet: View Account List
//
// -----------------------------------------
// No need

// -----------------------------------------
//
//    Sheet: Search Account
//
// -----------------------------------------
const userNameValid = "test2";
const userNameInvalid = "invalidUserName#!";

const phoneNumberValid = '0347669777';
const phoneNumberInvalid8Numbers = '0311cx3#2'
const phoneNumberInvalid11Numbers = '0348881234l'

const emailInvalid = 'invalid@z2&^.com';
const emailValid = 'hoangchhe130830@fpt.edu.vn';

const fullNameValid = 'Hoàng Cao';
const fullNameInvalid = 'VN 123';

module.exports.searchAccount_utc002 = {
    phoneNumber: phoneNumberValid,
    email: emailValid
}

module.exports.searchAccount_utc003 = {
    // Empty
}

module.exports.searchAccount_utc004 = {
    userName: userNameInvalid,
    phoneNumber: phoneNumberInvalid8Numbers,
    email: emailInvalid,
    fullName: fullNameInvalid
}

module.exports.searchAccount_utc005 = {
    userName: userNameInvalid,
    phoneNumber: phoneNumberInvalid11Numbers,
    email: emailInvalid,
    fullName: fullNameInvalid
}

module.exports.searchAccount_utc008 = {
    userName: userNameValid,
    phoneNumber: phoneNumberValid,
    email: emailValid,
    fullName: fullNameValid
}

// -----------------------------------------
//
//    Sheet: Add New Account
//
// -----------------------------------------
const passwordValid = 'hghgE123@456hg';
const passwordInvalid = 'invalidPassword';

const roleIdValid = 'R002';
const roleIdInvalid = 'R005';

const departmentIdValid = 'DEPT001';
const departmentIdInvalid = 'xDEPT000';

const firstNameValid = 'Hoàng';
const firstNameInvalid = '123';

const lastNameValid = 'Cao';
const lastNameInvalid = '345';

const genderValid = '0';
const genderInvalid = '2';

const dobValid = '1982-10-10';
const dobInvalid = '2020/99/99';

const cityProvinceIdInvalid = '64';
const cityProvinceIdValid = '1';

const districtIdInvalid = '100000';
const districtIdValid = '1';

const wardCommuneIdInvalid = '100000';
const wardCommuneIdValid = '1';

const addressValid = '7 Khâm Thiên'

const companyInfoIdValid = 'COM001';
const companyInfoIdInvalid = 'COM000x';

const disableFlagValid = '0';
const disableFlagInvalid = '2';

const isChangePasswordValid = '1';
const screen_id = 'WEB_UC08';

module.exports.addNewAccount_utc002 = {
    userName: userNameValid,
    password: passwordValid,
    roleId: roleIdValid,
    departmentId: departmentIdValid,
    firstName: firstNameValid,
    lastName: lastNameValid,
    gender: genderValid,
    dob: dobValid,
    email: emailValid,
    phoneNumber: phoneNumberValid,
    cityProvinceId: cityProvinceIdValid,
    districtId: districtIdValid,
    wardCommuneId: wardCommuneIdValid,
    address: addressValid,
    companyInfoId: companyInfoIdValid,
    disableFlag: disableFlagValid,
    isChangePassword: isChangePasswordValid,
    screen_id: screen_id
}

module.exports.addNewAccount_utc004 = {
    // Empty
}

module.exports.addNewAccount_utc006 = {
    userName: userNameInvalid,
    password: passwordInvalid,
    roleId: roleIdInvalid,
    departmentId: departmentIdInvalid,
    firstName: firstNameInvalid,
    lastName: lastNameInvalid,
    gender: genderInvalid,
    dob: dobInvalid,
    email: emailInvalid,
    phoneNumber: phoneNumberInvalid8Numbers,
    cityProvinceId: cityProvinceIdInvalid,
    districtId: districtIdInvalid,
    wardCommuneId: wardCommuneIdInvalid,
    companyInfoId: companyInfoIdInvalid,
    disableFlag: disableFlagInvalid,
    isChangePassword: isChangePasswordValid,
    screen_id: screen_id
}

module.exports.addNewAccount_utc007 = {
    userName: 'oklah',
    password: passwordInvalid,
    roleId: roleIdInvalid,
    departmentId: departmentIdInvalid,
    firstName: firstNameInvalid,
    lastName: lastNameInvalid,
    gender: genderInvalid,
    dob: dobInvalid,
    email: emailInvalid,
    phoneNumber: phoneNumberInvalid8Numbers,
    cityProvinceId: cityProvinceIdInvalid,
    districtId: districtIdInvalid,
    wardCommuneId: wardCommuneIdInvalid,
    companyInfoId: companyInfoIdInvalid,
    disableFlag: disableFlagInvalid,
    isChangePassword: isChangePasswordValid,
    screen_id: screen_id
}

module.exports.addNewAccount_utc008 = {
    userName: userNameValid,
    password: passwordValid,
    roleId: roleIdValid,
    departmentId: departmentIdValid,
    firstName: firstNameValid,
    lastName: lastNameValid,
    gender: genderValid,
    dob: dobValid,
    email: emailValid,
    phoneNumber: phoneNumberValid,
    cityProvinceId: cityProvinceIdValid,
    districtId: districtIdValid,
    wardCommuneId: wardCommuneIdValid,
    address: addressValid,
    companyInfoId: companyInfoIdValid,
    disableFlag: disableFlagValid,
    isChangePassword: isChangePasswordValid,
    screen_id: screen_id
}

module.exports.addNewAccount_utc009 = {
    userName: 'oklah',
    password: passwordValid,
    roleId: roleIdValid,
    departmentId: departmentIdValid,
    firstName: firstNameValid,
    lastName: lastNameValid,
    gender: genderValid,
    dob: dobValid,
    email: 'bobo@bobohehe.com',
    phoneNumber: '0341115555',
    cityProvinceId: cityProvinceIdValid,
    districtId: districtIdValid,
    wardCommuneId: wardCommuneIdValid,
    address: addressValid,
    companyInfoId: companyInfoIdValid,
    disableFlag: disableFlagValid,
    isChangePassword: isChangePasswordValid,
    screen_id: screen_id
}

// -----------------------------------------
//
//    Sheet: Modify Existing Account
//
// -----------------------------------------
module.exports.modifyExistingAccount_utc002 = {
    userName: 'oklah',
    password: passwordValid,
    roleId: roleIdInvalid,
    departmentId: departmentIdInvalid,
    firstName: firstNameInvalid,
    lastName: lastNameInvalid,
    gender: genderInvalid,
    dob: dobInvalid,
    email: emailInvalid,
    phoneNumber: phoneNumberInvalid11Numbers,
    cityProvinceId: cityProvinceIdInvalid,
    districtId: districtIdInvalid,
    wardCommuneId: wardCommuneIdInvalid,
    address: '',
    companyInfoId: companyInfoIdInvalid,
    disableFlag: disableFlagInvalid,
    isChangePassword: '0',
    screen_id: screen_id
}

module.exports.modifyExistingAccount_utc003 = {
    // Req.body empty
}

module.exports.modifyExistingAccount_utc006 = {
    userName: 'oklah',
    password: passwordValid,
    roleId: roleIdInvalid,
    departmentId: departmentIdInvalid,
    firstName: firstNameInvalid,
    lastName: lastNameInvalid,
    gender: genderInvalid,
    dob: dobInvalid,
    email: emailInvalid,
    phoneNumber: phoneNumberInvalid11Numbers,
    cityProvinceId: cityProvinceIdInvalid,
    districtId: districtIdInvalid,
    wardCommuneId: wardCommuneIdInvalid,
    address: '',
    companyInfoId: companyInfoIdInvalid,
    disableFlag: disableFlagInvalid,
    isChangePassword: '0',
    screen_id: screen_id
}

module.exports.modifyExistingAccount_utc007 = {
    userName: 'zxc',
    password: passwordValid,
    roleId: roleIdValid,
    departmentId: departmentIdValid,
    firstName: firstNameValid,
    lastName: lastNameValid,
    gender: genderValid,
    dob: dobValid,
    email: 'bobo@bobohehe.com',
    phoneNumber: '0341115555',
    cityProvinceId: cityProvinceIdValid,
    districtId: districtIdValid,
    wardCommuneId: wardCommuneIdValid,
    address: addressValid,
    companyInfoId: companyInfoIdValid,
    disableFlag: disableFlagValid,
    isChangePassword: '0',
    screen_id: screen_id
}

module.exports.modifyExistingAccount_utc008 = {
    userName: 'oklah',
    password: passwordValid,
    roleId: roleIdValid,
    departmentId: departmentIdValid,
    firstName: firstNameValid,
    lastName: lastNameValid,
    gender: genderValid,
    dob: dobValid,
    email: 'bobo@bobohehe.com',
    phoneNumber: '0341115555',
    cityProvinceId: cityProvinceIdValid,
    districtId: districtIdValid,
    wardCommuneId: wardCommuneIdValid,
    address: addressValid,
    companyInfoId: companyInfoIdValid,
    disableFlag: disableFlagValid,
    isChangePassword: '0',
    screen_id: screen_id
}


// -----------------------------------------
//
//    Sheet: Delete disabled account
//
// -----------------------------------------
const arrOfUsernamesInvalid = 'dzxcj,dakcvxzk';

module.exports.deleteDisabledAccount_utc002 = {
    // Empty
}

module.exports.deleteDisabledAccount_utc004 = {
    // Empty
}

module.exports.deleteDisabledAccount_utc005 = {
    arrOfUsernames: 'delete01,delete02'
}

module.exports.deleteDisabledAccount_utc006 = {
    arrOfUsernames: arrOfUsernamesInvalid
}

module.exports.deleteDisabledAccount_utc008 = {
    arrOfUsernames: 'delete05,delete06'
}

// -----------------------------------------
//
//    Sheet: View Account Information
//
// -----------------------------------------
module.exports.viewAccountInformation_allUTC = 'test1';
