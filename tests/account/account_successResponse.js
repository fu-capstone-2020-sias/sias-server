// -----------------------------------------
//
//    Sheet: View Account List
//
// -----------------------------------------
module.exports.viewAccountList_utc007 = {
    "accounts": [
        {
            "row_num": 1,
            "userName": "abampkinl",
            "roleId": "R004",
            "departmentId": "DEPT002",
            "firstName": "Alvin",
            "lastName": "Chipmunk",
            "gender": 1,
            "dob": "1995-03-21",
            "email": "abampkinl@chipmunk.com",
            "phoneNumber": "0776660323",
            "cityProvinceId": 2,
            "districtId": 39,
            "wardCommuneId": 598,
            "address": "312 Đội Cấn",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-27",
            "disableFlag": 1,
            "role": "Office Employee",
            "department": "Phòng Sản xuất",
            "cityProvince": "Hà Nội",
            "district": "Long Biên",
            "wardCommune": "Bồ Đề"
        },
        {
            "row_num": 2,
            "userName": "acrocket2m",
            "roleId": "R004",
            "departmentId": "DEPT004",
            "firstName": "Angie",
            "lastName": "Crocket",
            "gender": 0,
            "dob": "1987-08-15",
            "email": "hoangchhe130830@fpt.edu.vn",
            "phoneNumber": "0993745880",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "92 Vera Terrace",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-11",
            "disableFlag": 1,
            "role": "Office Employee",
            "department": "Ban cấp cao",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 3,
            "userName": "afritchley16",
            "roleId": "R004",
            "departmentId": "DEPT003",
            "firstName": "Ameline",
            "lastName": "Fritchley",
            "gender": 1,
            "dob": "1979-08-21",
            "email": "afritchley16@company.com",
            "phoneNumber": "0745207652",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "8 Eastlawn Street",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 4,
            "userName": "apenticostg",
            "roleId": "R003",
            "departmentId": "DEPT003",
            "firstName": "Adrianna",
            "lastName": "Penticost",
            "gender": 0,
            "dob": "1996-04-11",
            "email": "apenticostg@company.com",
            "phoneNumber": "0526508032",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "30654 Northfield Parkway",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Factory Manager",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 5,
            "userName": "bchasier2g",
            "roleId": "R004",
            "departmentId": "DEPT001",
            "firstName": "Billi",
            "lastName": "Chasier",
            "gender": 1,
            "dob": "1981-05-09",
            "email": "bchasier2g@company.com",
            "phoneNumber": "0544067970",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "24622 Fremont Trail",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-10",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Phòng Quản lý Nhập xuất",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 6,
            "userName": "bdautry1h",
            "roleId": "R002",
            "departmentId": "DEPT003",
            "firstName": "Benedict",
            "lastName": "Dautry",
            "gender": 0,
            "dob": "1988-10-26",
            "email": "bdautry1h@company.com",
            "phoneNumber": "0976034782",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "9 Eastlawn Lane",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Manager",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 7,
            "userName": "bkilpini",
            "roleId": "R002",
            "departmentId": "DEPT003",
            "firstName": "Barry",
            "lastName": "Kilpin",
            "gender": 1,
            "dob": "1980-01-24",
            "email": "bkilpini@company.com",
            "phoneNumber": "0554112408",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "81619 Gerald Junction",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 1,
            "role": "Office Manager",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 8,
            "userName": "bmancktelow1p",
            "roleId": "R002",
            "departmentId": "DEPT004",
            "firstName": "Bailie",
            "lastName": "Mancktelow",
            "gender": 0,
            "dob": "1997-05-23",
            "email": "bmancktelow1p@company.com",
            "phoneNumber": "0508724924",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "7 Anzinger Circle",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 1,
            "role": "Office Manager",
            "department": "Ban cấp cao",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 9,
            "userName": "cgobbet2e",
            "roleId": "R004",
            "departmentId": "DEPT004",
            "firstName": "Carlina",
            "lastName": "Gobbet",
            "gender": 0,
            "dob": "1984-10-20",
            "email": "cgobbet2e@company.com",
            "phoneNumber": "0599455979",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "66055 Lotheville Street",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Ban cấp cao",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 10,
            "userName": "cgrzelewski1z",
            "roleId": "R004",
            "departmentId": "DEPT002",
            "firstName": "Chen",
            "lastName": "Grzelewski",
            "gender": 1,
            "dob": "1975-05-29",
            "email": "cgrzelewski1z@company.com",
            "phoneNumber": "0835167861",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "147 Sommers Place",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Phòng Sản xuất",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        }
    ],
    "numberOfPages": 10,
    "totalEntries": 91
}


// -----------------------------------------
//
//    Sheet: Search Account
//
// -----------------------------------------
module.exports.searchAccount_utc004 = {
    "accounts": [],
    "numberOfPages": 0,
    "totalEntries": 0
}

module.exports.searchAccount_utc005 = {
    "accounts": [],
    "numberOfPages": 0,
    "totalEntries": 0
}

module.exports.searchAccount_utc008 = {
    "accounts": [
        {
            "row_num": 1,
            "userName": "test2",
            "roleId": "R001",
            "departmentId": "DEPT001",
            "firstName": "Hoàng",
            "lastName": "Cao",
            "gender": 0,
            "dob": "1999-01-11",
            "email": "hoangchhe130830@fpt.edu.vn",
            "phoneNumber": "0347669777",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "Hoan Kiem Lake",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-10",
            "updatedDate": "2020-11-16",
            "disableFlag": 0,
            "role": "Administrator",
            "department": "Phòng Quản lý Nhập xuất",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        }
    ],
    "numberOfPages": 1,
    "totalEntries": 1
}

// -----------------------------------------
//
//    Sheet: Add New Account
//
// -----------------------------------------
module.exports.addNewAccount_utc009 = {
    message: "Account with userName: " + 'oklah'
        + " is added into the database."
}

// -----------------------------------------
//
//    Sheet: Modify Existing Account
//
// -----------------------------------------
module.exports.modifyExistingAccount_utc008 = {
    message: "Account with userName: " + 'oklah' + " updated."
}

// -----------------------------------------
//
//    Sheet: Delete disabled account
//
// -----------------------------------------
module.exports.deleteDisabledAccount_utc008 = {
    message: 'Account with usernames: [delete05, delete06], are deleted from SIAS system.'
}

// -----------------------------------------
//
//    Sheet: View Account Information
//
// -----------------------------------------
const sharedResponse = {
    "account": {
        "userName": "test1",
        "roleId": "R002",
        "departmentId": "DEPT003",
        "firstName": "Người",
        "lastName": "Chơi",
        "gender": 1,
        "dob": "1999-11-11",
        "email": "hoangchhe130830@fpt.edu.vn",
        "phoneNumber": "0347619779",
        "cityProvinceId": 1,
        "cityProvince": "Hồ Chí Minh",
        "districtId": 1,
        "district": "Bình Chánh",
        "wardCommuneId": 1,
        "wardCommune": "An Phú Tây",
        "address": "Hoan Kiem Lake",
        "companyInfoId": "COM001",
        "createdDate": "2020-10-10",
        "updatedDate": "2020-11-15",
        "disableFlag": 0,
        "role": "Office Manager",
        "department": "Ban điều hành"
    }
};

module.exports.viewAccountInfo_utc004 = sharedResponse;

module.exports.viewAccountInfo_utc005 = sharedResponse;

module.exports.viewAccountInfo_utc006 = sharedResponse;

module.exports.viewAccountInfo_utc007 = sharedResponse;
