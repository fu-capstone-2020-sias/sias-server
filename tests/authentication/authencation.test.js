const app = require("../../app");
const session = require("supertest-session");
var testSession = null;

const successResponse = require("./authentication_successResponse"); // Response Body trả về từ server
const failedResponse = require("./authentication_failedResponse");
const input = require("./authentication_input");
const authLoginObject = require("../shared/authenticate_loginObject");

beforeEach(function () {
    testSession = session(app);
});

// -----------------------------------------
//
//    Sheet: Login
//
// -----------------------------------------
describe('-------Sheet: Login', function () {

    it('UTC002', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc002)
            .expect(400, failedResponse.login_utc002, done)
    });

    it('UTC003', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc003)
            .expect(400, failedResponse.login_utc003, done)
    });

    it('UTC004', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc004)
            .expect(400, failedResponse.login_utc004, done)
    });

    it('UTC005', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc005)
            .expect(200, successResponse.login_utc005, done)
    });

    it('UTC006', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc006)
            .expect(400, failedResponse.login_utc006, done)
    });

    it('UTC007', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc007)
            .expect(400, failedResponse.login_utc007, done)
    });

    it('UTC008', function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc008)
            .expect(400, failedResponse.login_utc008, done)
    });
});

describe('-------Sheet: Login', function () {

    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(input.login_utc009)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC009', function (done) {
        authenticatedSession.post('/auth/login')
            .send(input.login_utc009)
            .expect(400, failedResponse.login_utc009, done)
    });

    it('UTC011', function (done) {
        authenticatedSession.post('/auth/login')
            .send(input.login_utc011)
            .expect(400, failedResponse.login_utc011, done)
    });
})

// -----------------------------------------
//
//    Sheet: Forgot Password
//
// -----------------------------------------
describe('-------Sheet: Forgot Password', function () {

    it('UTC002', function (done) {
        testSession.post('/auth/forgotPassword/' + input.forgotPwd_utc002.userName)
            .send()
            .expect(400, failedResponse.forgotPwd_utc002, done)
    });

    it('UTC003', function (done) {
        testSession.post('/auth/forgotPassword/' + input.forgotPwd_utc003.userName)
            .send()
            .expect(404, done)
    });

    it('UTC004', function (done) {
        testSession.post('/auth/forgotPassword/' + input.forgotPwd_utc004.userName)
            .send()
            .expect(200, successResponse.forgotPwd_utc004, done)
    });
});

// -----------------------------------------
//
//    Sheet: Logout
//
// -----------------------------------------
describe('-------Sheet: Logout', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_admin)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC004', function (done) {
        authenticatedSession.post('/auth/logout')
            .send()
            .expect(200, successResponse.logout_utc004, done)
    });
});