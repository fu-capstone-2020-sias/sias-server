const userNameEmpty = '';
const passwordEmpty = '';
const validUsername = 'insert05'
const validPassword = 'hghgE123@456hg'
const invalidUserName = 'invalidUserName001';
const invalidPassword = 'invalidPassword';

const commonException = require("../shared/common_exception");
// -----------------------------------------
//
//    Sheet: Login
//
// -----------------------------------------

// UTC002
module.exports.login_utc002 = {
    message: 'Missing credentials'
}

// UTC003
module.exports.login_utc003 = {
    message: 'Missing credentials'
}

// UTC004
module.exports.login_utc004 = {
    error: ["Incorrect password. Please enter again."]
}

// UTC006
module.exports.login_utc006 = {
    message: 'Missing credentials'
}

// UTC007
module.exports.login_utc007 = {
    error: ["Account with userName: " + invalidUserName
    + " NOT existed."]
}

// UTC008
module.exports.login_utc008 = {
    error: ["Account with userName: " + invalidUserName
    + " NOT existed."]
}

// UTC009
module.exports.login_utc009 = {
    error: ["Hey there! You are logged in, please don't authenticate yourself again."]
}

// UTC010 test thủ công bằng tay
// // UTC010
// module.exports.login_utc010 = commonException.sessionExpired;

// UTC011
module.exports.login_utc011 = {
    error: ["Hey there! You must log out your current account" + " (" + validUsername +
    ") " + "in order to login another account."]
}

// -----------------------------------------
//
//    Sheet: Forgot Password
//
// -----------------------------------------
module.exports.forgotPwd_utc002 = {
    error: ["Account with username: " + "invalidUserName001" + " not existed."]
}

module.exports.forgotPwd_utc003 = "404 Not Found"
