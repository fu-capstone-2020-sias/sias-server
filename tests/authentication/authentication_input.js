// Re-use code
const userNameEmpty = '';
const passwordEmpty = '';
const validUsername = 'insert05'
const validPassword = 'hghgE123@456hg'
const invalidUserName = 'invalidUserName001';
const invalidPassword = 'invalidPassword';

// -----------------------------------------
//
//    Sheet: Login
//
// -----------------------------------------

// UTC002
module.exports.login_utc002 = {
    userName: userNameEmpty,
    password: passwordEmpty,
}

// UTC003
module.exports.login_utc003 = {
    userName: validUsername,
    password: passwordEmpty,
}

// UTC004
module.exports.login_utc004 = {
    userName: validUsername,
    password: invalidPassword,
}

// UTC005
module.exports.login_utc005 = {
    userName: validUsername,
    password: validPassword,
}

// UTC006
module.exports.login_utc006 = {
    userName: invalidUserName,
    password: passwordEmpty,
}

// UTC007
module.exports.login_utc007 = {
    userName: invalidUserName,
    password: invalidPassword,
}

// UTC008
module.exports.login_utc008 = {
    userName: invalidUserName,
    password: validPassword,
}

// UTC009
module.exports.login_utc009 = {
    userName: validUsername,
    password: validPassword,
}

// UTC010 test thủ công bằng tay
// // UTC010
// module.exports.login_utc010 = {
//     userName: validUsername,
//     password: validPassword,
// }

// UTC011
module.exports.login_utc011 = {
    userName: invalidUserName,
    password: invalidPassword,
}

// -----------------------------------------
//
//    Sheet: Forgot Password
//
// -----------------------------------------
module.exports.forgotPwd_utc002 = {
    userName: "invalidUserName001"
}

module.exports.forgotPwd_utc003 = {
    userName: userNameEmpty
}

module.exports.forgotPwd_utc004 = {
    userName: "insert02"
}

// -----------------------------------------
//
//    Sheet:
//
// -----------------------------------------
