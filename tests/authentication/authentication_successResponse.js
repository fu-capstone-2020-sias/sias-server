module.exports.demo = {
    "accounts": [
        {
            "row_num": 1,
            "userName": "abampkinl",
            "roleId": "R004",
            "departmentId": "DEPT002",
            "firstName": "Alvin",
            "lastName": "Chipmunk",
            "gender": 1,
            "dob": "1995-03-21",
            "email": "abampkinl@chipmunk.com",
            "phoneNumber": "0776660323",
            "cityProvinceId": 2,
            "districtId": 39,
            "wardCommuneId": 598,
            "address": "312 Đội Cấn",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-27",
            "disableFlag": 1,
            "role": "Office Employee",
            "department": "Phòng Sản xuất",
            "cityProvince": "Hà Nội",
            "district": "Long Biên",
            "wardCommune": "Bồ Đề"
        },
        {
            "row_num": 2,
            "userName": "acrocket2m",
            "roleId": "R004",
            "departmentId": "DEPT004",
            "firstName": "Angie",
            "lastName": "Crocket",
            "gender": 0,
            "dob": "1987-08-15",
            "email": "hoangchhe130830@fpt.edu.vn",
            "phoneNumber": "0993745880",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "92 Vera Terrace",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-11",
            "disableFlag": 1,
            "role": "Office Employee",
            "department": "Ban cấp cao",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 3,
            "userName": "afritchley16",
            "roleId": "R004",
            "departmentId": "DEPT003",
            "firstName": "Ameline",
            "lastName": "Fritchley",
            "gender": 1,
            "dob": "1979-08-21",
            "email": "afritchley16@company.com",
            "phoneNumber": "0745207652",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "8 Eastlawn Street",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 4,
            "userName": "apenticostg",
            "roleId": "R003",
            "departmentId": "DEPT003",
            "firstName": "Adrianna",
            "lastName": "Penticost",
            "gender": 0,
            "dob": "1996-04-11",
            "email": "apenticostg@company.com",
            "phoneNumber": "0526508032",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "30654 Northfield Parkway",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Factory Manager",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 5,
            "userName": "bchasier2g",
            "roleId": "R004",
            "departmentId": "DEPT001",
            "firstName": "Billi",
            "lastName": "Chasier",
            "gender": 1,
            "dob": "1981-05-09",
            "email": "bchasier2g@company.com",
            "phoneNumber": "0544067970",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "24622 Fremont Trail",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-10",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Phòng Quản lý Nhập xuất",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 6,
            "userName": "bdautry1h",
            "roleId": "R002",
            "departmentId": "DEPT003",
            "firstName": "Benedict",
            "lastName": "Dautry",
            "gender": 0,
            "dob": "1988-10-26",
            "email": "bdautry1h@company.com",
            "phoneNumber": "0976034782",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "9 Eastlawn Lane",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Manager",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 7,
            "userName": "bkilpini",
            "roleId": "R002",
            "departmentId": "DEPT003",
            "firstName": "Barry",
            "lastName": "Kilpin",
            "gender": 1,
            "dob": "1980-01-24",
            "email": "bkilpini@company.com",
            "phoneNumber": "0554112408",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "81619 Gerald Junction",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 1,
            "role": "Office Manager",
            "department": "Ban điều hành",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 8,
            "userName": "bmancktelow1p",
            "roleId": "R002",
            "departmentId": "DEPT004",
            "firstName": "Bailie",
            "lastName": "Mancktelow",
            "gender": 0,
            "dob": "1997-05-23",
            "email": "bmancktelow1p@company.com",
            "phoneNumber": "0508724924",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "7 Anzinger Circle",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 1,
            "role": "Office Manager",
            "department": "Ban cấp cao",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 9,
            "userName": "bmckellen1o",
            "roleId": "R002",
            "departmentId": "DEPT001",
            "firstName": "Beckiex",
            "lastName": "McKellenx",
            "gender": 0,
            "dob": "1978-06-21",
            "email": "bmckellen1o@company.com.vn",
            "phoneNumber": "0364301999",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "170 6th Center xzy",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-23",
            "disableFlag": 1,
            "role": "Office Manager",
            "department": "Phòng Quản lý Nhập xuất",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        },
        {
            "row_num": 10,
            "userName": "cgobbet2e",
            "roleId": "R004",
            "departmentId": "DEPT004",
            "firstName": "Carlina",
            "lastName": "Gobbet",
            "gender": 0,
            "dob": "1984-10-20",
            "email": "cgobbet2e@company.com",
            "phoneNumber": "0599455979",
            "cityProvinceId": 1,
            "districtId": 1,
            "wardCommuneId": 1,
            "address": "66055 Lotheville Street",
            "companyInfoId": "COM001",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "disableFlag": 0,
            "role": "Office Employee",
            "department": "Ban cấp cao",
            "cityProvince": "Hồ Chí Minh",
            "district": "Bình Chánh",
            "wardCommune": "An Phú Tây"
        }
    ],
    "numberOfPages": 10,
    "totalEntries": 91
};

// -----------------------------------------
//
//    Sheet: Login
//
// -----------------------------------------
// UTC005
module.exports.login_utc005 = {
    "message": "Welcome back, insert05. You are now logged in.",
    "account": {
        "userName": "insert05",
        "roleId": "R002",
        "departmentId": "DEPT002",
        "firstName": "Oke",
        "lastName": "Hehe",
        "gender": 0,
        "dob": "1978-12-24",
        "email": "hoangchzhe130830@fpt.edu.vn",
        "phoneNumber": "0348881126",
        "cityProvinceId": 1,
        "cityProvince": "Hồ Chí Minh",
        "districtId": 1,
        "district": "Bình Chánh",
        "wardCommuneId": 1,
        "wardCommune": "An Phú Tây",
        "address": "Hello  1",
        "companyInfoId": "COM001",
        "createdDate": "2020-11-23",
        "updatedDate": "2020-11-23",
        "disableFlag": 0
    }
}

// -----------------------------------------
//
//    Sheet: Forgot Password
//
// -----------------------------------------
module.exports.forgotPwd_utc004 = {
    message: `An email has been sent to your inbox (hoangchhe130830@fpt.edu.vn). Please check it.`,
    email: "hoangchhe130830@fpt.edu.vn"
}

// -----------------------------------------
//
//    Sheet: Logout
//
// -----------------------------------------
module.exports.logout_utc004 = {
    message: "User with username: " + "test2" + " is logged out. See you again!"
}

// -----------------------------------------
//
//    Sheet: View Company Information
//
// -----------------------------------------
const sameCompany = {
    "companyInfo": {
        "companyInfoId": "COM001",
        "name": "Văn phòng ông Biđần 6",
        "cityProvinceId": 1,
        "districtId": 1,
        "wardCommuneId": 1,
        "address": "07 Nguyễn Thiền Quang 1",
        "phoneNumber": "0347619779",
        "email": "phone@phone.com.vn",
        "notes": "Văn phòng to nhất cái Việt Nam này nhes",
        "createdDate": "2020-10-14",
        "updatedDate": "2020-11-23",
        "updater": "test2",
        "companyWebsite": "biden.com.vn",
        "cityProvince": "Hồ Chí Minh",
        "district": "Bình Chánh",
        "wardCommune": "An Phú Tây"
    }
}

module.exports.viewCompanyInfo_utc010 = sameCompany;

module.exports.viewCompanyInfo_utc011 = sameCompany;

module.exports.viewCompanyInfo_utc012 = sameCompany;

module.exports.viewCompanyInfo_utc013 = sameCompany;