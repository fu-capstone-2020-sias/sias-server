const app = require("../../app");
const session = require("supertest-session");
var testSession = null;

const successResponse = require("./companyInfo_successResponse"); // Response Body trả về từ server
const failedResponse = require("./companyInfo_failedResponse");
const input = require("./companyInfo_input");
const authLoginObject = require("../shared/authenticate_loginObject");

beforeEach(function () {
    testSession = session(app);
});


// -----------------------------------------
//
//    Sheet: View Company Information
//
// -----------------------------------------
describe('-------Sheet: View Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_officeEmployee)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC013', function (done) {
        authenticatedSession.get('/company')
            .send()
            .expect(200, successResponse.viewCompanyInfo_utc013, done)
    });
});

// -----------------------------------------
//
//    Sheet: Modify Company Information
//
// -----------------------------------------
describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_admin)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC002', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc002)
            .expect(400, failedResponse.modifyCompanyInfo_utc002, done)
    });
});

describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_admin)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC003', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc003)
            .expect(400, failedResponse.modifyCompanyInfo_utc003, done)
    });
});

describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_admin)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC004', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc004)
            .expect(400, failedResponse.modifyCompanyInfo_utc004, done)
    });
});

describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_officeManager)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC005', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc005)
            .expect(401, failedResponse.modifyCompanyInfo_utc005, done)
    });
});

describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_factoryManager)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC006', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc006)
            .expect(401, failedResponse.modifyCompanyInfo_utc006, done)
    });
});

describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_officeEmployee)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC007', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc007)
            .expect(401, failedResponse.modifyCompanyInfo_utc007, done)
    });
});

describe('-------Sheet: Modify Company Information', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_admin)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC010', function (done) {
        authenticatedSession.put('/company')
            .send(input.modifyCompanyInfo_utc010)
            .expect(200, successResponse.modifyCompanyInfo_utc010, done)
    });
});