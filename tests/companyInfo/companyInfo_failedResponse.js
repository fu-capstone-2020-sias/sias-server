const commonException = require("../shared/common_exception");

const comboError = {
    "error": [
        "Invalid Company Name. Please provide Company Name again.\n",
        "City/Province is not existed! Please provide City/Province again.\n",
        "District is not existed! Please provide District again.\n",
        "Ward/Commune is not existed! Please provide Ward/Commune again.\n",
        "Invalid Working Address. Please provide Working Address again.\n",
        "Invalid Phone Number. Please provide Phone Number again.\n",
        "Invalid Email. Please provide Email again.\n",
        "Invalid Company Website. Please provide Company Website again.\n"
    ]
}


// -----------------------------------------
//
//    Sheet: Modify Company Info
//
// -----------------------------------------
module.exports.modifyCompanyInfo_utc002 = {
    error: [
        'Invalid Company Name. Please provide Company Name again.\n',
        'Invalid City/Province. Please provide City Province again.\n',
        'Invalid District. Please provide District again.\n',
        'Invalid Ward/Commune. Please provide Ward/Commune again.\n',
        'Invalid Working Address. Please provide Working Address again.\n',
        'Invalid Phone Number. Please provide Phone Number again.\n',
        'Invalid Email. Please provide Email again.\n',
        'Invalid Company Website. Please provide Company Website again.\n'
    ]
}

module.exports.modifyCompanyInfo_utc003 = comboError;

module.exports.modifyCompanyInfo_utc004 = comboError;

module.exports.modifyCompanyInfo_utc005 = commonException.unauthorizedAccess

module.exports.modifyCompanyInfo_utc006 = commonException.unauthorizedAccess

module.exports.modifyCompanyInfo_utc007 = commonException.unauthorizedAccess

