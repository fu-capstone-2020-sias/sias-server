const nameEmpty = '';
const nameValid = 'Cao Huy Hoàng';
const nameInvalid = 'C@o Huy Ho@ng';

const cityProvinceIdEmpty = '';
const cityProvinceIdNotExistedUpperBound = '64';
const cityProvinceIdNotExistedLowerBound = '0';
const cityProvinceIdExisted = '1';

const districtIdEmpty = '';
const districtIdNotExistedUpperBound = '1000000';
const districtIdNotExistedLowerBound = '0';
const districtIdExisted = '1';

const wardCommuneIdEmpty = '';
const wardCommuneIdNotExistedUpperBound = '1000000';
const wardCommuneIdNotExistedLowerBound = '0';
const wardCommuneIdExisted = '1';

const addressEmpty = '';
const addressValid = '7 Khâm Thiên';

const phoneNumberEmpty = '';
const phoneNumberValid = '0347669777';
const phoneNumberInvalid8Numbers = '0311cx3#2'
const phoneNumberInvalid11Numbers = '0348881234l'

const emailEmpty = '';
const emailInvalid = 'hoan#$gch@@zzz.com';
const emailValid = 'hoangch213@fpt.com';

const notesEmpty = '';
const notesValid = 'This is content 123';

const companyWebsiteEmpty = '';
const companyWebsiteValid = 'fpt.com.vn';

// -----------------------------------------
//
//    Sheet: Modify Company Information
//
// -----------------------------------------
module.exports.modifyCompanyInfo_utc002 = {
    name: nameEmpty,
    cityProvinceId: cityProvinceIdEmpty,
    districtId: districtIdEmpty,
    wardCommuneId: wardCommuneIdEmpty,
    address: addressEmpty,
    phoneNumber: phoneNumberEmpty,
    email: emailEmpty,
    notes: notesEmpty,
    companyWebsite: companyWebsiteEmpty
}

module.exports.modifyCompanyInfo_utc003 = {

    name: nameInvalid,
    cityProvinceId: cityProvinceIdNotExistedLowerBound,
    districtId: districtIdNotExistedLowerBound,
    wardCommuneId: wardCommuneIdNotExistedLowerBound,
    address: addressEmpty,
    phoneNumber: phoneNumberInvalid8Numbers,
    email: emailInvalid,
    notes: notesEmpty,
    companyWebsite: companyWebsiteEmpty
}

module.exports.modifyCompanyInfo_utc004 = {

    name: nameInvalid,
    cityProvinceId: cityProvinceIdNotExistedUpperBound,
    districtId: districtIdNotExistedUpperBound,
    wardCommuneId: wardCommuneIdNotExistedUpperBound,
    address: addressEmpty,
    phoneNumber: phoneNumberInvalid11Numbers,
    email: emailInvalid,
    notes: notesEmpty,
    companyWebsite: companyWebsiteEmpty
}

module.exports.modifyCompanyInfo_utc005 = {

    name: nameEmpty,
    cityProvinceId: cityProvinceIdEmpty,
    districtId: districtIdEmpty,
    wardCommuneId: wardCommuneIdEmpty,
    address: addressEmpty,
    phoneNumber: phoneNumberInvalid8Numbers,
    email: emailEmpty,
    notes: notesEmpty,
    companyWebsite: companyWebsiteEmpty
}

module.exports.modifyCompanyInfo_utc006 = {

    name: nameEmpty,
    cityProvinceId: cityProvinceIdNotExistedLowerBound,
    districtId: districtIdNotExistedLowerBound,
    wardCommuneId: wardCommuneIdNotExistedLowerBound,
    address: addressEmpty,
    phoneNumber: phoneNumberInvalid11Numbers,
    email: emailInvalid,
    notes: notesValid,
    companyWebsite: companyWebsiteValid
}

module.exports.modifyCompanyInfo_utc007 = {

    name: nameEmpty,
    cityProvinceId: cityProvinceIdNotExistedUpperBound,
    districtId: districtIdNotExistedUpperBound,
    wardCommuneId: wardCommuneIdNotExistedUpperBound,
    address: addressEmpty,
    phoneNumber: phoneNumberValid,
    email: emailValid,
    notes: notesEmpty,
    companyWebsite: companyWebsiteEmpty
}

module.exports.modifyCompanyInfo_utc010 = {

    name: nameValid,
    cityProvinceId: cityProvinceIdExisted,
    districtId: districtIdExisted,
    wardCommuneId: wardCommuneIdExisted,
    address: addressValid,
    phoneNumber: phoneNumberValid,
    email: emailValid,
    notes: notesValid,
    companyWebsite: companyWebsiteValid
}