// -----------------------------------------
//
//    Sheet: View Company Information
//
// -----------------------------------------
const sameCompany = {
    "companyInfo": {
        "companyInfoId": "COM001",
        "name": "Văn phòng ông Biđần 6",
        "cityProvinceId": 1,
        "districtId": 1,
        "wardCommuneId": 1,
        "address": "07 Nguyễn Thiền Quang 1",
        "phoneNumber": "0347619779",
        "email": "phone@phone.com.vn",
        "notes": "Văn phòng to nhất cái Việt Nam này nhes",
        "createdDate": "2020-10-14",
        "updatedDate": "2020-11-23",
        "updater": "test2",
        "companyWebsite": "biden.com.vn",
        "cityProvince": "Hồ Chí Minh",
        "district": "Bình Chánh",
        "wardCommune": "An Phú Tây"
    }
}

module.exports.viewCompanyInfo_utc010 = sameCompany;

module.exports.viewCompanyInfo_utc011 = sameCompany;

module.exports.viewCompanyInfo_utc012 = sameCompany;

module.exports.viewCompanyInfo_utc013 = sameCompany;

// -----------------------------------------
//
//    Sheet: Modify Company Information
//
// -----------------------------------------
module.exports.modifyCompanyInfo_utc010 = {
    message: "Company with ID: " + "COM001" + " updated."
}