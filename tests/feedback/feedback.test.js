const app = require("../../app");
const session = require("supertest-session");
var testSession = null;

const successResponse = require("./feedback_successResponse"); // Response Body trả về từ server
const failedResponse = require("./feedback_failedResponse");
const input = require("./feedback_input");
const authLoginObject = require("../shared/authenticate_loginObject");

beforeEach(function () {
    testSession = session(app);
});

// -----------------------------------------
//
//    Sheet: Feedback
//
// -----------------------------------------
// describe('-------Sheet: Feedback', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.post('/feedback')
//             .send(input.feedback_utc003)
//             .expect(400, failedResponse.feedback_utc003, done)
//     });
// });
//
// describe('-------Sheet: Feedback', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.post('/feedback')
//             .send(input.feedback_utc004)
//             .expect(400, failedResponse.feedback_utc004, done)
//     });
// });
//
// describe('-------Sheet: Feedback', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.post('/feedback')
//             .send(input.feedback_utc005)
//             .expect(400, failedResponse.feedback_utc005, done)
//     });
// });
//
// describe('-------Sheet: Feedback', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.post('/feedback')
//             .send(input.feedback_utc007)
//             .expect(200, successResponse.feedback_utc007, done)
//     });
// });

// -----------------------------------------
//
//    Sheet: View Feedback
//
// -----------------------------------------
describe('-------Sheet: View Feedback', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_officeManager)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC003', function (done) {
        authenticatedSession.get('/feedback')
            .send()
            .expect(401, failedResponse.viewFeedback_utc003, done)
    });
});

describe('-------Sheet: View Feedback', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_factoryManager)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC004', function (done) {
        authenticatedSession.get('/feedback')
            .send()
            .expect(401, failedResponse.viewFeedback_utc004, done)
    });
});

describe('-------Sheet: View Feedback', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_officeEmployee)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC005', function (done) {
        authenticatedSession.get('/feedback')
            .send()
            .expect(401, failedResponse.viewFeedback_utc005, done)
    });
});

describe('-------Sheet: View Feedback', function () {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(authLoginObject.auth_admin)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('UTC007', function (done) {
        authenticatedSession.get('/feedback')
            .send()
            .expect(200, successResponse.viewFeedback_utc007, done)
    });
});