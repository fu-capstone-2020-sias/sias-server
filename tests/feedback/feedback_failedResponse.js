const commonException = require("../shared/common_exception");

// -----------------------------------------
//
//    Sheet: Feedback
//
// -----------------------------------------
module.exports.feedback_utc003 = commonException.reqBodyEmpty;

module.exports.feedback_utc004 = {
    error: ["Invalid Title. Please provide Title again.\n",
        "Invalid Description. Please provide Description again."]
}

module.exports.feedback_utc005 = {
    error: ["Invalid Title. Please provide Title again.\n"]
}

// -----------------------------------------
//
//    Sheet: Feedback
//
// -----------------------------------------
module.exports.viewFeedback_utc003 = commonException.unauthorizedAccess

module.exports.viewFeedback_utc004 = commonException.unauthorizedAccess

module.exports.viewFeedback_utc005 = commonException.unauthorizedAccess