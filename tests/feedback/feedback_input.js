const titleValid = 'This is a title';
const titleInvalid = 'This is a title !@#%@';

const descriptionValid = 'This is a description';

// -----------------------------------------
//
//    Sheet: Feedback
//
// -----------------------------------------
module.exports.feedback_utc003 = {

}

module.exports.feedback_utc004 = {
    title: titleInvalid,
}

module.exports.feedback_utc005 = {
    title: titleInvalid,
    description: descriptionValid,
}

module.exports.feedback_utc007 = {
    title: titleValid,
    description: descriptionValid,
}