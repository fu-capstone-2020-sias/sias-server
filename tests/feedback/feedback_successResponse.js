// -----------------------------------------
//
//    Sheet: Feedback
//
// -----------------------------------------
module.exports.feedback_utc007 = {
    message: "An feedback was created successfully."
}

// -----------------------------------------
//
//    Sheet: View Feedback
//
// -----------------------------------------
module.exports.viewFeedback_utc007 = {
    "feedbacks": [
        {
            "feedbackId": 1,
            "title": "Les Feux Arctiques (Arktiset tulet)",
            "description": "aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet",
            "senderName": "hoangch1",
            "senderEmail": "hoangch1@company.com",
            "createdDate": "2020-10-14"
        },
        {
            "feedbackId": 2,
            "title": "Man in the Chair",
            "description": "neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras",
            "senderName": "hoangch1",
            "senderEmail": "hoangch1@company.com",
            "createdDate": "2020-10-14"
        },
        {
            "feedbackId": 3,
            "title": "Luke and Lucy: The Texas Rangers",
            "description": "luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis",
            "senderName": "hoangch1",
            "senderEmail": "hoangch1@company.com",
            "createdDate": "2020-10-14"
        },
        {
            "feedbackId": 4,
            "title": "Đây là feedback desu",
            "description": "Đây là mô tả",
            "senderName": "hoangch1",
            "senderEmail": "hoangchhe130830@fpt.edu.vn",
            "createdDate": "2020-10-22"
        },
        {
            "feedbackId": 5,
            "title": "Đây là feedback desu",
            "description": "Đây là mô tả",
            "senderName": "hoangch1",
            "senderEmail": "hoangchhe130830@fpt.edu.vn",
            "createdDate": "2020-10-28"
        },
        {
            "feedbackId": 6,
            "title": "Test1",
            "description": "second try",
            "senderName": "test1",
            "senderEmail": "hoangchhe130830@fpt.edu.vn",
            "createdDate": "2020-10-29"
        },
        {
            "feedbackId": 7,
            "title": "Test1",
            "description": "tell us your name",
            "senderName": "test1",
            "senderEmail": "hoangchhe130830@fpt.edu.vn",
            "createdDate": "2020-10-29"
        },
        {
            "feedbackId": 8,
            "title": "Test1",
            "description": "tell us again bro",
            "senderName": "test1",
            "senderEmail": "hoangchhe130830@fpt.edu.vn",
            "createdDate": "2020-10-29"
        },
        {
            "feedbackId": 9,
            "title": "Demo Paging",
            "description": "Demo Paging Description",
            "senderName": "hoangch1",
            "senderEmail": "nikokovac@g2.com",
            "createdDate": "2020-10-30"
        },
        {
            "feedbackId": 10,
            "title": "Demo Paging",
            "description": "Demo Paging Description",
            "senderName": "hoangch1",
            "senderEmail": "nikokovac@g2.com",
            "createdDate": "2020-10-30"
        }
    ],
    "numberOfPages": 4
}
