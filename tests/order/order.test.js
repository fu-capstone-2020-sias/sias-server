const app = require("../../app");
const session = require("supertest-session");
var testSession = null;

const successResponse = require("./order_successResponse"); // Response Body trả về từ server
const failedResponse = require("./order_failedResponse");
const input = require("./order_input");
const authLoginObject = require("../shared/authenticate_loginObject");

beforeEach(function () {
    testSession = session(app);
});

// // -----------------------------------------
// //
// //    Sheet: View order list
// //
// // -----------------------------------------
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.get('/order?page=1')
//             .send()
//             .expect(401, failedResponse.viewOrderList_utc002, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.get('/order?page=1')
//             .send()
//             .expect(200, successResponse.viewOrderList_utc005, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.get('/order?page=1')
//             .send()
//             .expect(200, successResponse.viewOrderList_utc006, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.get('/order?page=1')
//             .send()
//             .expect(200, successResponse.viewOrderList_utc007, done)
//     });
// });

// // -----------------------------------------
// //
// //    Sheet: View order detal
// //
// // -----------------------------------------
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.get('/order/4')
//             .send()
//             .expect(401, failedResponse.viewOrderDetail_utc002, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.get('/order/19999')
//             .send()
//             .expect(400, failedResponse.viewOrderDetail_utc004, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.get('/order/4')
//             .send()
//             .expect(200, successResponse.viewOrderDetail_utc006, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.get('/order/4')
//             .send()
//             .expect(200, successResponse.viewOrderDetail_utc007, done)
//     });
// });
//
// describe('-------Sheet: View order list', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeEmployee)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.get('/order/4')
//             .send()
//             .expect(400, successResponse.viewOrderDetail_utc008, done)
//     });
// });
//
// // -----------------------------------------
// //
// //    Sheet: Add new order
// //
// // -----------------------------------------
// describe('-------Sheet: Add new order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.post('/order/')
//             .send()
//             .expect(401, failedResponse.addNewOrder_utc002, done)
//     });
// });
//
// describe('-------Sheet: Add new order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.post('/order/')
//             .send()
//             .expect(400, failedResponse.addNewOder_utc003, done)
//     });
// });
//
//
// describe('-------Sheet: Add new order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.post('/order/')
//             .send(input.addNewOder_utc006)
//             .expect(400, failedResponse.addNewOder_utc006, done)
//     });
// });
//
// describe('-------Sheet: Add new order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.post('/order/')
//             .send(input.addNewOder_utc007)
//             .expect(400, failedResponse.addNewOder_utc007, done)
//     });
// });
//
// describe('-------Sheet: Add new order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.post('/order/')
//             .send(input.addNewOder_utc008)
//             .expect(400, failedResponse.addNewOder_utc008, done)
//     });
// });
//
// describe('-------Sheet: Add new order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC009', function (done) {
//         authenticatedSession.post('/order/')
//             .send(input.addNewOder_utc009)
//             .expect(200, done)
//     });
// });

// // -----------------------------------------
// //
// //    Sheet: Modify order information
// //
// // -----------------------------------------
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.put('/order/')
//             .send()
//             .expect(401, failedResponse.modifyOrderInfo_utc002, done)
//     });
// });
//
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.put('/order/')
//             .send(input.modifyOrderInfo_utc003)
//             .expect(400, failedResponse.modifyOrderInfo_utc003, done)
//     });
// });
//
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.put('/order/')
//             .send(input.modifyOrderInfo_utc005)
//             .expect(400, failedResponse.modifyOrderInfo_utc005, done)
//     });
// });
//
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.put('/order/')
//             .send(input.modifyOrderInfo_utc006)
//             .expect(400, failedResponse.modifyOrderInfo_utc006, done)
//     });
// });
//
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.put('/order/')
//             .send(input.modifyOrderInfo_utc007)
//             .expect(400, failedResponse.modifyOrderInfo_utc007, done)
//     });
// });
//
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.put('/order/')
//             .send(input.modifyOrderInfo_utc008)
//             .expect(400, failedResponse.modifyOrderInfo_utc008, done)
//     });
// });
//
// describe('-------Sheet: Modify order information', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC009', function (done) {
//         authenticatedSession.put('/order/')
//             .send(input.modifyOrderInfo_utc009)
//             .expect(200, successResponse.modifyOrderInfo_utc009, done)
//     });
// });

//
// // -----------------------------------------
// //
// //    Sheet: Delete rejected orders
// //
// // -----------------------------------------
// describe('-------Sheet: Delete rejected orders', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.delete('/order/')
//             .send()
//             .expect(401, failedResponse.deleteRejectedOrders_utc002, done)
//     });
// });
//
// describe('-------Sheet: Delete rejected orders', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC003', function (done) {
//         authenticatedSession.delete('/order/')
//             .send(input.deleteRejectedOrders_utc003)
//             .expect(400, failedResponse.deleteRejectedOrders_utc003, done)
//     });
// });
//
// describe('-------Sheet: Delete rejected orders', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.delete('/order/')
//             .send(input.deleteRejectedOrders_utc006)
//             .expect(200, failedResponse.deleteRejectedOrders_utc006, done)
//     });
// });
//
// describe('-------Sheet: Delete rejected orders', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.delete('/order/')
//             .send(input.deleteRejectedOrders_utc007)
//             .expect(200, failedResponse.deleteRejectedOrders_utc007, done)
//     });
// });
//
// describe('-------Sheet: Delete rejected orders', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.delete('/order/')
//             .send(input.deleteRejectedOrders_utc008)
//             .expect(200, successResponse.deleteRejectedOrders_utc008, done)
//     });
// });


// // -----------------------------------------
// //
// //    Sheet: Search order
// //
// // -----------------------------------------
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send()
//             .expect(401, failedResponse.searchOrder_utc002, done)
//     });
// });
//
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send(input.searchOrder_utc006)
//             .expect(200, successResponse.searchOrder_utc006, done)
//     });
// });
//
//
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send(input.searchOrder_utc007)
//             .expect(400, failedResponse.searchOrder_utc007, done)
//     });
// });
//
//
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send(input.searchOrder_utc008)
//             .expect(400, failedResponse.searchOrder_utc008, done)
//     });
// });
//
//
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC009', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send(input.searchOrder_utc009)
//             .expect(400, failedResponse.searchOrder_utc009, done)
//     });
// });
//
//
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC010', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send(input.searchOrder_utc010)
//             .expect(400, failedResponse.searchOrder_utc010, done)
//     });
// });
//
//
// describe('-------Sheet: Search order', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC011', function (done) {
//         authenticatedSession.post('/order/search?page=1')
//             .send(input.searchOrder_utc011)
//             .expect(200, successResponse.searchOrder_utc011, done)
//     });
// });


// // -----------------------------------------
// //
// //    Sheet: Modify order status
// //
// // -----------------------------------------
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '37';
//     let orderStatusId = 'OS005';
//     it('UTC002', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(401, failedResponse.changeOrderStatus_utc002, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus`)
//             .send()
//             .expect(400, failedResponse.changeOrderStatus_utc004, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '999999';
//     let orderStatusId = 'OS005z';
//     it('UTC006', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(400, failedResponse.changeOrderStatus_utc006, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '195';
//     let orderStatusId = 'OS003';
//     it('UTC007', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(400, failedResponse.changeOrderStatus_utc007, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '196';
//     let orderStatusId = 'OS002';
//     it('UTC008', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(400, failedResponse.changeOrderStatus_utc008, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '197';
//     let orderStatusId = 'OS005';
//     it('UTC009', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(400, failedResponse.changeOrderStatus_utc009, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '198';
//     let orderStatusId = 'OS005';
//     it('UTC010', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(400, failedResponse.changeOrderStatus_utc010, done)
//     });
// });
//
// describe('-------Sheet: Modify order status', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     let orderId = '199';
//     let orderStatusId = 'OS005';
//     it('UTC011', function (done) {
//         authenticatedSession.put(`/order/changeOrderStatus?orderId=${orderId}&orderStatusId=${orderStatusId}`)
//             .send()
//             .expect(200, successResponse.changeOrderStatus_utc011, done)
//     });
// });


// // -----------------------------------------
// //
// //    Sheet: Get order item by Order ID
// //
// // -----------------------------------------
// describe('-------Sheet: Get order item by Order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.get(`/order/item?orderId=4`)
//             .send()
//             .expect(401, failedResponse.getOrderItemByOrderId_utc002, done)
//     });
// });
//
// describe('-------Sheet: Get order item by Order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.get(`/order/item?orderId=4`)
//             .send()
//             .expect(200, successResponse.getOrderItemByOrderId_utc005, done)
//     });
// });

//
// // -----------------------------------------
// //
// //    Sheet: Add new order item by order ID
// //
// // -----------------------------------------
// describe('-------Sheet: Add new order item by order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.post(`/order/4/item`)
//             .send()
//             .expect(401, failedResponse.addNewOrderItem_utc002, done)
//     });
// });
//
// describe('-------Sheet: Add new order item by order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.post(`/order/199/item`)
//             .send(input.addNewOrderItem_utc004)
//             .expect(400, failedResponse.addNewOrderItem_utc004, done)
//     });
// });
//
// describe('-------Sheet: Add new order item by order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.post(`/order/4/item`)
//             .send(input.addNewOrderItem_utc006)
//             .expect(400, failedResponse.addNewOrderItem_utc006, done)
//     });
// });
//
// describe('-------Sheet: Add new order item by order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.post(`/order/NotExisted/item`)
//             .send(input.addNewOrderItem_utc007)
//             .expect(400, failedResponse.addNewOrderItem_utc007, done)
//     });
// });
//
// describe('-------Sheet: Add new order item by order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.post(`/order/200/item`)
//             .send(input.addNewOrderItem_utc008)
//             .expect(400, failedResponse.addNewOrderItem_utc008, done)
//     });
// });
//
// describe('-------Sheet: Add new order item by order ID', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC009', function (done) {
//         authenticatedSession.post(`/order/200/item`)
//             .send(input.addNewOrderItem_utc009)
//             .expect(200, successResponse.addNewOrderItem_utc009, done)
//     });
// });


// // -----------------------------------------
// //
// //    Sheet: Update order item
// //
// // -----------------------------------------
// describe('-------Sheet: Update order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.put(`/order/133/item`)
//             .send()
//             .expect(401, failedResponse.updateOrderItem_utc002, done)
//     });
// });
//
// describe('-------Sheet: Update order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.put(`/order/133/item`)
//             .send(input.updateOrderItem_utc004)
//             .expect(400, failedResponse.updateOrderItem_utc004, done)
//     });
// });
//
//
// describe('-------Sheet: Update order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.put(`/order/133/item`)
//             .send(input.updateOrderItem_utc006)
//             .expect(400, failedResponse.updateOrderItem_utc006, done)
//     });
// });
//
// describe('-------Sheet: Update order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.put(`/order/200/item`)
//             .send(input.updateOrderItem_utc007)
//             .expect(400, failedResponse.updateOrderItem_utc007, done)
//     });
// });
//
// describe('-------Sheet: Update order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.put(`/order/200/item`)
//             .send(input.updateOrderItem_utc008)
//             .expect(400, failedResponse.updateOrderItem_utc008, done)
//     });
// });
//
// describe('-------Sheet: Update order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC009', function (done) {
//         authenticatedSession.put(`/order/200/item`)
//             .send(input.updateOrderItem_utc009)
//             .expect(200, successResponse.updateOrderItem_utc009, done)
//     });
// });

// // -----------------------------------------
// //
// //    Sheet: Delete order item
// //
// // ----------------------------------------
// describe('-------Sheet: Delete order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.delete(`/order/items`)
//             .send()
//             .expect(401, failedResponse.deleteOrderItem_utc002, done)
//     });
// });
//
// describe('-------Sheet: Delete order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.delete(`/order/items`)
//             .send(input.deleteOrderItem_utc004)
//             .expect(400, failedResponse.deleteOrderItem_utc004, done)
//     });
// });
//
// describe('-------Sheet: Delete order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.delete(`/order/items`)
//             .send(input.deleteOrderItem_utc005)
//             .expect(200, failedResponse.deleteOrderItem_utc005, done)
//     });
// });
//
// describe('-------Sheet: Delete order item', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_officeManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC007', function (done) {
//         authenticatedSession.delete(`/order/items`)
//             .send(input.deleteOrderItem_utc007)
//             .expect(200, successResponse.deleteOrderItem_utc007, done)
//     });
// });


// // -----------------------------------------
// //
// //    Sheet: Create Cutting Instruction
// //
// // ----------------------------------------
// describe('-------Sheet: Create Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.post(`/order/createInstruction?orderId=` + 4)
//             .send()
//             .expect(401, failedResponse.createCuttingIns_utc002, done)
//     });
// });
//
// describe('-------Sheet: Create Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.post(`/order/createInstruction?orderId=`)
//             .send()
//             .expect(400, failedResponse.createCuttingIns_utc004, done)
//     });
// });
//
// describe('-------Sheet: Create Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send({
//                 userName: 'insert05',
//                 password: '123456@aA'
//             })
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC005', function (done) {
//         authenticatedSession.post(`/order/createInstruction?orderId=` + 4)
//             .send()
//             .expect(400, failedResponse.createCuttingIns_utc005, done)
//     });
// });
//
// describe('-------Sheet: Create Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.post(`/order/createInstruction?orderId=` + 65)
//             .send()
//             .expect(400, failedResponse.createCuttingIns_utc006, done)
//     });
// });
//
// describe('-------Sheet: Create Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.post(`/order/createInstruction?orderId=` + 4)
//             .send()
//             .expect(400, failedResponse.createCuttingIns_utc008, done)
//     });
// });
//
// describe('-------Sheet: Create Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC010', function (done) {
//         authenticatedSession.post(`/order/createInstruction?orderId=` + 85)
//             .send()
//             .expect(200, successResponse.createCuttingIns_utc010, done)
//     });
// });

//
// // -----------------------------------------
// //
// //    Sheet: View Cutting Instruction
// //
// // ----------------------------------------
// describe('-------Sheet: View Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_admin)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC002', function (done) {
//         authenticatedSession.get(`/order/cuttingInstruction?orderId=` + 4)
//             .send()
//             .expect(401, failedResponse.viewCuttingIns_utc002, done)
//     });
// });
//
// describe('-------Sheet: View Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC004', function (done) {
//         authenticatedSession.get(`/order/cuttingInstruction?orderId=`)
//             .send()
//             .expect(400, failedResponse.viewCuttingIns_utc004, done)
//     });
// });
//
// describe('-------Sheet: View Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send({
//                 userName: 'insert05',
//                 password: '123456@aA'
//             })
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC006', function (done) {
//         authenticatedSession.get(`/order/cuttingInstruction?orderId=` + 4)
//             .send()
//             .expect(400, failedResponse.viewCuttingIns_utc006, done)
//     });
// });
//
// describe('-------Sheet: View Cutting Instruction', function () {
//     var authenticatedSession = null;
//
//     beforeEach(function (done) {
//         testSession.post('/auth/login')
//             .send(authLoginObject.auth_factoryManager)
//             .expect(200)
//             .end(function (err) {
//                 if (err) return done(err);
//                 authenticatedSession = testSession;
//                 return done();
//             });
//     });
//
//     it('UTC008', function (done) {
//         authenticatedSession.get(`/order/cuttingInstruction?orderId=` + 4)
//             .send()
//             .expect(200, successResponse.viewCuttingIns_utc008, done)
//     });
// });
//
