const commonException = require("../shared/common_exception");

// -----------------------------------------
//
//    Sheet:  View order list
//
// -----------------------------------------
module.exports.viewOrderList_utc002 = commonException.unauthorizedAccess;

// -----------------------------------------
//
//    Sheet:  View order detail
//
// -----------------------------------------
module.exports.viewOrderDetail_utc002 = commonException.unauthorizedAccess;

module.exports.viewOrderDetail_utc004 = {
    error: ["No order with ID: " + '19999' + " to be found."]
}

// -----------------------------------------
//
//    Sheet:  Add new order
//
// -----------------------------------------
module.exports.addNewOrder_utc002 = commonException.unauthorizedAccess;

module.exports.addNewOder_utc003 = commonException.reqBodyEmpty;

module.exports.addNewOder_utc006 = {
    "error": [
        "Invalid Approver. Please provide Approver again.\n",
        "Invalid Assignee. Please provide Assignee again.\n"
    ]
}

module.exports.addNewOder_utc007 = {
    "error": [
        "Approver is not existed. Please enter again.\n",
        "Assignee is not existed. Please enter again.\n"
    ]
}

module.exports.addNewOder_utc008 = {
    "error": [
        "Invalid Order Status ID. Please provide Order Status ID again.",
        "Invalid Customer Name. Please provide Customer Name again.\n",
        "Invalid Company Name. Please provide Company Name again.\n",
        "City/Province is not existed! Please provide City Province again.\n",
        "District is not existed! Please provide District again.\n",
        "Ward/Commune is not existed! Please provide Ward/Commune again.\n",
        "Invalid Phone Number. Please provide Phone Number again.\n",
        "Invalid Email. Please provide Email again.\n",
        "Approver is not existed. Please enter again.\n",
        "Assignee is not existed. Please enter again.\n"
    ]
}

// -----------------------------------------
//
//    Sheet:  Modify Order Information
//
// -----------------------------------------
module.exports.modifyOrderInfo_utc002 = commonException.unauthorizedAccess;

module.exports.modifyOrderInfo_utc003 = commonException.reqBodyEmpty;

module.exports.modifyOrderInfo_utc005 = {
    error: ["You must send with valid screen_id parameter for this request!"]
}
module.exports.modifyOrderInfo_utc006 = {
    error: ["Order with ID: " + "Not Existed" + " NOT existed.\n"]
};

module.exports.modifyOrderInfo_utc007 = {
    "error": [
        "Invalid Order Status ID. Please provide Order Status ID again.",
        "Invalid Customer Name. Please provide Customer Name again.\n",
        "Invalid Company Name. Please provide Company Name again.\n",
        "City/Province is not existed! Please provide City Province again.\n",
        "District is not existed! Please provide District again.\n",
        "Ward/Commune is not existed! Please provide Ward/Commune again.\n",
        "Invalid Address. Please provide Address again.",
        "Invalid Phone Number. Please provide Phone Number again.\n",
        "Invalid Email. Please provide Email again.\n",
    ]
}

module.exports.modifyOrderInfo_utc008 = {
    "error": [
        "City/Province is not existed! Please provide City Province again.\n",
        "District is not existed! Please provide District again.\n",
        "Ward/Commune is not existed! Please provide Ward/Commune again.\n",
    ]
}

// -----------------------------------------
//
//    Sheet:  Delete rejected orders
//
// -----------------------------------------
module.exports.deleteRejectedOrders_utc002 = commonException.unauthorizedAccess;

module.exports.deleteRejectedOrders_utc003 = commonException.reqBodyEmpty;

module.exports.deleteRejectedOrders_utc006 = {error: "No order is deleted."}

module.exports.deleteRejectedOrders_utc007 = {error: "No order is deleted."}

// -----------------------------------------
//
//    Sheet:  Search order
//
// -----------------------------------------

module.exports.searchOrder_utc002 = commonException.unauthorizedAccess

module.exports.searchOrder_utc007 = {
    error: ["Field 'Order Status' is in wrong format."]
}

module.exports.searchOrder_utc008 = {error: ["Field 'From Date' is in wrong format."]}


module.exports.searchOrder_utc009 = {error: ["Field 'From Date' is in wrong format."]}


module.exports.searchOrder_utc010 = {
    error: ["Field 'From Date' is required before field 'To Date'."]
}


// -----------------------------------------
//
//    Sheet:  Modify order status
//
// -----------------------------------------
module.exports.changeOrderStatus_utc002 = commonException.unauthorizedAccess;

module.exports.changeOrderStatus_utc004 =
    {error: ['req.query is EMPTY.']}


module.exports.changeOrderStatus_utc006 = {
    error: [
        "Order with ID: " +
        999999 + " NOT existed or you are not authorized to do this action.\n"
    ]
}

module.exports.changeOrderStatus_utc007 = {
    error: ["Order Status 'Pending' (OS001) must be changed to 'Approved' (OS002) or 'Rejected' (OS005)."]
}

module.exports.changeOrderStatus_utc008 = {
    error: ["Order Status 'Completed' (OS004) or Rejected' (OS005) cannot be changed to another status because" +
    " it is the end of workflow."]
}

module.exports.changeOrderStatus_utc009 = {
    error: ["Order Status 'Processing' (OS003) must be changed to 'Completed' (OS004)."]
}

module.exports.changeOrderStatus_utc010 = {
    error: ["Order Status 'Approved' (OS002) must be changed to 'Processing' (OS003)."]
}


// -----------------------------------------
//
//    Sheet:  Get order item by Order ID
//
// -----------------------------------------
module.exports.getOrderItemByOrderId_utc002 = commonException.unauthorizedAccess;

// -----------------------------------------
//
//    Sheet: Add new order item by order ID
//
// -----------------------------------------
module.exports.addNewOrderItem_utc002 = commonException.unauthorizedAccess;

module.exports.addNewOrderItem_utc004 = commonException.reqBodyEmpty;

module.exports.addNewOrderItem_utc006 = {
    error: ["Order's status (ID: " + 4 + ") must be 'Pending' to be able to do CRUD."]
}

module.exports.addNewOrderItem_utc007 = {
    error: [
        "Order with ID: " +
        "NotExisted" + " NOT existed.\n"
    ]
}

module.exports.addNewOrderItem_utc008 = {
    "error": [
        "Invalid Length. Please provide Length again.",
        "Invalid Quantity. Please provide Quantity again.",
        "Invalid Blade Width. Please provide Blade Width again.",
        "Invalid Price. Please provide Price again."
    ]
}

// -----------------------------------------
//
//    Sheet: Update order item
//
// -----------------------------------------
module.exports.updateOrderItem_utc002 = commonException.unauthorizedAccess;

module.exports.updateOrderItem_utc004 = commonException.reqBodyEmpty;

module.exports.updateOrderItem_utc006 = {
    error: ["Order's status (ID: " + 133 + ") must be 'Pending' to be able to do CRUD."]
}

module.exports.updateOrderItem_utc007 = {
    error: [
        "Order Item with ID: " +
        49645645 + " NOT existed.\n"
    ]
}

module.exports.updateOrderItem_utc008 = {
    error: [
        "Invalid Length. Please provide Length again.",
        "Invalid Quantity. Please provide Quantity again.",
        "Invalid Blade Width. Please provide Blade Width again.",
        "Invalid Price. Please provide Price again."
    ]
}

// -----------------------------------------
//
//    Sheet: Delete order item
//
// -----------------------------------------
module.exports.deleteOrderItem_utc002 = commonException.unauthorizedAccess;

module.exports.deleteOrderItem_utc004 = commonException.reqBodyEmpty;

module.exports.deleteOrderItem_utc005 = {
    message: "No order item is deleted."
}

// -----------------------------------------
//
//    Sheet: Create Cutting Instruction
//
// -----------------------------------------
module.exports.createCuttingIns_utc002 = commonException.unauthorizedAccess;

module.exports.createCuttingIns_utc004 = {
    error: ["Missing orderId parameter!"]
}

module.exports.createCuttingIns_utc005 = {
    error: [
        'This order (Order ID: 4) has existed Cutting Instruction. You can not create a new one!'
    ]

}

// Order ID 4
module.exports.createCuttingIns_utc006 = {
    error: ["This order (Order ID: " + 65 + ") must have status of 'Processing' (OS003)."]
}

// Order ID 4
module.exports.createCuttingIns_utc008 = {
    error: ["This order (Order ID: " + 4 + ") has existed Cutting Instruction. You can not create a new one!"]
}

module.exports.createCuttingIns_utc009 = {
    message: ["There are not enough stock products in all warehouses!"]
}

// -----------------------------------------
//
//    Sheet: View Cutting Instruction
//
// -----------------------------------------
module.exports.viewCuttingIns_utc002 = commonException.unauthorizedAccess;

module.exports.viewCuttingIns_utc004 = { error: [ 'Missing orderId parameter!' ] }


module.exports.viewCuttingIns_utc006 = {
    error: ["There is no cutting instruction for this order (Order ID: " + 4 + ") or " +
    "you are not Assignee for this cutting instruction."]
}