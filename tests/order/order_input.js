// -----------------------------------------
//
//    Sheet:  View order list
//
// -----------------------------------------
// No need input

// -----------------------------------------
//
//    Sheet:  View order detail
//
// -----------------------------------------
// No need input

// -----------------------------------------
//
//    Sheet:  Add new order
//
// -----------------------------------------
module.exports.addNewOder_utc003 = {
    // req body empty
}

module.exports.addNewOder_utc006 = {
    orderStatusId: "OS001",
    customerName: "Cao Hoàng",
    customerCompanyName: "123 Company",
    customerCityProvinceId: "1",
    customerDistrictId: "1",
    customerWardCommuneId: "1",
    customerAddress: "7 Khâm Thiên",
    customerPhoneNumber: "0341115555",
    customerEmail: "hoangchhe130830@fpt.edu.vn",
    approver: "",
    assignee: ""
}

module.exports.addNewOder_utc007 = {
    orderStatusId: "OS001",
    customerName: "Cao Hoàng",
    customerCompanyName: "123 Company",
    customerCityProvinceId: "1",
    customerDistrictId: "1",
    customerWardCommuneId: "1",
    customerAddress: "7 Khâm Thiên",
    customerPhoneNumber: "0341115555",
    customerEmail: "hoangchhe130830@fpt.edu.vn",
    approver: "test1c",
    assignee: "test1c"
}

module.exports.addNewOder_utc008 = {
    orderStatusId: "OS001z",
    customerName: "Cao Hoàng123",
    customerCompanyName: "123 Company@",
    customerCityProvinceId: "19999999",
    customerDistrictId: "199999",
    customerWardCommuneId: "199999",
    customerAddress: "3 Ba Dinh, H4 Noi!@#n",
    customerPhoneNumber: "0311cx3#2",
    customerEmail: "ho!@#XZC.com",
    approver: "test1c",
    assignee: "test1c"
}

module.exports.addNewOder_utc009 = {
    orderStatusId: "OS001",
    customerName: "Cao Hoàng",
    customerCompanyName: "123 Company",
    customerCityProvinceId: "1",
    customerDistrictId: "1",
    customerWardCommuneId: "1",
    customerAddress: "7 Khâm Thiên",
    customerPhoneNumber: "0341115555",
    customerEmail: "hoangchhe130830@fpt.edu.vn",
    approver: "test1",
    assignee: "factoryManager"
}

// -----------------------------------------
//
//    Sheet:  Modify Order Information
//
// -----------------------------------------
module.exports.modifyOrderInfo_utc003 = {
    // empty
}

module.exports.modifyOrderInfo_utc005 = {
    screen_id: ""
}

module.exports.modifyOrderInfo_utc006 = {
    orderId: "Not Existed",
    orderStatusId: "",
    customerName: "",
    customerCompanyName: "",
    customerCityProvinceId: "",
    customerDistrictId: "",
    customerWardCommuneId: "",
    customerAddress: "",
    customerPhoneNumber: "",
    customerEmail: "",
    screen_id: "WEB_UC33"
}

module.exports.modifyOrderInfo_utc007 = {
    orderId: "30",
    orderStatusId: "OS001z",
    customerName: "Cao Hoàng123",
    customerCompanyName: "123 Company@",
    customerCityProvinceId: "19999999",
    customerDistrictId: "19999999",
    customerWardCommuneId: "19999999",
    customerAddress: "",
    customerPhoneNumber: "0312#!@4",
    customerEmail: "@#Xzc123#$^T%@fpt.edu.vn",
    screen_id: "WEB_UC33"
}

module.exports.modifyOrderInfo_utc008 = {
    orderId: "30",
    orderStatusId: "OS001",
    customerName: "Cao Hoàng",
    customerCompanyName: "123 Company",
    customerCityProvinceId: "19999999",
    customerDistrictId: "19999999",
    customerWardCommuneId: "19999999",
    customerAddress: "7 Khâm Thiên",
    customerPhoneNumber: "0347619999",
    customerEmail: "hoangchhe130830@fpt.edu.vn",
    screen_id: "WEB_UC33"
}

module.exports.modifyOrderInfo_utc009 = {
    orderId: "30",
    orderStatusId: "OS001",
    customerName: "Cao Hoàng",
    customerCompanyName: "123 Company",
    customerCityProvinceId: "1",
    customerDistrictId: "1",
    customerWardCommuneId: "1",
    customerAddress: "7 Khâm Thiên",
    customerPhoneNumber: "0341115555",
    customerEmail: "hoangchhe130830@fpt.edu.vn",
    screen_id: "WEB_UC33"
}

// -----------------------------------------
//
//    Sheet:  Delete rejected orders
//
// -----------------------------------------
module.exports.deleteRejectedOrders_utc003 = {
    // req body empty
}

module.exports.deleteRejectedOrders_utc006 = {
    arrOfOrderIds: "28"
}

module.exports.deleteRejectedOrders_utc007 = {
    arrOfOrderIds: "dzklxck,zxczjda"
}

module.exports.deleteRejectedOrders_utc008 = {
    arrOfOrderIds: "203,204"
}


// -----------------------------------------
//
//    Sheet:  Search order
//
// -----------------------------------------
module.exports.searchOrder_utc006 = {
    orderId: "Not Existed"
}

module.exports.searchOrder_utc007 = {
    orderId: "30",
    orderStatusId: "OS001z",
    customerName: "Cao Hoàng 123",
    customerCompanyName: "123 Company@",
    customerPhoneNumber: "0341115555",
    customerEmail: "@!Q##@xz.com",
    searchByDate: "0",
    fromDate: "",
    toDate: ""
}

module.exports.searchOrder_utc008 = {
    orderId: "30",
    orderStatusId: "OS001",
    customerName: "Cao Hoàng 123",
    customerCompanyName: "123 Company@",
    customerPhoneNumber: "0341115555",
    customerEmail: "@!Q##@xz.com",
    searchByDate: "1",
    fromDate: "",
    toDate: ""
}

module.exports.searchOrder_utc009 = {
    orderId: "30",
    orderStatusId: "OS001",
    customerName: "Cao Hoàng 123",
    customerCompanyName: "123 Company@",
    customerPhoneNumber: "0341115555",
    customerEmail: "@!Q##@xz.com",
    searchByDate: "1",
    fromDate: "2019-11-czx",
    toDate: "2019-11-czx"
}

module.exports.searchOrder_utc010 = {
    orderId: "30",
    orderStatusId: "OS001",
    customerName: "Cao Hoàng 123",
    customerCompanyName: "123 Company@",
    customerPhoneNumber: "0341115555",
    customerEmail: "@!Q##@xz.com",
    searchByDate: "1",
    toDate: "2019-11-12"
}

module.exports.searchOrder_utc011 = {
    orderId: '30',
    orderStatusId: "OS001",
    customerName: "Cao Hoàng",
    customerCompanyName: "123 Company",
    customerPhoneNumber: "0341115555",
    customerEmail: "hoangchhe130830@fpt.edu.vn",
    searchByDate: "1",
    fromDate: "2019-11-10",
    toDate: "2020-12-30"
}

// -----------------------------------------
//
//    Sheet:  Modify order status
//
// -----------------------------------------
// It is web parameters

// -----------------------------------------
//
//    Sheet: Add new order item by order ID
//
// -----------------------------------------
module.exports.addNewOrderItem_utc004 = []

// Pending to do CRUD
module.exports.addNewOrderItem_utc006 = [{
    length: '300',
    bladeWidth: '5',
    quantity: '5',
    price: '1000',
}]

// Order ID not exist
module.exports.addNewOrderItem_utc007 = [{
    length: '300',
    bladeWidth: '5',
    quantity: '5',
    price: '1000',
}]


module.exports.addNewOrderItem_utc008 = [{
    length: '300abc',
    bladeWidth: '5abc',
    quantity: '5abc',
    price: '1000abc',
}]

module.exports.addNewOrderItem_utc009 = [
    {
        "length": "300",
        "bladeWidth": "5",
        "quantity": "5",
        "price": "1000"
    }
]

// -----------------------------------------
//
//    Sheet: Update order item
//
// -----------------------------------------
module.exports.updateOrderItem_utc004 = {
    // req body empty
}

// Order ID 133
module.exports.updateOrderItem_utc006 = [
    {
        "orderItemId": '49',
        "length": "300",
        "bladeWidth": '5',
        "quantity": '5',
        "price": "2000"
    }
]

module.exports.updateOrderItem_utc007 = [
    {
        "orderItemId": '49645645',
        "length": "300",
        "bladeWidth": '5',
        "quantity": '5',
        "price": "2000"
    }
]

module.exports.updateOrderItem_utc008 = [
    {
        "orderItemId": '49abcv',
        "length": "300zc",
        "bladeWidth": '5zxc',
        "quantity": '5zxc',
        "price": "2000zxc"
    }
]

// Order ID 200
module.exports.updateOrderItem_utc009 = [
    {
        "orderItemId": '79',
        "length": "300",
        "bladeWidth": '5',
        "quantity": '5',
        "price": "2000"
    }
]

// -----------------------------------------
//
//    Sheet: Delete order item
//
// -----------------------------------------
module.exports.deleteOrderItem_utc004 = {
    // req body empty
}

// Not existed order item id
module.exports.deleteOrderItem_utc005 = {
    arrOfOrderItemIds: "99999,1000000"
}

// Not existed order item id
module.exports.deleteOrderItem_utc005 = {
    arrOfOrderItemIds: "99999,1000000"
}

module.exports.deleteOrderItem_utc007 = {
    arrOfOrderItemIds: "80,81"
}

// -----------------------------------------
//
//    Sheet: Create Cutting Instruction
//
// -----------------------------------------
// It is web parameter