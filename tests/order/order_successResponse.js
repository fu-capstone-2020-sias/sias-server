// -----------------------------------------
//
//    Sheet:  View order list
//
// -----------------------------------------
module.exports.viewOrderList_utc005 = {
    "orders": [
        {
            "orderId": 4,
            "orderStatusId": "OS003",
            "customerName": "CSGO Player Đẹp Trai",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-16",
            "creator": "test1",
            "updater": "test1",
            "approver": "factoryManager",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 5,
            "orderStatusId": "OS003",
            "customerName": "Laurence Rickson",
            "customerCompanyName": "Bins LLC",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "822 Iowa Alley",
            "customerPhoneNumber": "26317160374",
            "customerEmail": "hoangch1@company.com",
            "totalPrice": "1000",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 6,
            "orderStatusId": "OS003",
            "customerName": "Annabelle Hume",
            "customerCompanyName": "Blanda, Vandervort and Bradtke",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "9223 Elmside Park",
            "customerPhoneNumber": "20241988988",
            "customerEmail": "hoangch1@company.com",
            "totalPrice": "5000",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 7,
            "orderStatusId": "OS004",
            "customerName": "Edithe Sibary",
            "customerCompanyName": "Luettgen LLC",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "4 Trailsway Crossing",
            "customerPhoneNumber": "27751985130",
            "customerEmail": "hoangch1@company.com",
            "totalPrice": "1000",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "test1",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Completed",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 17,
            "orderStatusId": "OS004",
            "customerName": "Cao Hoàng",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-21",
            "updatedDate": null,
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "factoryManager",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Completed",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 20,
            "orderStatusId": "OS003",
            "customerName": "Hieu BT dang test",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0323445678",
            "customerEmail": "hieubthe130790@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-21",
            "updatedDate": "2020-11-16",
            "creator": "test1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 21,
            "orderStatusId": "OS003",
            "customerName": "Cao Hoàng",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-21",
            "updatedDate": null,
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 25,
            "orderStatusId": "OS003",
            "customerName": "Nguyen Văn Ba",
            "customerCompanyName": "The 85th District Inc",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0347778888",
            "customerEmail": "company@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": null,
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 26,
            "orderStatusId": "OS003",
            "customerName": "Jonh McKen",
            "customerCompanyName": "Vjp pr0",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0347778888",
            "customerEmail": "company@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": null,
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 27,
            "orderStatusId": "OS002",
            "customerName": "Nguyen Văn Ba",
            "customerCompanyName": "The 85th District Inc",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0347778888",
            "customerEmail": "company@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": null,
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Approved",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        }
    ],
    "numberOfPages": 5,
    "totalEntries": 48
}

module.exports.viewOrderList_utc006 = {
    "orders": [
        {
            "orderId": 4,
            "orderStatusId": "OS003",
            "customerName": "CSGO Player Đẹp Trai",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-16",
            "creator": "test1",
            "updater": "test1",
            "approver": "factoryManager",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 5,
            "orderStatusId": "OS003",
            "customerName": "Laurence Rickson",
            "customerCompanyName": "Bins LLC",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "822 Iowa Alley",
            "customerPhoneNumber": "26317160374",
            "customerEmail": "hoangch1@company.com",
            "totalPrice": "1000",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 6,
            "orderStatusId": "OS003",
            "customerName": "Annabelle Hume",
            "customerCompanyName": "Blanda, Vandervort and Bradtke",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "9223 Elmside Park",
            "customerPhoneNumber": "20241988988",
            "customerEmail": "hoangch1@company.com",
            "totalPrice": "5000",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-10-14",
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 17,
            "orderStatusId": "OS004",
            "customerName": "Cao Hoàng",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-21",
            "updatedDate": null,
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "factoryManager",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Completed",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 20,
            "orderStatusId": "OS003",
            "customerName": "Hieu BT dang test",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0323445678",
            "customerEmail": "hieubthe130790@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-21",
            "updatedDate": "2020-11-16",
            "creator": "test1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 21,
            "orderStatusId": "OS003",
            "customerName": "Cao Hoàng",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-21",
            "updatedDate": null,
            "creator": "test1",
            "updater": "hoangch1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 25,
            "orderStatusId": "OS003",
            "customerName": "Nguyen Văn Ba",
            "customerCompanyName": "The 85th District Inc",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0347778888",
            "customerEmail": "company@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": null,
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 26,
            "orderStatusId": "OS003",
            "customerName": "Jonh McKen",
            "customerCompanyName": "Vjp pr0",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0347778888",
            "customerEmail": "company@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": null,
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Processing",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 27,
            "orderStatusId": "OS002",
            "customerName": "Nguyen Văn Ba",
            "customerCompanyName": "The 85th District Inc",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0347778888",
            "customerEmail": "company@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": null,
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Approved",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        },
        {
            "orderId": 28,
            "orderStatusId": "OS004",
            "customerName": "Trần Văn Bốn",
            "customerCompanyName": "The 85th District Inc",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "187 Nguyen Trai, Ba Dinh",
            "customerPhoneNumber": "0989767676",
            "customerEmail": "omegalul@company.com",
            "totalPrice": "101",
            "createdDate": "2020-10-10",
            "updatedDate": "2020-11-16",
            "creator": "hoangch1",
            "updater": "test1",
            "approver": "hoangch1",
            "assignee": "factoryManager",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Completed",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        }
    ],
    "numberOfPages": 7,
    "totalEntries": 66
}

module.exports.viewOrderList_utc007 = {
    "orders": [],
    "numberOfPages": 0,
    "totalEntries": 0
}

// -----------------------------------------
//
//    Sheet:  View order detail
//
// -----------------------------------------
module.exports.viewOrderDetail_utc006 = {
    "order": {
        "orderId": 4,
        "orderStatusId": "OS003",
        "customerName": "CSGO Player Đẹp Trai",
        "customerCompanyName": "123 Company",
        "customerCityProvinceId": 1,
        "customerDistrictId": 1,
        "customerWardCommuneId": 1,
        "customerAddress": "Việt Nam dân chủ cộng hòa",
        "customerPhoneNumber": "0347778875",
        "customerEmail": "hoangchhe130830@fpt.edu.vn",
        "totalPrice": "100",
        "createdDate": "2020-10-14",
        "updatedDate": "2020-11-16",
        "creator": "test1",
        "updater": "test1",
        "approver": "factoryManager",
        "assignee": "factoryManager",
        "isBatchAdded": 0,
        "orderImportBatchId": null,
        "orderStatus": "Processing",
        "customerCityProvince": "Hồ Chí Minh",
        "customerDistrict": "Bình Chánh",
        "customerWardCommune": "An Phú Tây"
    }
}

module.exports.viewOrderDetail_utc007 = {
    "order": {
        "orderId": 4,
        "orderStatusId": "OS003",
        "customerName": "CSGO Player Đẹp Trai",
        "customerCompanyName": "123 Company",
        "customerCityProvinceId": 1,
        "customerDistrictId": 1,
        "customerWardCommuneId": 1,
        "customerAddress": "Việt Nam dân chủ cộng hòa",
        "customerPhoneNumber": "0347778875",
        "customerEmail": "hoangchhe130830@fpt.edu.vn",
        "totalPrice": "100",
        "createdDate": "2020-10-14",
        "updatedDate": "2020-11-16",
        "creator": "test1",
        "updater": "test1",
        "approver": "factoryManager",
        "assignee": "factoryManager",
        "isBatchAdded": 0,
        "orderImportBatchId": null,
        "orderStatus": "Processing",
        "customerCityProvince": "Hồ Chí Minh",
        "customerDistrict": "Bình Chánh",
        "customerWardCommune": "An Phú Tây"
    }
}

module.exports.viewOrderDetail_utc008 = {
    error: ["No order with ID: " + '4' + " to be found."]
}

// -----------------------------------------
//
//    Sheet:  Add new order
//
// -----------------------------------------
module.exports.addNewOder_utc009 = {
    message: "An order was created successfully."
}

// -----------------------------------------
//
//    Sheet:  Modify Order Information
//
// -----------------------------------------
module.exports.modifyOrderInfo_utc009 = {
    message: "Order with ID: " + "30" + " updated."
}

// -----------------------------------------
//
//    Sheet:  Delete rejected orders
//
// -----------------------------------------
module.exports.deleteRejectedOrders_utc008 = {
    message: "Order with IDs: [" + "203, 204" + "], are deleted from SIAS system."
}

// -----------------------------------------
//
//    Sheet:  Search order
//
// -----------------------------------------
module.exports.searchOrder_utc006 = {
    "orders": [],
    "numberOfPages": 0,
    "totalEntries": 0
}

module.exports.searchOrder_utc011 = {
    "orders": [
        {
            "row_num": 1,
            "orderId": 30,
            "orderStatusId": "OS001",
            "customerName": "Cao Hoàng",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "7 Khâm Thiên",
            "customerPhoneNumber": "0341115555",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-10",
            "updatedDate": "2020-12-09",
            "creator": "test1",
            "updater": "test1",
            "approver": "test1",
            "assignee": "hoangch1",
            "isBatchAdded": 1,
            "orderImportBatchId": null,
            "orderStatus": "Pending",
            "customerCityProvince": "Hồ Chí Minh",
            "customerDistrict": "Bình Chánh",
            "customerWardCommune": "An Phú Tây"
        }
    ],
    "numberOfPages": 1,
    "totalEntries": 1
}


// -----------------------------------------
//
//    Sheet:  Modify order status
//
// -----------------------------------------
module.exports.changeOrderStatus_utc011 = {
    message: ["Status of Order #" + 199 + " is changed to " + 'Rejected' + "."]
}

// -----------------------------------------
//
//    Sheet:  Get order item by Order ID
//
// -----------------------------------------
module.exports.getOrderItemByOrderId_utc005 = {
    "orderItems": [
        {
            "orderItemId": 56,
            "orderId": 4,
            "length": "500",
            "bladeWidth": 5,
            "quantity": 1,
            "price": "2000",
            "creator": null,
            "createdDate": null,
            "updater": "test1",
            "updatedDate": "2020-11-03",
            "assignee": "factoryManager"
        },
        {
            "orderItemId": 57,
            "orderId": 4,
            "length": "1200",
            "bladeWidth": 5,
            "quantity": 3,
            "price": "5000",
            "creator": null,
            "createdDate": null,
            "updater": null,
            "updatedDate": null,
            "assignee": "factoryManager"
        },
        {
            "orderItemId": 58,
            "orderId": 4,
            "length": "568",
            "bladeWidth": 5,
            "quantity": 4,
            "price": "5000",
            "creator": null,
            "createdDate": null,
            "updater": null,
            "updatedDate": null,
            "assignee": "factoryManager"
        },
        {
            "orderItemId": 59,
            "orderId": 4,
            "length": "1000",
            "bladeWidth": 5,
            "quantity": 1,
            "price": "5000",
            "creator": null,
            "createdDate": null,
            "updater": null,
            "updatedDate": null,
            "assignee": "factoryManager"
        },
        {
            "orderItemId": 60,
            "orderId": 4,
            "length": "1000",
            "bladeWidth": 5,
            "quantity": 5,
            "price": "5000",
            "creator": null,
            "createdDate": null,
            "updater": null,
            "updatedDate": null,
            "assignee": "factoryManager"
        }
    ],
    "numberOfPages": 1
}

// -----------------------------------------
//
//    Sheet: Add new order item by order ID
//
// -----------------------------------------
module.exports.addNewOrderItem_utc009 = {
    message: "List of order items were created successfully."
}

// -----------------------------------------
//
//    Sheet: Update order item
//
// -----------------------------------------
module.exports.updateOrderItem_utc009 = {
    message: "Order Items with IDs: " + 79 + " updated."
}

// -----------------------------------------
//
//    Sheet: Delete order item
//
// -----------------------------------------
module.exports.deleteOrderItem_utc007 = {
    message: "Order Item with IDs: [" + "80, 81" + "], " +
        "are deleted from SIAS system. (Total: " + 2 + ")"
}

// -----------------------------------------
//
//    Sheet: Create Cutting Instruction
//
// -----------------------------------------
module.exports.createCuttingIns_utc010 = {
    message: "Created cutting instruction for order (ID: " + 85 + ") successfully."
}

// -----------------------------------------
//
//    Sheet: View Cutting Instruction
//
// -----------------------------------------
module.exports.viewCuttingIns_utc008 = {
    "cuttingInstruction": [
        {
            "cuttingInstructionId": 97,
            "orderId": 4,
            "orderItemId": 56,
            "totalQuantity": 1,
            "steelBarNumber": 1,
            "steelLength": 500,
            "stockProductId": 18,
            "stockProductLength": 13956,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 98,
            "orderId": 4,
            "orderItemId": 57,
            "totalQuantity": 3,
            "steelBarNumber": 1,
            "steelLength": 1200,
            "stockProductId": 18,
            "stockProductLength": 12756,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 99,
            "orderId": 4,
            "orderItemId": 57,
            "totalQuantity": 3,
            "steelBarNumber": 2,
            "steelLength": 1200,
            "stockProductId": 18,
            "stockProductLength": 11556,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 100,
            "orderId": 4,
            "orderItemId": 57,
            "totalQuantity": 3,
            "steelBarNumber": 3,
            "steelLength": 1200,
            "stockProductId": 18,
            "stockProductLength": 10356,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 101,
            "orderId": 4,
            "orderItemId": 58,
            "totalQuantity": 4,
            "steelBarNumber": 1,
            "steelLength": 568,
            "stockProductId": 18,
            "stockProductLength": 9788,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 102,
            "orderId": 4,
            "orderItemId": 58,
            "totalQuantity": 4,
            "steelBarNumber": 2,
            "steelLength": 568,
            "stockProductId": 18,
            "stockProductLength": 9220,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 103,
            "orderId": 4,
            "orderItemId": 58,
            "totalQuantity": 4,
            "steelBarNumber": 3,
            "steelLength": 568,
            "stockProductId": 18,
            "stockProductLength": 8652,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 104,
            "orderId": 4,
            "orderItemId": 58,
            "totalQuantity": 4,
            "steelBarNumber": 4,
            "steelLength": 568,
            "stockProductId": 18,
            "stockProductLength": 8084,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 105,
            "orderId": 4,
            "orderItemId": 59,
            "totalQuantity": 1,
            "steelBarNumber": 1,
            "steelLength": 1000,
            "stockProductId": 18,
            "stockProductLength": 7084,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 106,
            "orderId": 4,
            "orderItemId": 60,
            "totalQuantity": 5,
            "steelBarNumber": 1,
            "steelLength": 1000,
            "stockProductId": 18,
            "stockProductLength": 6084,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 107,
            "orderId": 4,
            "orderItemId": 60,
            "totalQuantity": 5,
            "steelBarNumber": 2,
            "steelLength": 1000,
            "stockProductId": 18,
            "stockProductLength": 5084,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 108,
            "orderId": 4,
            "orderItemId": 60,
            "totalQuantity": 5,
            "steelBarNumber": 3,
            "steelLength": 1000,
            "stockProductId": 18,
            "stockProductLength": 4084,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 109,
            "orderId": 4,
            "orderItemId": 60,
            "totalQuantity": 5,
            "steelBarNumber": 4,
            "steelLength": 1000,
            "stockProductId": 18,
            "stockProductLength": 3084,
            "assignee": "factoryManager"
        },
        {
            "cuttingInstructionId": 110,
            "orderId": 4,
            "orderItemId": 60,
            "totalQuantity": 5,
            "steelBarNumber": 5,
            "steelLength": 1000,
            "stockProductId": 18,
            "stockProductLength": 2084,
            "assignee": "factoryManager"
        }
    ],
    "order": [
        {
            "orderId": 4,
            "orderStatusId": "OS003",
            "customerName": "CSGO Player Đẹp Trai",
            "customerCompanyName": "123 Company",
            "customerCityProvinceId": 1,
            "customerDistrictId": 1,
            "customerWardCommuneId": 1,
            "customerAddress": "Việt Nam dân chủ cộng hòa",
            "customerPhoneNumber": "0347778875",
            "customerEmail": "hoangchhe130830@fpt.edu.vn",
            "totalPrice": "100",
            "createdDate": "2020-10-14",
            "updatedDate": "2020-11-16",
            "creator": "test1",
            "updater": "test1",
            "approver": "factoryManager",
            "assignee": "factoryManager",
            "isBatchAdded": 0,
            "orderImportBatchId": null,
            "orderStatus": "Processing"
        }
    ]
}