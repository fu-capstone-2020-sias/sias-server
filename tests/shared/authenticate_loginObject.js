// ROLE: Admin
module.exports.auth_admin = {
    userName: 'test2',
    password: 'hghgE123@456hg'
}

// ROLE: Office Manager
module.exports.auth_officeManager = {
    userName: 'test1',
    password: 'hghgE123@456hg'
}

// ROLE: Factory Manager
module.exports.auth_factoryManager = {
    userName: 'factoryManager',
    password: 'hghgE123@456hg'
}

// ROLE: Office Employee
module.exports.auth_officeEmployee = {
    userName: 'test9',
    password: 'hghgE123@456hg'
}