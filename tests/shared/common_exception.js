module.exports.sessionExpired = {
    error: ["Your session is expired or you are not logged in. Please log in to authenticate yourself."]
}

module.exports.unauthorizedAccess = {
    error: ['401 - Unauthorized Access.']
}

module.exports.reqBodyEmpty = {
    error: ["req.body is EMPTY."]
}