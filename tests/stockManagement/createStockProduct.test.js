const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const sheetName = 'Create StockProduct';
const commonErrors = require('./commonErrors');
const stockProductAccount = require("./stockProductAccount");
const stockProductController = require('../../controllers/stockProductController');
beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        authenticatedSession.post('/stockProduct')
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})

const createStockProductError = require('./createStockProductError')

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        authenticatedSession.post('/stockProduct')
            .send()
            .expect(400, createStockProductError.TC02, done)
    });

    it('TC03', function (done) {
        const dataToInsert = {
            sku: 'kiraqueen',
            barcode: 'kiraqueen',
            type: 'as',
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 'as',
            standard: 'as',
            statusId: 'as',
            manufacturedDate: '2020-01-01',
            warehouseId: 'as'
        }
        authenticatedSession.post('/stockProduct')
            .send(dataToInsert)
            .expect(400, createStockProductError.TC03, done)
    });
    const dataToInsert = {
        sku: 'DHAKA-100',
        barcode: '123456789011',
        type: 1,
        name: 'quangtmhe130060 stockproduct',
        price: 10,
        length: 100,
        stockGroupId: 2,
        standard: 1,
        statusId: 'SPS001',
        manufacturedDate: '2020-01-01',
        warehouseId: 66,
        user: stockProductAccount.role02.userName
    }
    it('TC04', function (done) {
        authenticatedSession.post('/stockProduct')
            .send(dataToInsert)
            .expect(201)
            .end(async (err, response) => {
                if (err) {
                    return done(err);
                }
                delete dataToInsert.user
                for (key in dataToInsert) {
                    const isEqual = dataToInsert[key] === response.body.insertedStockProduct[key];
                    // console.log(`isEqual: ${isEqual} dataToInsert[key] ${dataToInsert[key]} response.body.insertedStockProduct[key]: ${response.body.insertedStockProduct[key]}`)
                    expect(isEqual).toBe(true);
                }
                await stockProductController.deleteStockProduct({ stockProductId: response.body.insertedStockProduct.stockProductId });
                return done()
            })
    });
    it('TC05', function (done) {
        const newData = {
            sku: 'kiraqueen',
            barcode: 'kiraqueen',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: -1,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        authenticatedSession.post('/stockProduct')
            .send(newData)
            .expect(400, createStockProductError.TC05, done)
    });
    it('TC06', function (done) {
        const newData = {
            sku: 'kiraqueen',
            barcode: 'kiraqueen',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: -100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        authenticatedSession.post('/stockProduct')
            .send(newData)
            .expect(400, createStockProductError.TC06, done)
    });
    it('TC07', function (done) {
        const newData = {
            sku: 'kiraqueen',
            barcode: 'kiraqueen',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020x-01x-01x',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        authenticatedSession.post('/stockProduct')
            .send(newData)
            .expect(400, createStockProductError.TC07, done)
    });

})