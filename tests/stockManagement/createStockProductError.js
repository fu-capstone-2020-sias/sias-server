const emptyFieldError = {
    "error": "Invalid field",
    "errorList": [
        "Stock keeping unit (sku) is empty",
        "Stock product barcode is empty",
        "Stock product steel type is empty",
        "Stock product name is empty",
        "Stock product price is empty",
        "Stock product length is empty",
        "Stock group code is empty",
        "Stock standard is empty",
        "Stock status code is empty",
        "Stock product manufacture date is empty",
        "Stock product warehouse code is empty"
    ]
}
const notExistedInDatabaseError = {
    "error": "Invalid field",
    "errorList": [
        "Stock product steel type not existed in database",
        "Stock group code not existed in database",
        "Stock standard not existed in database",
        "Stock status code not existed in database",
        "Stock product warehouse code not existed in database"
    ]
}
const invalidPrice = {
    "error": "Invalid field",
    "errorList": [
        "Stock product price must be < 9999999999 and > 0"
    ]
}
const invalidLength = {
    "error": "Invalid field",
    "errorList": [
        "Stock product length must be < 99999 and > 0"
    ]
}
const invalidDateFormat = {
    "error": "Invalid field",
    "errorList": [
        "Stock product manufacture date is invalid format"
    ]
}
module.exports = {
    TC02: emptyFieldError,
    TC03: notExistedInDatabaseError,
    TC05: invalidPrice,
    TC06: invalidLength,
    TC07: invalidDateFormat 
}