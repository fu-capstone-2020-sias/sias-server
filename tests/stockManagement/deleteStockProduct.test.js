const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const sheetName = 'Delete StockProduct';
const commonErrors = require('./commonErrors');
const stockProductAccount = require("./stockProductAccount");
const stockProductController = require('../../controllers/stockProductController');

beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        authenticatedSession.delete('/stockProduct')
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})
const deleteStockProdutError = require('./deleteStockProductError')
describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        authenticatedSession.delete('/stockProduct')
            .send()
            .expect(400, deleteStockProdutError.TC02, done)
    });

    it('TC03', function (done) {
        const deleteStockProduct = {
            stockProductId: 'as'
        }
        authenticatedSession.delete('/stockProduct')
            .send(deleteStockProduct)
            .expect(400, deleteStockProdutError.TC03, done)
    });

    it('TC04', async function (done) {
        const dataToInsert = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        const insertedData = await stockProductController.insertWarehouse(dataToInsert);

        const deleteStockProduct = {
            stockProductId: insertedData.dataValues.stockProductId
        }
        authenticatedSession.delete('/stockProduct')
            .send(deleteStockProduct)
            .expect(200)
            .end(async (err) => {
                if(err){
                    return done(err)
                }
                else{
                    const findData = await stockProductController.find(insertedData.dataValues.stockProductId)
                    const count = findData.data.count;
                    expect(count).toBe(0)
                    done()
                }
            })
    });
})