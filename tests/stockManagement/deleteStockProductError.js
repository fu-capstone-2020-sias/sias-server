const emptyStockProductCode = {
    "error": "Invalid field",
    "errorList": [
        "Stock product code is empty"
    ]
}
const notExistedInDatabaseError = {
    "error": "Invalid field",
    "errorList": [
        "Stock product code not existed in database"
    ]
}
module.exports = {
    TC02: emptyStockProductCode,
    TC03: notExistedInDatabaseError
}