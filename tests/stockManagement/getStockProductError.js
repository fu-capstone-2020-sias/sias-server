const emptyPageNumber = {
    "error": "Invalid field",
    "errorList": [
        "Page number is empty"
    ]
}

const pageNumberMustBeGreaterThan0 = {
    "error": "Invalid field",
    "errorList": [
        "Page number must be > 0"
    ]
}
const fromPriceToPriceOutOfRange = {
    'error': 'Invalid field',
    'errorList': [
        'From price of stock product must be < 9999999999 and > 0',
        'To price of stock product must be < 9999999999 and > 0'
    ]
}
const fromPriceAndToPriceMustBeANumber = {
    "error": "Invalid field",
    "errorList": [
        "From price of stock product must be a number",
        "To price of stock product must be a number"
    ]
}
const fromLengt2LengthOutOfRange = {
    "error": "Invalid field",
    "errorList": [
        "From length of stock product must be < 99999 and > 0",
        "To length of stock product must be < 99999 and > 0"
    ]
}
const toLengthOutOfRange = {
    "error": "Invalid field",
    "errorList": [
        "To length of stock product must be < 99999 and > 0"
    ]
}
const fromLengthtoLengthMustBeANumber = {
    "error": "Invalid field",
    "errorList": [
        "From length of stock product must be a number",
        "To length of stock product must be a number"
    ]
}
const invalidManufactureDate = {
    "error": "Invalid field",
    "errorList": [
        "Stock product manufacture date is invalid format"
    ]
}
module.exports = {
    TC02: emptyPageNumber,
    TC03: pageNumberMustBeGreaterThan0,
    TC04: pageNumberMustBeGreaterThan0,
    TC18: fromPriceToPriceOutOfRange,
    TC19: fromPriceToPriceOutOfRange,
    TC20: fromPriceAndToPriceMustBeANumber,
    TC21: fromPriceToPriceOutOfRange,
    TC24: toLengthOutOfRange,
    TC25: fromLengt2LengthOutOfRange,
    TC26: fromLengt2LengthOutOfRange,
    TC27: fromLengthtoLengthMustBeANumber,
    TC28: fromLengt2LengthOutOfRange,
    TC36: invalidManufactureDate
}