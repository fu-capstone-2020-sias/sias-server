const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const sheetName = 'View StockProduct List';
const commonErrors = require('./commonErrors');
const stockProductAccount = require("./stockProductAccount");

beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        const testCaseQuery = {
            pageNumber: 1
        }
        authenticatedSession.get('/stockProduct')
            .query(testCaseQuery)
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})

const viewStockProductError = require('./getStockProductError');
const stockProductController = require('../../controllers/stockProductController');
describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        const testCaseQuery = {
            pageNumber: ''
        }
        authenticatedSession.get('/stockProduct')
            .query(testCaseQuery)
            .send()
            .expect(400, viewStockProductError.TC02, done)
    });

    it('TC03', function (done) {
        const testCaseQuery = {
            pageNumber: 0
        }
        authenticatedSession.get('/stockProduct')
            .query(testCaseQuery)
            .send()
            .expect(400, viewStockProductError.TC03, done)
    });

    it('TC04', function (done) {
        const testCaseQuery = {
            pageNumber: -1
        }
        authenticatedSession.get('/stockProduct')
            .query(testCaseQuery)
            .send()
            .expect(400, viewStockProductError.TC04, done)
    });

    it('TC05', function (done) {
        const testCaseQuery = {
            pageNumber: 1
        }
        authenticatedSession.get('/stockProduct')
            .query(testCaseQuery)
            .send()
            .expect(200)
            .end((err, response) => {
                if (err) return done(err);
                done(err)
            })
    });

    it('TC06', function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            stockProductId: 'as'
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });

    it('TC07', async function (done) {
        const insertedStockProduct = {
            "sku": "FU851VN",
            "barcode": "627708031119",
            "type": "1",
            "name": "Quangtm stock product",
            "price": "1",
            "length": "1",
            "stockGroupId": "3",
            "standard": "1",
            "statusId": "SPS001",
            "manufacturedDate": "2020-10-08",
            "warehouseId": "66",
            "user": `${stockProductAccount.role02.userName}`
        }
        const insertedData = await stockProductController.insertWarehouse(insertedStockProduct)
        const newStockProductID = insertedData.dataValues.stockProductId
        const testCaseQuery = {
            pageNumber: 1,
            stockProductId: `${newStockProductID}`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(1)
                done()
            })
    });

    it('TC08', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            sku: `1`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });

    it('TC09', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            sku: `/_ \（；´д｀）ゞ`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });

    it('TC10', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            barcode: `1`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });

    it('TC11', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            barcode: `/_ \（；´д｀）ゞ`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });

    it('TC12', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            name: `a`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });

    it('TC13', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            name: `/_ \（；´д｀）ゞ`
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });

    it('TC14', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 20,
            toPrice: 10
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });

    it('TC15', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 0,
            toPrice: 9e8
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });
    it('TC16', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: '',
            toPrice: 9e9
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThan(0)
                done()
            })
    });
    it('TC17', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 0,
            toPrice: ''
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThan(0)
                done()
            })
    });
    it('TC18', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 90000000001,
            toPrice: 90000000000
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC18, done)
    });
    it('TC19', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: -1,
            toPrice: -1
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC19, done)
    });
    it('TC20', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 'as',
            toPrice: 'as'
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC20, done)
    });
    it('TC21', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 99999999999,
            toPrice: 99999999999
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC21, done)
    });
    it('TC22', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromPrice: 99999999999,
            toPrice: 99999999999
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC21, done)
    });
    it('TC23', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: 10,
            toLength: 0
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });
    it('TC24', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: 0,
            toLength: 1e5
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC24, done)
    });
    it('TC25', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: 9999999999,
            toLength: 9999999999
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC25, done)
    });
    it('TC26', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: -1,
            toLength: -1
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC26, done)
    });
    it('TC27', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: 'as',
            toLength: 'as'
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC27, done)
    });
    it('TC28', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: 9999,
            toLength: 9999
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });
    it('TC29', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            fromLength: 0,
            toLength: 0
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });
    it('TC30', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            type: 'as'
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });
    it('TC31', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            type: 1
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });
    it('TC32', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            statusId: 'as'
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });
    it('TC33', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            statusId: 1
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });

    it('TC34', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            searchByDate: 1,
            fromDate: '2000-01-01',
            toDate: '2000-01-01' 
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBe(0)
                done()
            })
    });
    it('TC35', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            searchByDate: 1,
            fromDate: '2000-01-01',
            toDate: '2020-12-31' 
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(200)
            .end((err, response) => {
                if (err) return done(err);
                const count = response.body.data.totalResult;
                expect(count).toBeGreaterThanOrEqual(0)
                done()
            })
    });

    it('TC36', async function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            searchByDate: 1,
            fromDate: '2000x-01x-01x',
            toDate: '2020x-12x-31x' 
        }
        authenticatedSession.get('/stockProduct').query(testCaseQuery).send().expect(400, viewStockProductError.TC36, done);
    });
})
