const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const sheetName = 'Update StockProduct';
const commonErrors = require('./commonErrors');
const stockProductAccount = require("./stockProductAccount");
const stockProductController = require('../../controllers/stockProductController');

beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        authenticatedSession.put('/stockProduct')
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})
const updateStockProductError = require('./updateStockProductError')
describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        authenticatedSession.put('/stockProduct')
            .send()
            .expect(400, updateStockProductError.TC02, done)
    });

    it('TC03', function (done) {
        const dataToPut = {
            stockProductId: 'as',
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 'as',
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 'as',
            standard: 'as',
            statusId: 'as',
            manufacturedDate: '2020-01-01',
            warehouseId: 'as'
        }
        authenticatedSession.put('/stockProduct')
            .send(dataToPut)
            .expect(400, updateStockProductError.TC03, done)
    });

    it('TC04',async function (done) {
        const dataToInsert = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        const insertedData = await stockProductController.insertWarehouse(dataToInsert);
        const modifiedData = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 modified stockproduct',
            price: 1,
            length: 10,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        modifiedData.stockProductId = insertedData.dataValues.stockProductId
        authenticatedSession.put('/stockProduct')
            .send(modifiedData)
            .expect(200)
            .end(async (err, response) => {
                if (err) {
                    return done(err);
                }
                delete modifiedData.user
                for (key in modifiedData) {
                    const isEqual = modifiedData[key] === response.body.updatedStockProduct[key];
                    console.log(`isEqual: ${isEqual} modifiedData[key] ${modifiedData[key]} response.body.updatedStockProduct[key]: ${response.body.updatedStockProduct[key]}`)
                    // expect(isEqual).toBe(true);
                }
                await stockProductController.deleteStockProduct({ stockProductId: modifiedData.stockProductId });
                // console.log(response.body)
                return done()
            })
    });

    it('TC05',async function (done) {
        const dataToInsert = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        const insertedData = await stockProductController.insertWarehouse(dataToInsert);
        const modifiedData = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 modified stockproduct',
            price: -1,
            length: 10,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        modifiedData.stockProductId = insertedData.dataValues.stockProductId
        authenticatedSession.put('/stockProduct')
            .send(modifiedData)
            .expect(400, updateStockProductError.TC05).end(async function (err, response) {
                if(err){ 
                    return done(err);
                }else{
                    await stockProductController.deleteStockProduct({ stockProductId: modifiedData.stockProductId });
                    return done();
                }
            })
    });

    it('TC06',async function (done) {
        const dataToInsert = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        const insertedData = await stockProductController.insertWarehouse(dataToInsert);
        const modifiedData = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 modified stockproduct',
            price: 100,
            length: -1,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        modifiedData.stockProductId = insertedData.dataValues.stockProductId
        authenticatedSession.put('/stockProduct')
            .send(modifiedData)
            .expect(400, updateStockProductError.TC06).end(async function (err, response) {
                if(err){ 
                    return done(err);
                }else{
                    await stockProductController.deleteStockProduct({ stockProductId: modifiedData.stockProductId });
                    return done();
                }
            })
    });
    it('TC07',async function (done) {
        const dataToInsert = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 stockproduct',
            price: 10,
            length: 100,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020-01-01',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        const insertedData = await stockProductController.insertWarehouse(dataToInsert);
        const modifiedData = {
            sku: 'DHAKA-100',
            barcode: '123456789011',
            type: 1,
            name: 'quangtmhe130060 modified stockproduct',
            price: 100,
            length: 10,
            stockGroupId: 2,
            standard: 1,
            statusId: 'SPS001',
            manufacturedDate: '2020x-01x-01x',
            warehouseId: 66,
            user: stockProductAccount.role02.userName
        }
        modifiedData.stockProductId = insertedData.dataValues.stockProductId
        authenticatedSession.put('/stockProduct')
            .send(modifiedData)
            .expect(400, updateStockProductError.TC07).end(async function (err, response) {
                if(err){ 
                    return done(err);
                }else{
                    await stockProductController.deleteStockProduct({ stockProductId: modifiedData.stockProductId });
                    return done();
                }
            })
    });
})