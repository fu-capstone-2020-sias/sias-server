const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const sheetName = 'View StockProduct Detail';
const commonErrors = require('./commonErrors');
const stockProductAccount = require("./stockProductAccount");
const stockProductController = require('../../controllers/stockProductController');
beforeEach(function () {
    testSession = session(app);
});

const viewStockProductDetail = require('./viewStockProductDetailError');

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(stockProductAccount.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });
    const newStockProduct = {
        "sku": "FU851VN",
        "barcode": "627708031119",
        "type": 1,
        "name": "Quangtm stock product",
        "price": 1,
        "length": 1,
        "stockGroupId": 3,
        "standard": 1,
        "statusId": "SPS001",
        "manufacturedDate": "2020-10-08",
        "warehouseId": 66,
        "user": stockProductAccount.role02.userName
    }
    it('TC01', async function (done) {

        const insertedStockProduct = await stockProductController.insertWarehouse(newStockProduct);
        const newStockProductId = insertedStockProduct.dataValues.stockProductId
        const testCaseQuery = {
            pageNumber: 1
        }
        authenticatedSession.get(`/stockProduct/${newStockProductId}`)
            .query(testCaseQuery)
            .send()
            .expect(200)
            .end(async (err, response) => {
                if (err) {
                    return done(err)
                }
                delete newStockProduct.user
                for (key in newStockProduct) {
                    const isEqual = newStockProduct[key] == response.body.data.rows[0][key];
                    console.log(`KEY ${key}`)
                    console.log(`newStockProduct[key]: ${newStockProduct[key]} response.body.data.rows[0][key]: ${response.body.data.rows[0][key]} isEqual: ${isEqual}`)
                    expect(isEqual).toBe(true);
                }
                await stockProductController.deleteStockProduct({ stockProductId: newStockProductId })
                return done()
            })
    });
    it('TC02', async function (done) {
        const newStockProductId = 'as'
        authenticatedSession.get(`/stockProduct/${newStockProductId}`)
            .send()
            .expect(200)
            .end(async (err, response) => {
                if (err) {
                    return done(err)
                }
                expect(response.body.data.count).toBe(0)
                return done()
            })
    });
    it('TC03', async function (done) {
        const newStockProductId = ''
        authenticatedSession.get(`/stockProduct/${newStockProductId}`)
            .send()
            .expect(400, viewStockProductDetail.TC03, done)
    });
})

describe(sheetName, () => {
    it('TC04', async function (done) {
        const newStockProductId = 'as'
        testSession.get(`/stockProduct/${newStockProductId}`)
            .send()
            .expect(401,viewStockProductDetail.TC04, done )
    }); 
})