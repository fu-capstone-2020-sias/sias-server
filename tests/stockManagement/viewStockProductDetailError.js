const emptyPageNumber = {
    "error": "Invalid field",
    "errorList": [
        "Page number is empty"
    ]
}
const notLoginError = {
    "error": [
        "Your session is expired or you are not logged in. Please log in to authenticate yourself."
    ]
}
module.exports = {
    TC03: emptyPageNumber,
    TC04: notLoginError
}