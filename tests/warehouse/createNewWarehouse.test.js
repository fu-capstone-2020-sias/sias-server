const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const account = require("./warehouseAccount")
const sheetName = 'Create Warehouse List';
const commonErrors = require('./commonError')
const qs = require('qs');
const warehouseController = require('../../controllers/viewWarehouseController')
beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        const testCaseQuery = {
            name: '',
            cityProvinceId: '',
            districtId: '',
            wardCommuneId: '',
            address: '',
            parentDepartmentId: '',
            headOfWarehouse: ''
        }
        authenticatedSession.post('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})

const insertWarehouseErrors = require('./insertWarehouseErrors')
describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        const testCaseQuery = {
            name: '',
            cityProvinceId: '',
            districtId: '',
            wardCommuneId: '',
            address: '',
            parentDepartmentId: '',
            headOfWarehouse: ''
        }
        authenticatedSession.post('/warehouse',
            { body: qs.stringify(testCaseQuery) })
            .send()
            .expect(400, insertWarehouseErrors.TC02, done)
    });

    it('TC03', function (done) {
        const testCaseQuery = {
            name: 'insert warehouse TC03',
            cityProvinceId: 'as',
            districtId: 'as',
            wardCommuneId: 'as',
            address: 'insert warehouse TC03',
            parentDepartmentId: 'as',
            headOfWarehouse: 'as'
        }
        authenticatedSession.post('/warehouse')
            .send(testCaseQuery)
            .expect(400, insertWarehouseErrors.TC03, done)
    });
    
    const validtestCaseQuery = {
        name: 'insert warehouse TC03',
        cityProvinceId: '1',
        districtId: '1',
        wardCommuneId: '1',
        address: 'insert warehouse TC03',
        parentDepartmentId: 'DEPT001',
        headOfWarehouse: 'quangtmr2'
    }
    it('TC03', function (done) {
        authenticatedSession.post('/warehouse')
            .send(validtestCaseQuery)
            .expect(201).end(async (err, response) => {
                if(err) return done(err);
                const data = response.body.newWarehouse;
                for (key in validtestCaseQuery) {
                    expect(validtestCaseQuery[key]).toBe(data[key]);
                }
                await warehouseController.deleteWarehouse({warehouseId: data.warehouseId})
                return done()
            })
    });
})



