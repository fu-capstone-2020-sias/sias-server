const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const account = require("./warehouseAccount")
const sheetName = 'Delete Warehouse';
const commonErrors = require('./commonError')
const warehouseController = require('../../controllers/viewWarehouseController');
const deleteWarehouseError = require('./deleteWarehouseError');
beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        authenticatedSession.delete('/warehouse')
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})


describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        authenticatedSession.delete('/warehouse')
            .send()
            .expect(400, deleteWarehouseError.TC02, done)
    });

    it('TC03', function (done) {
        authenticatedSession.delete('/warehouse')
            .send({ warehouseId: 'as' }).expect(400, deleteWarehouseError.TC03, done)
    });
    const validtestCaseQuery = {
        name: 'insert warehouse TC03',
        cityProvinceId: '1',
        districtId: '1',
        wardCommuneId: '1',
        address: 'insert warehouse TC03',
        parentDepartmentId: 'DEPT001',
        headOfWarehouse: 'quangtmr2'
    }
    it('TC04', async (done) => {
        validtestCaseQuery.user = account.role02.userName
        const result = await warehouseController.insertWarehouse(validtestCaseQuery);
        authenticatedSession.delete('/warehouse')
            .send({ warehouseId: result.dataValues.warehouseId })
            .expect(200, deleteWarehouseError.TC04, done);
    });
})
