const invalidWarehouseCode = {
    "error": "Invalid warehouse code"
}

const notExistedInDatabaseError = {
    "error": "Data not deleted"
}
const deleteCompleted = {
    "deletedWarehouse": 1
}
module.exports = {
    TC02: invalidWarehouseCode,
    TC03: notExistedInDatabaseError,
    TC04: deleteCompleted
}