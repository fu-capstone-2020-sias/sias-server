const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const account = require("./warehouseAccount")
const sheetName = 'View Warehouse List';
const commonErrors = require('./commonError');
const warehouseErrors = require('./getWarehouseListError');
const getWarehouseListError = require("./getWarehouseListError");
beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(200)
            .end((error, response) => {
                if (error) return done(error);
                expect(response.body.data.count).toBeGreaterThanOrEqual(0);
                return done();
            })

    });

    it('TC02', function (done) {
        const testCaseQuery = {
            pageNumber: 0,
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(400, getWarehouseListError.TC02, done);
    });

    it('TC03', function (done) {
        const testCaseQuery = {
            pageNumber: 'as',
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(400, getWarehouseListError.TC03, done);
    });

    it('TC04', function (done) {
        const testCaseQuery = {
            pageNumber: '',
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(400, getWarehouseListError.TC04, done);
    });

    it('TC05', function (done) {
        const testCaseQuery = {
            pageNumber: '',
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(400, getWarehouseListError.TC04, done);
    });

    it('TC06', function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            itemsPerPage: 0
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(200)
            .end((error, response) => {
                if (error) return done(error);
                expect(response.body.data.count).toBeGreaterThanOrEqual(0);
                expect(response.body.itemsPerPage).toBe(10);
                return done();
            })
    });
    it('TC07', function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            itemsPerPage: 'as'
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(200)
            .end((error, response) => {
                if (error) return done(error);
                expect(response.body.data.count).toBeGreaterThanOrEqual(0);
                expect(response.body.itemsPerPage).toBe(10);
                return done();
            })
    });
    it('TC08', function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(200)
            .end((error, response) => {
                if (error) return done(error);
                expect(response.body.data.count).toBeGreaterThanOrEqual(0);
                expect(response.body.itemsPerPage).toBe(10);
                return done();
            })
    });
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });
    it('TC09', function (done) {
        const testCaseQuery = {
            pageNumber: 1,
            itemsPerPage: ''
        }
        authenticatedSession.get('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });
})



