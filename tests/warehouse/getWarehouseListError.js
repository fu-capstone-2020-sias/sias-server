const pageNumberMustBeGreaterThan0 = {
    "error": "Invalid field",
    "errorList": [
        "Page number must be > 0"
    ]
}
const pageNumberMustBeNumber = {
    "error": "Invalid field",
    "errorList": [
        "Page number must be a number"
    ]
}
const pageNumberIsEmpty ={
    "error": "Invalid field",
    "errorList": [
        "Page number is empty"
    ]
}

module.exports = {
    TC02: pageNumberMustBeGreaterThan0,
    TC03: pageNumberMustBeNumber,
    TC04: pageNumberIsEmpty
}