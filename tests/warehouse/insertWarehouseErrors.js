const emptyField = {
    "error": "Invalid field",
    "errorList": [
        "Warehouse name is empty",
        "Warehouse City/Provincy code is empty",
        "Warehouse district code is empty",
        "Warehouse ward commune code is empty",
        "Warehouse parent Department code is empty",
        "Warehouse supervisor is empty"
    ]
}
const notExistedError = {
    "error": "Invalid field",
    "errorList": [
        "Warehouse City/Provincy code not existed in database",
        "Warehouse district code not existed in database",
        "Warehouse ward commune code not existed in database",
        "Warehouse parent Department code not existed in database",
        "Warehouse supervisor not existed in database"
    ]
}
module.exports = {
    TC02: emptyField,
    TC03: notExistedError,
}