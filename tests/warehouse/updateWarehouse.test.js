const app = require("../../app");
const session = require("supertest-session");
let testSession = null;
const { test, expect } = require("@jest/globals");
const account = require("./warehouseAccount")
const sheetName = 'Update Warehouse List';
const commonErrors = require('./commonError')
const qs = require('qs');
const warehouseController = require('../../controllers/viewWarehouseController');
const updateWarehouseError = require('./updateWarehouseError');
beforeEach(function () {
    testSession = session(app);
});

describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role01)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC01', function (done) {
        const testCaseQuery = {
            name: '',
            cityProvinceId: '',
            districtId: '',
            wardCommuneId: '',
            address: '',
            parentDepartmentId: '',
            headOfWarehouse: ''
        }
        authenticatedSession.put('/warehouse')
            .query(testCaseQuery)
            .send()
            .expect(401, commonErrors.invalidRole, done)
    });

})


describe(sheetName, () => {
    var authenticatedSession = null;

    beforeEach(function (done) {
        testSession.post('/auth/login')
            .send(account.role02)
            .expect(200)
            .end(function (err) {
                if (err) return done(err);
                authenticatedSession = testSession;
                return done();
            });
    });

    it('TC02', function (done) {
        const testCaseQuery = {
            warehouseId: '',
            name: '',
            cityProvinceId: '',
            districtId: '',
            wardCommuneId: '',
            address: '',
            parentDepartmentId: '',
            headOfWarehouse: ''
        }
        authenticatedSession.put('/warehouse')
            .send(testCaseQuery)
            .expect(400, updateWarehouseError.TC02, done)
    });

    it('TC03', function (done) {
        const testCaseQuery = {
            warehouseId: 'sd',
            name: 'sd',
            cityProvinceId: 'sd',
            districtId: 'sd',
            wardCommuneId: 'sd',
            address: 'sd',
            parentDepartmentId: 'sd',
            headOfWarehouse: 'sd'
        }
        authenticatedSession.put('/warehouse')
            .send(testCaseQuery)
            .expect(400, updateWarehouseError.TC03, done)
    });
    const validtestCaseQuery = {
        name: 'insert warehouse TC03',
        cityProvinceId: '1',
        districtId: '1',
        wardCommuneId: '1',
        address: 'insert warehouse TC03',
        parentDepartmentId: 'DEPT001',
        headOfWarehouse: 'quangtmr2'
    }
    it('TC04', async function (done) {
        const testCaseQuery = {
            name: 'inserted warehouse TC03',
            cityProvinceId: '1',
            districtId: '1',
            wardCommuneId: '1',
            address: 'inserted warehouse TC03',
            parentDepartmentId: 'DEPT001',
            headOfWarehouse: 'quangtmr2'
        }
        validtestCaseQuery.user = account.role02.userName
        const result = await warehouseController.insertWarehouse(validtestCaseQuery);
        console.log(result)
        testCaseQuery.warehouseId = result.dataValues.warehouseId
        authenticatedSession.put('/warehouse')
            .send(testCaseQuery)
            .expect(200)
            .end(async (err, response) => {
                if (err) { return done(err) }
                const data = response.body.updatedWarehouse
                for (key in testCaseQuery) {
                    const isEqual = testCaseQuery[key] == data[key] ? true : false
                    expect(isEqual).toBe(true)
                }
                await warehouseController.deleteWarehouse({ warehouseId: testCaseQuery.warehouseId })
                return done()
            })
    });
})
