var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var accountController = require("../controllers/accountController");
var passwordUtils = require("../utils/passwordUtils");

passport.use("local",
    new LocalStrategy(
        {
            usernameField: "userName", // pass userName to callback below
            passwordField: "password", // pass password to callback below
            passReqToCallback: true,
        },
        async function (req, userName, password, done) {
            var account = await accountController.getAccountByUsername(req, null);

            if (!account) {
                return done(null, false, {
                    error: ["Account with userName: " + userName + " NOT existed."]
                });
            } else {
                if (account.disableFlag === 1) {
                    return done(null, false, {
                        error: ["Account with userName: " + userName + " is DISABLED. Please contact Administrator for more information."]
                    });
                }
            }

            const result = await passwordUtils.isValidPassword(password, account.hashedPassword);
            if (!result) {
                return done(null, false, {error: ["Incorrect password. Please enter again."]});
            }

            delete account.hashedPassword; // Hide salt + hash to client (less risk)

            return done(null, account);
        }
    )
);

passport.serializeUser(function (user, done) {
    done(null, user);
}); // assign user to store in session

passport.deserializeUser(function (user, done) {
    done(null, user);
}); // assign user to request object

module.exports.isAuthenticated = (req, res, next) => {
    // Check if user has valid session
    if (req.isAuthenticated()) return next();

    res.statusCode = 401;
    let temp = [];
    temp.push("Your session is expired or you are not logged in. Please log in to authenticate yourself.")
    res.json({
        error: temp
    })
};