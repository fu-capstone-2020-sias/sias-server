const { response } = require('express');
const { isDate } = require('moment');
const validateDate = require('validate-date')
module.exports = {

    handleError(res, e, message = 'Something when wrong', statusCode = 500) {
        console.log(e)
        res.status(e.statusCode || statusCode);
        res.send({
            error: e.message || message,
            errorList: e.errorList
        })
    },

    checkNullUndefinedEmpty(input, alias) {
        let errors = []
        for (key in input) {
            const a = key;
            const b = input[key];
            if (!input[key]) {
                const field = alias[key];
                errors.push(`${alias[key].eng} is empty`)
            }
        }
        return errors
    },

    checkNumber(input, alias) {
        let errors = []
        for (key in input) {
            if (isNaN(input[key])) {
                errors.push(`${alias[key].eng} must be a number`)
            }
        }
        return errors
    },
    checkNumberInRange(input, alias, range) {
        let errors = []
        const { lower, upper } = range
        for (key in input) {
            if (input[key] > upper || input[key] < lower) {
                errors.push(`${alias[key].eng} must be < ${upper} and > ${lower}`)
            }
        }
        return errors
    },

    validateDateFormat(inputDate, expectedFormat) {
        let res = false
        try{
            res = validateDate(inputDate, responseType = "boolean", dateFormat = expectedFormat);
        }catch(e){
            console.log(e);
        }finally{
            return res;
        }
    },
    dateIsInTheFuture(inputDate){
        const currentUnix = new Date().getTime();
        const inputInUnix = new Date(inputDate).getTime();
        if(inputInUnix < currentUnix){  // date not in the future
            return false
        }else { // date in the future
            return true
        }
    },
    validateNumberList(input){
        const errors = []
        input.forEach((el) => {
            try{
                if(errors.length > 0){
                    return res
                }
                if(parseInt(el) < 0){

                }
            }catch(e){
                errors.push('Invalid')
            }
        });
        return errors
    }
}