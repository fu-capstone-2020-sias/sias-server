const fs = require('fs');
const db = require("../db");
const csval = require("csval");
const detectNewline = require('detect-newline');
const Sequelize = require("sequelize");
const Account = require("../models/account")(db, Sequelize.DataTypes);
const CityProvince = require("../models/cityprovince")(db, Sequelize.DataTypes);
const District = require("../models/district")(db, Sequelize.DataTypes);
const WardCommune = require("../models/wardcommune")(db, Sequelize.DataTypes);
const chardet = require('chardet');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

module.exports.csvWriterForOrder = async (pathCSV, data) => {
    const csvWriterForOrder = createCsvWriter({
        path: pathCSV,
        recordDelimiter: "\r\n",
        header: [
            {id: 'orderId', title: 'Order ID'},
            {id: 'orderStatus', title: 'Order Status'},
            {id: 'customerName', title: 'Customer Name'},
            {id: 'customerCompanyName', title: 'Customer Company Name'},
            {id: 'customerCityProvince', title: 'Customer City/Province'},
            {id: 'customerDistrict', title: 'Customer District'},
            {id: 'customerWardCommune', title: 'Customer Ward/Commune'},
            {id: 'customerAddress', title: 'Customer Address'},
            {id: 'customerPhoneNumber', title: 'Customer Phone Number'},
            {id: 'customerEmail', title: 'Customer Email'},
            {id: 'totalPrice', title: 'Total Price'},
            {id: 'createdDate', title: 'Created Date'},
            {id: 'updatedDate', title: 'Updated Date'},
            {id: 'creator', title: 'Creator'},
            {id: 'updater', title: 'Updater'},
            {id: 'approver', title: 'Approver'},
            {id: 'assignee', title: 'Assignee'},
            {id: 'isBatchAdded', title: 'Is Batch Added?'},
            {id: 'orderImportBatchId', title: 'Order Import Batch ID'},
            {id: 'orderItemId', title: 'Order Item ID'},
            {id: 'length', title: 'Length'},
            {id: 'quantity', title: 'Quantity'},
            {id: 'price', title: 'Price'},
        ]
    });

    return await csvWriterForOrder.writeRecords(data);

}

module.exports.readCsvFile = (inputPath) => {
    let buffer = fs.readFileSync(inputPath);
    let encoding = chardet.detect(buffer);

    let stringErrorResult = "";
    if (encoding !== "UTF-8") {
        stringErrorResult += "Error\nYour CSV file is not encoded by UTF-8 format.\n"
    }

    // not CRLF
    let strUTF8 = buffer.toString("utf8");

    if (detectNewline(strUTF8) !== "\r\n") {
        stringErrorResult += "Your CSV file is not in CRLF mode.\n"
    }

    if (stringErrorResult !== "") {
        return stringErrorResult;
    }

    return buffer.toString('utf8');
}

module.exports.validateCsvFileByRules = async (csvString, rules) => {
    const parsed = await csval.parseCsv(csvString);
    let messageObj = [];

    if (parsed.data.length > 50) {
        messageObj.push("Only 50 records in CSV file are allowed to import by batch");
        return {
            status: false,
            errorMessage: messageObj
        }
    }

    let j = 2;
    for (let i = 0; i < parsed.data.length; i++) {

        // const date = new Date();
        // const todayDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        //     .toISOString()
        //     .split("T")[0];
        //
        // if (parsed.data[i].createdDate !== todayDate) {
        //     messageObj.push("Row " + (j) + ": " + "Created Date must be today's date. Today is " + todayDate + ".");
        // }

        // // check creator
        // const creator = await Account.findByPk(parsed.data[i].creator);
        //
        //
        // if (creator === null) {
        //     messageObj.push("Row " + (j) + ": " + "Creator [" + parsed.data[i].creator + "] is not existed! (Creator is Account ID of an employee).")
        // }

        // check approver
        const approver = await Account.findAll({
            where: {
                userName: parsed.data[i].approver,
                roleId: "R002"
            }
        });

        if (approver.length === 0) {
            messageObj.push("Row " + (j) + ": " + "Approver [" + parsed.data[i].approver + "] is not existed! (Approver is Account ID of an employee).")
        }

        // check assignee
        const assignee = await Account.findAll({
            where: {
                userName: parsed.data[i].assignee,
                roleId: "R003"
            }
        });

        if (assignee.length === 0) {
            messageObj.push("Row " + (j) + ": " + "Assignee [" + parsed.data[i].assignee + "] is not existed! (Assignee is Account ID of an employee).")
        }

        const customerCityProvinceId = await CityProvince.findByPk(parsed.data[i].customerCityProvinceId);

        if (customerCityProvinceId === null) {
            messageObj.push("Row " + (j) + ": " + "Customer City/Province [" + parsed.data[i].customerCityProvinceId + "] is not existed!")
        }

        const customerDistrictId = await District.findByPk(parsed.data[i].customerDistrictId);
        if (customerDistrictId === null) {
            messageObj.push("Row " + (j) + ": " + "Customer District [" + parsed.data[i].customerDistrictId + "] is not existed!")
        }

        const customerWardCommuneId = await WardCommune.findByPk(parsed.data[i].customerWardCommuneId);

        if (customerWardCommuneId === null) {
            messageObj.push("Row " + (j) + ": " + "Customer Ward/Commune [" + parsed.data[i].customerWardCommuneId + "] is not existed!")
        }

        j++;
    }

    try {
        const valid = await csval.validate(parsed, rules);
        if (messageObj.length === 0 && valid) {
            return {
                status: true,
                data: parsed
            }
        }
    } catch (err) {
        console.log("ERR CSV");
        console.log(err);

        let errMessageArr = err.message.split("\n");

        // Split error message by /r/n
        // For each mỗi string, lấy ra row number, field name
        // push message đó vào obj
        for (let i = 0; i < errMessageArr.length; i++) {
            if (i === 0) {
                continue;
            }

            const rowNumber = errMessageArr[i].substring(4, 7)
                .replace(":", "")
                .replace(" ", "");

            // Lấy field tên là gì. e.g: "orderStatusId", để ý hai dấu nháy
            let indexFirstDoubleQuote = errMessageArr[i].indexOf('"');
            let indexLastDoubleQuote = errMessageArr[i].indexOf('"', indexFirstDoubleQuote + 1);
            let fieldName = errMessageArr[i]
                .substring(indexFirstDoubleQuote + 1, indexLastDoubleQuote)
                .replace(":", "")
                .replace(" ", "");

            if (fieldName === "orderStatusId") {
                let temp = "Row " + rowNumber + ": " + "Order Status ID must be OS001 (Pending).";
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Order Status ID must be OS001 (Pending).");
                continue;
            }
            if (fieldName === "customerName") {
                let temp = "Row " + rowNumber + ": " + "Customer Name must be alphabetic (letter from A-Z case-insensitive)."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer Name must be alphabetic (letter from A-Z case-insensitive).");
                continue;
            }
            if (fieldName === "customerCompanyName") {
                let temp = "Row " + rowNumber + ": " + "Customer Company Name must contain only numbers and words (e.g: The 95th Company)."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer Company Name must contain only numbers and words (e.g: The 95th Company).");
                continue;
            }
            if (fieldName === "customerCityProvinceId") {
                let temp = "Row " + rowNumber + ": " + "Customer City/Province ID must be a number."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer City/Province ID must be a number.");
                continue;
            }
            if (fieldName === "customerDistrictId") {
                let temp = "Row " + rowNumber + ": " + "Customer District ID must be must be a number."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer District ID must be must be a number.");
                continue;
            }
            if (fieldName === "customerWardCommuneId") {
                let temp = "Row " + rowNumber + ": " + "Customer Ward/Commune ID must be must be a number."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer Ward/Commune ID must be must be a number.");
                continue;
            }
            if (fieldName === "customerAddress") {
                let temp = "Row " + rowNumber + ": " + "Customer Address must be not empty or invalid."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer Address must be not empty or invalid.");
                continue;
            }
            if (fieldName === "customerPhoneNumber") {
                let temp = "Row " + rowNumber + ": " + "Customer Phone Number must be a Vietnamese Phone Number (e.g: 034 777 8432)."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer Phone Number must be a Vietnamese Phone Number (e.g: 034 777 8432).");
                continue;
            }
            if (fieldName === "customerEmail") {
                let temp = "Row " + rowNumber + ": " + "Customer Email must be in valid email standard format (e.g: jonh@company.com)."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Customer Email must be in valid email standard format (e.g: jonh@company.com).");
                continue;
            }
            if (fieldName === "totalPrice") {
                let temp = "Row " + rowNumber + ": " + "An order which is added by batch must have Total Price a value of '0'."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "An order which is added by batch must have Total Price a value of '0'.");
                continue;
            }
            if (fieldName === "createdDate") {
                let temp = "Row " + rowNumber + ": " + "Created Date must be in right date format (e.g: 2020-10-10)."
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Created Date must be in right date format (e.g: 2020-10-10).");
                continue;
            }
            if (fieldName === "creator") {
                let temp = "Row " + rowNumber + ": " + "Creator must be not empty."
                let temp2 = "Row " + rowNumber + ": " + "Creator [" + parsed.data[rowNumber - 2].creator + "] is not existed! (Creator is Account ID of an employee)."
                if (messageObj.indexOf(temp) === -1 && messageObj.indexOf(temp2) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Creator must be not empty.");
                continue;
            }
            if (fieldName === "approver") {
                let temp = "Row " + rowNumber + ": " + "Approver must be not empty."
                let temp2 = "Row " + rowNumber + ": " + "Approver [" + parsed.data[rowNumber - 2].approver + "] is not existed! (Approver is Account ID of an employee)."
                if (messageObj.indexOf(temp) === -1 && messageObj.indexOf(temp2) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Approver must be not empty.");
                continue;
            }
            if (fieldName === "assignee") {
                let temp = "Row " + rowNumber + ": " + "Assignee must be not empty."
                let temp2 = "Row " + rowNumber + ": " + "Assignee [" + parsed.data[rowNumber - 2].assignee + "] is not existed! (Assignee is Account ID of an employee)."
                if (messageObj.indexOf(temp) === -1 && messageObj.indexOf(temp2) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Assignee must be not empty.");
                continue;
            }
            if (fieldName === "isBatchAdded") {
                let temp = "Row " + rowNumber + ": " + "Is Batch Added must be a value of '1'"
                if (messageObj.indexOf(temp) === -1)
                    messageObj.push("Row " + rowNumber + ": " + "Is Batch Added must be a value of '1'");
            }

        }
    }

    return {
        status: false,
        errorMessage: messageObj
    }
}

module.exports.orderCsvRules = {
    properties: {
        orderStatusId: {
            type: "string",
            pattern: "^OS001$"
        },
        customerName: {
            type: "string",
            pattern: "^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z ]+$"
        },
        customerCompanyName: {
            type: "string",
            pattern: "^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9]+$"
        },
        customerCityProvinceId: {
            type: "string",
            pattern: "^[0-9]+$"
        },
        customerDistrictId: {
            type: "string",
            pattern: "^[0-9]+$"
        },
        customerWardCommuneId: {
            type: "string",
            pattern: "^[0-9]+$"
        },
        customerAddress: {
            type: "string",
            minLength: "15",
            maxLength: "255",
            pattern: "^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9,.]+$"
        },
        customerPhoneNumber: {
            type: "string",
            pattern: "^((09|03|07|08|05)+([0-9]{8})\\b)$"
        },
        customerEmail: {
            type: "string",
            pattern: "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"
        },
        totalPrice: {
            type: "number",
            minimum: 0,
            exclusiveMaximum: 1
        },
        // createdDate: {
        //     type: "string",
        //     pattern: "^\\d{4}-\\d{2}-\\d{2}$"
        // },
        // creator: {
        //     type: "string",
        //     minLength: 4,
        //     maxLength: 20
        // },
        approver: {
            type: "string",
            minLength: 4,
            maxLength: 20
        },
        assignee: {
            type: "string",
            minLength: 4,
            maxLength: 20
        },
        isBatchAdded: {
            type: "string",
            pattern: "^1$"
        },
    },
    required: [
        "orderStatusId",
        "customerName",
        "customerCompanyName",
        "customerCityProvinceId",
        "customerDistrictId",
        "customerWardCommuneId",
        "customerAddress",
        "customerPhoneNumber",
        "customerEmail",
        "totalPrice",
        // "createdDate",
        // "creator",
        "approver",
        "assignee",
        "isBatchAdded"
    ]
};