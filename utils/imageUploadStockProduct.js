const multer = require('multer');
const path = require('path');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads');
    },
    filename: (req, file, cb) => {
        console.log(file);
        // const fileName = Date.now() + "_" + file.originalname + "_" + req.user.userName;
        //const fileName = file.originalname
        const fileName = new Date().toISOString().replace(/[\/\\:]/g, "_") + file.originalname
        req.params.fileNameAndUploader = fileName + "," + req.user.userName;
        req.file = file;
        cb(null, fileName);
    }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}
module.exports.multerUploadImageStockProduct = multer({storage: storage, fileFilter: fileFilter});        