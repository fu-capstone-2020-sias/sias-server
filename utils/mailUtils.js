const sgMail = require('@sendgrid/mail')
const {SEND_GRID_API_KEY} = require("../config");
sgMail.setApiKey(SEND_GRID_API_KEY);

module.exports.sendMail = async (password, recipient, ip_address) => {
    const msg = {
        to: recipient, // Change to your recipient
        from: 'hoangchhe130830@fpt.edu.vn', // Change to your verified sender
        subject: 'SIAS: Password Reset',
        text: 'You has requested to reset your password.\nTime: ' + new Date().toLocaleString() +
            ".\nIP Address: " + ip_address + "\nHere is your new password: " + password + "\nYou can change it yourself later."
    }
    sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent')
        })
        .catch((error) => {
            throw error;
        })
}
