const pagingAlias = require('../constants/pagingAlias')
const {checkNullUndefinedEmpty, checkNumber} = require('./commonUtils')

module.exports = {
    async validateNotNullPageNumber(input){
        const errors = checkNullUndefinedEmpty(input , pagingAlias);
        if (errors.length > 0){
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async validateNumberPageNumber(input){
        const errors = checkNumber(input, pagingAlias);
        if (errors.length > 0){
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
        if(parseInt(input.pageNumber) <= 0 ){
            errors.push(`${pagingAlias['pageNumber'].eng} must be > 0`)
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    }
}