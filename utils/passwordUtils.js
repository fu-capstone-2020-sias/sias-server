const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports.hashBcryptPassword = async (password) => {
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    return hashedPassword;
}

module.exports.isValidPassword = async (password, hashedPassword) => {
    const isValid = await bcrypt.compare(password, hashedPassword);
    return isValid;
}

module.exports.validatePasswordRequirements = (password) => {
    // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character (@$!%*?&)
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(password);
}

