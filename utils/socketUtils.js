const net = require("net")
const {PromiseSocket} = require("promise-socket")
const {SOCKET_SERVER} = require("../constants/constants");
var dataResult;

module.exports.sendRequestToSocketServer = async (req, res) => {
    const socket = new net.Socket()
    const promiseSocket = new PromiseSocket(socket);
    try {
        await promiseSocket.connect({port: SOCKET_SERVER.PORT, host: SOCKET_SERVER.DOMAIN_LOCAL});

        await promiseSocket.write(req.body.finalString);

        dataResult = await promiseSocket.readAll();

        dataResult = dataResult.toString();

        if (dataResult === "0") {
            res.statusCode = 500;
            res.json({
                error: "ERROR: Invalid input for ARN module."
            })
            return;
        }

        await promiseSocket.destroy();

        dataResult = dataResult.substring(6).replace(/\|/g, "");

        console.log("Data Result from Socket Server");
        console.log(dataResult)

        return dataResult;
    } catch (err) {
        res.statusCode = 500;
        res.json({
            message: err
        })
    }

}