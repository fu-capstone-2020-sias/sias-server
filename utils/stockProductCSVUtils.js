const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const stockProductAlias = require('../constants/stockProductAlias');
const fs = require('fs');
const detectNewline = require('detect-newline');
const StockProductImportModel = require('../constants/stockProductImportModel.js');
const StockProductCSVParser = require('../parser/stockProductCSVParser.js');
const stockProductUtils = require('../utils/stockProductUtils.js');
const commonUtils = require('../utils/commonUtils.js');
const { DATE_FORMAT_STOCKPRODUCT } = require('../constants/constants');
// const stockProductController = require('../controllers/stockProductController.js');
// const stockProductController = require('../controllers/stockProductController.js');
module.exports = {
    async writeCsvForStock(data, pathCSV) {
        const header = []
        const templateAlias = { ...stockProductAlias }

        delete templateAlias.fromPrice
        delete templateAlias.toPrice
        delete templateAlias.fromLength
        delete templateAlias.toLength
        for (field in templateAlias) {
            header.push({
                id: field,
                title: templateAlias[field].eng
            })
        }
        const CSVforStock = createCsvWriter({
            path: pathCSV,
            recordDelimiter: "\r\n",
            header
        })
        const records = await CSVforStock.writeRecords(data);
        return records
    },
    async readCsvFile(inputPath) {
        try{
            let buffer = fs.readFileSync(inputPath);
            let strUTF8 = buffer.toString("utf8");

            if (detectNewline(strUTF8) !== "\r\n") {
                const a = detectNewline(strUTF8)
                const invalidFieldError = Error('Invalid file new line standard')
                invalidFieldError.statusCode = 400
                invalidFieldError.errorList = ['Invalid file new line standard']
                throw invalidFieldError
            }
            return buffer.toString('utf8');
        }catch(err) {
            const invalidFieldError = Error('Invalid File')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = ['File data must be csv file']
            throw invalidFieldError
        }
    },
    async getTemplateCSVforStock(pathCSV) {
        const header = []
        const templateAlias = { ...stockProductAlias }

        delete templateAlias.stockProductId
        delete templateAlias.updatedDate
        delete templateAlias.updater
        delete templateAlias.stockProductImportBatchId
        delete templateAlias.fromPrice
        delete templateAlias.toPrice
        delete templateAlias.fromLength
        delete templateAlias.toLength

        delete templateAlias.creator;
        delete templateAlias.isBatchAdded;
        delete templateAlias.createdDate;

        for (field in templateAlias) {
            header.push({
                id: field,
                title: templateAlias[field].eng
            })
        }
        const CSVforStock = createCsvWriter({
            path: pathCSV,
            recordDelimiter: "\r\n",
            header
        })
        const stockProductController = require('../controllers/stockProductController');
        const rawfirstResultInDB = await stockProductController.getFirstRow();
        const firstResultInDB = rawfirstResultInDB.data.rows[0];
        const records = await CSVforStock.writeRecords([firstResultInDB]);
        return records
    },
    async checkHeader({ header, path }) {
        const cols = header.split(",");
        const errors = [];
        StockProductImportModel.forEach((standardCol, index) => {
            if (standardCol.eng === cols[index]) {

            } else {
                errors.push(`File Header ${standardCol.eng}is missing`);
            }
        })
        if (errors.length > 0) {
            fs.unlinkSync(path)
            const invalidFieldError = Error('Invalid file header')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkData({ data, path }) {
        const parsedStockProduct = await StockProductCSVParser.parseLineToStockProduct(data);
        const errorsList = [];
        if (data.length === 0) {
            fs.unlinkSync(path)
            const invalidFieldError = Error('Invalid data')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = ['File data is empty']
            throw invalidFieldError
        }
        for (let i = 0; i < parsedStockProduct.length; i++) {
            const el = parsedStockProduct[i];
            const index = i + 1
            const thisLineErrors = {}
            const { sku, barcode, type,
                name, price, length, stockGroupId,
                standard, statusId, manufacturedDate, warehouseId } = el;
            const emptyErrors = commonUtils.checkNullUndefinedEmpty({
                sku, barcode,
                name, price, length,
                manufacturedDate
            }, stockProductAlias);

            if (emptyErrors.length > 0) {
                thisLineErrors['emptyField'] = emptyErrors;
            }

            const notExistedErrors = await stockProductUtils.checkExistDatabaseForImport({
                type, stockGroupId, standard, statusId, warehouseId
            });

            if (notExistedErrors.length > 0) {
                thisLineErrors['notExistedField'] = notExistedErrors;
            }
            const notValidNumbersErrors = commonUtils.checkNumber({
                price, length
            }, stockProductAlias);

            if (notValidNumbersErrors.length > 0) {
                thisLineErrors['notValidNumber'] = notValidNumbersErrors;
            }

            const invalidNumberRangeErrors = commonUtils.checkNumberInRange({ price, length },
                stockProductAlias, { lower: 0, upper: Number.MAX_VALUE });
            const a = 1
            if (invalidNumberRangeErrors.length > 0) {
                thisLineErrors['numberNotInRange'] = invalidNumberRangeErrors;
            }

            const validDateFormatError = await commonUtils.validateDateFormat(manufacturedDate, DATE_FORMAT_STOCKPRODUCT);

            if (!validDateFormatError) {
                thisLineErrors['invalidDateFormat'] = 'Invalid manufacture date format'
            }else{
                if(commonUtils.dateIsInTheFuture(manufacturedDate)){
                    thisLineErrors['invalidDateFormat'] = 'Manufacture date cannot be in future'
                }
            }

            const invalidSKUError = stockProductUtils.checkSKUForImport(sku);
            if(invalidSKUError.length >0 ) {
                thisLineErrors['invalidSKU'] = 'SKU can only contains character or digit'
            }

            const invalidName = stockProductUtils.checkStockProductNameForImport(name);
            if(invalidName.length >0 ) {
                thisLineErrors['invalidName'] = 'Name can only contains character or digit'
            }

            const invalidBarcode = stockProductUtils.checkBarcodeForImport(barcode);
            if(invalidBarcode.length >0 ) {
                thisLineErrors['invalidBarcode'] = 'Barcode can only contains digit and length equal 12'
            }

            if (Object.keys(thisLineErrors).length !== 0) {
                thisLineErrors['row'] = index + 1;
                errorsList.push(thisLineErrors)
            }
        }
        if (errorsList.length > 0) {
            fs.unlinkSync(path)
            const invalidFieldError = Error('Invalid data')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errorsList
            throw invalidFieldError
        }
        return parsedStockProduct;
    }
}