const { checkNullUndefinedEmpty, 
    checkNumberInRange, 
    checkNumber, 
    validateDateFormat, 
    dateIsInTheFuture
} = require('./commonUtils');
const stockProductAlias = require('../constants/stockProductAlias');
const steelTypeController = require('../controllers/steelTypeController');
const stockGroupController = require('../controllers/stockGroupController');
const stockProductStandardController = require('../controllers/stockProductStandardController');
const stockProductStatusController = require('../controllers/stockProductStatusController');
const warehouseController = require('../controllers/viewWarehouseController')
const { NOT_EXIST_IN_DATABASE, DATE_FORMAT_STOCKPRODUCT } = require('../constants/constants');

const validateSKU = (inputString) => {
    const regex = /^[A-Za-z0-9-*]{1,20}$/
    const res = inputString.match(regex);
    return res;
}

const validateBarcode = (input) => {
    const isLenEq12 = input.length === 12;
    const isDigit = /^[0-9]*$/.test(input);
    return isLenEq12 && isDigit;
}

const validateStockProductName = (input) => {
    const regex =  /^[\u00C0-\u1FFF\u2C00-\uD7FFA-Za-z 0-9&-/,.]+$/
    const res = regex.test(input)
    return res
}
module.exports = {
    checkBarcode(inputBarcode) {
        if (!validateBarcode(inputBarcode)) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = ['Barcode must be contains 12 digits']
            throw invalidFieldError
        }
    },
    checkBarcodeForImport(inputBarcode) {
        const isValidBarcode = validateBarcode(inputBarcode)
        if (isValidBarcode) {
            return []
        } else {
            const er = []
            er.push('Barcode must be contains 12 digits')
            return er;
        }
    },
    checkSKU(inputSKU) {
        if (!validateSKU(inputSKU)) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = ['SKU must contains only characters or numbers']
            throw invalidFieldError
        }
    },

    checkStockProductName(inputStockProductName) {
        if (!validateStockProductName(inputStockProductName)) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = ['StockProduct Name must contains only characters or numbers']
            throw invalidFieldError
        }
    },
    checkSKUForImport(inputSKU) {
        const isValid = validateSKU(inputSKU);
        const error = []
        if (isValid) {
            return error
        } else {
            error.push('SKU must contains only characters or numbers');
            return error;
        }
    },

    checkStockProductNameForImport(inputStockProductName) {
        const isValid = validateStockProductName(inputStockProductName);
        const error = []
        if (isValid) {
            return error
        } else {
            error.push('StockProduct Name must contains only characters or numbers');
            return error;
        }
    },
    async checkNullUndefinedEmptyStockProduct(input) {
        const errors = checkNullUndefinedEmpty(input, stockProductAlias);
        const a = 1
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    validateDateStockProduct(inputDate) {
        const isValid = validateDateFormat(inputDate, DATE_FORMAT_STOCKPRODUCT);
        if (!isValid) {
            const errors = []
            errors.push(`${stockProductAlias.manufacturedDate.eng} is invalid format`)
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
        if(dateIsInTheFuture(inputDate)) {
            const errors = []
            errors.push(`${stockProductAlias.manufacturedDate.eng} cannot be in the future`)
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    validateDateStockProductSearch(inputDate) {
        const isValid = validateDateFormat(inputDate, DATE_FORMAT_STOCKPRODUCT);
        if (!isValid) {
            const errors = []
            errors.push(`${stockProductAlias.manufacturedDate.eng} is invalid format`)
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkExistDatabase(input) {
        const {
            type, stockGroupId, standard,
            statusId, warehouseId } = input;
        const checkField = {
            type: {
                id: type,
                controller: steelTypeController
            },
            stockGroupId: {
                id: stockGroupId,
                controller: stockGroupController
            },
            standard: {
                id: standard,
                controller: stockProductStandardController
            },
            statusId: {
                id: statusId,
                controller: stockProductStatusController
            },
            warehouseId: {
                id: warehouseId,
                controller: require('../controllers/viewWarehouseController')
            }
        }
        const errors = []
        for (field in checkField) {
            const { id, controller } = checkField[field];
            if (isNaN(id)) {
                if (field !== 'statusId') {
                    errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                } else {
                    const res = await controller.find(id);
                    if (res.data.count === 0) {
                        errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                    }
                }
            } else {
                const res = await controller.find(id);
                if (res.data.count === 0) {
                    errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                }
            }
        }
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkExistDatabaseForUpdate(input) {
        const {
            stockProductId, type, stockGroupId, standard,
            statusId, warehouseId } = input;
        const stockProductController = require('../controllers/stockProductController');
        const checkField = {
            stockProductId: {
                id: stockProductId,
                controller: stockProductController
            },
            type: {
                id: type,
                controller: steelTypeController
            },
            stockGroupId: {
                id: stockGroupId,
                controller: stockGroupController
            },
            standard: {
                id: standard,
                controller: stockProductStandardController
            },
            statusId: {
                id: statusId,
                controller: stockProductStatusController
            },
            warehouseId: {
                id: warehouseId,
                controller: require('../controllers/viewWarehouseController')
            }
        }
        const errors = []
        for (field in checkField) {
            const { id, controller } = checkField[field];
            if (isNaN(id)) {
                if (field !== 'statusId') {
                    errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                } else {
                    const res = await controller.find(id);
                    if (res.data.count === 0) {
                        errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                    }
                }
            } else {
                const res = await controller.find(id);
                if (res.data.count === 0) {
                    errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                }
            }
        }
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkExistDatabaseForDelete(input) {
        const { stockProductId } = input;
        const stockProductController = require('../controllers/stockProductController');
        const res = await stockProductController.find(stockProductId);
        const errors = []
        if (res.data.count === 0) {
            errors.push(`${stockProductAlias.stockProductId.eng} ${NOT_EXIST_IN_DATABASE}`)
        }
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async isNumber(input) {
        const errors = checkNumber(input, stockProductAlias);
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkFieldInRange(input, range = { lower: 0, upper: Number.MAX_VALUE }) {
        const errors = checkNumberInRange(input, stockProductAlias, range);
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkExistDatabaseForImport(input) {
        const {
            type, stockGroupId, standard,
            statusId, warehouseId } = input;
        const checkField = {
            type: {
                id: type,
                controller: steelTypeController
            },
            stockGroupId: {
                id: stockGroupId,
                controller: stockGroupController
            },
            standard: {
                id: standard,
                controller: stockProductStandardController
            },
            statusId: {
                id: statusId,
                controller: stockProductStatusController
            },
            warehouseId: {
                id: warehouseId,
                controller: require('../controllers/viewWarehouseController')
            }
        }
        const errors = []
        for (field in checkField) {
            const { controller } = checkField[field];
            const id = checkField[field].id.trim();
            if (isNaN(id)) {
                if (field !== 'statusId') {
                    errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                } else {
                    const res = await controller.find(id);
                    if (res.data.count === 0) {
                        errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                    }
                }
            } else {
                const res = await controller.find(id);
                if (res.data.count === 0) {
                    errors.push(`${stockProductAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
                }
            }
        }
        return errors
    }
}