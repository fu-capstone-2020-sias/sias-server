const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads');
    },
    filename: (req, file, cb) => {
        console.log(file);
        const fileName = Date.now() + "_" + req.user.userName + "_" + file.originalname;
        req.params.fileNameAndUploader = fileName + "," + req.user.userName;
        cb(null, fileName);
    }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'text/csv'
        || file.mimetype === 'application/vnd.ms-excel') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}
module.exports.multerUpload = multer({storage: storage, fileFilter: fileFilter});