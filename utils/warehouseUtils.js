const { checkNullUndefinedEmpty } = require('./commonUtils');
const cityProvinceController = require('../controllers/cityProvinceController')
const districtController = require('../controllers/districtController')
const wardCommuneController = require('../controllers/wardcommuneController');
const departmentController = require('../controllers/parentDepartmentController')
const warehouseManagerController = require('../controllers/warehouseManagerController');
const warehouseAlias = require('../constants/warehouseAlias')
const { NOT_EXIST_IN_DATABASE } = require('../constants/constants')
// const viewWarehouseController = require('../controllers/viewWarehouseController')
module.exports = {
    async checkNullUndefinedEmptyWarehouse(input){
        const {
            name, cityProvinceId, districtId, wardCommuneId, address,
            parentDepartmentId, headOfWarehouse
        } = input;
        const errors = checkNullUndefinedEmpty({
            name, cityProvinceId, districtId, wardCommuneId,
            parentDepartmentId, headOfWarehouse
        }, warehouseAlias);
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    
    async checkExistDatabase(input){
        const {
            cityProvinceId, districtId, wardCommuneId, 
            parentDepartmentId, headOfWarehouse,
        } = input;
        const errors = []
        const checkField = {
            cityProvinceId: {
                id: cityProvinceId,
                controller: cityProvinceController
            },
            districtId:{
                id: districtId,
                controller: districtController
            },
            wardCommuneId: {
                id: wardCommuneId,
                controller: wardCommuneController
            },
            parentDepartmentId: {
                id: parentDepartmentId,
                controller: departmentController
            },
            headOfWarehouse: {
                id: headOfWarehouse,
                controller: warehouseManagerController
            }
        }
        for(field in checkField){
            const { controller, id } = checkField[field] 
            const res = await controller.find(id)
            if(res.data.count === 0){
                errors.push(`${warehouseAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
            }
        }
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkNullUndefinedEmptyWarehouseForUpdate(input){
        const {
            warehouseId, name, cityProvinceId, districtId, wardCommuneId, address,
            parentDepartmentId, headOfWarehouse
        } = input;
        const errors = checkNullUndefinedEmpty({
            warehouseId, name, cityProvinceId, districtId, wardCommuneId,
            parentDepartmentId, headOfWarehouse
        }, warehouseAlias);
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    },
    async checkExistDatabaseForUpdate(input){
        const {
            warehouseId, cityProvinceId, districtId, wardCommuneId, 
            parentDepartmentId, headOfWarehouse, 
        } = input;
        const viewWarehouseController = require('../controllers/viewWarehouseController')
        const errors = []
        const checkField = {
            warehouseId: {
                id: warehouseId,
                controller: viewWarehouseController
            },
            cityProvinceId: {
                id: cityProvinceId,
                controller: cityProvinceController
            },
            districtId:{
                id: districtId,
                controller: districtController
            },
            wardCommuneId: {
                id: wardCommuneId,
                controller: wardCommuneController
            },
            parentDepartmentId: {
                id: parentDepartmentId,
                controller: departmentController
            },
            headOfWarehouse: {
                id: headOfWarehouse,
                controller: warehouseManagerController
            }
        }
        for(field in checkField){
            const { controller, id } = checkField[field] 
            const res = await controller.find(id)
            if(res.data.count === 0){
                errors.push(`${warehouseAlias[field].eng} ${NOT_EXIST_IN_DATABASE}`)
            }
        }
        if (errors.length > 0) {
            const invalidFieldError = Error('Invalid field')
            invalidFieldError.statusCode = 400
            invalidFieldError.errorList = errors
            throw invalidFieldError
        }
    }
}